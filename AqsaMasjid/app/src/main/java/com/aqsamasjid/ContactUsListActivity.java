package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.adapter.GetSubjectAdapter;
import com.aqsamasjid.pojo.SubjectPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.ShowDialog;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi") public class ContactUsListActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private EditText etEmail;
	private EditText etName;
	private EditText etContact;
	private EditText etComment;

	private NetworkConnection networkConnection;
	private ShowDialog showMessage;
	private ArrayList<SubjectPojo> alSubject;
	private GetSubjectAdapter adapter;
	private SubjectPojo pojo;
	private long subjectId;
	private TextView tvAppLabel;
	private TextView tvServiceText;
	private ListView lvSubject;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_us_list);
		initComponents();
		initListiners();
		initSideBar();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		showMessage = new ShowDialog();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		lvSubject = (ListView) findViewById(R.id.lv_contact_us_list);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvServiceText= (TextView) findViewById(R.id.tv_serviice_text);
		tvAppLabel.setText(SPMasjid.getValue(ContactUsListActivity.this, SPMasjid.Masjid_Name));

		/*alSubject = new ArrayList<SubjectPojo>();
		pojo = new SubjectPojo();
		pojo.setSubjectId(0);
		pojo.setSubjectName("Choose Subject");
		alSubject.add(0, pojo);
		*/
		if (SPMasjid.getValue(ContactUsListActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(ContactUsListActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(ContactUsListActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(ContactUsListActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvServiceText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(ContactUsListActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(ContactUsListActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}




		if (networkConnection.isOnline(getApplicationContext())) {
			new GetSubjectAsyncTask().execute();
		} else {
			Toast.makeText(getBaseContext(),
					"Please check your network connection.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void initListiners() {
		// TODO Auto-generated method stub

		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(ContactUsListActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

				//setResult(RESULT_OK,new Intent(ContactUsActivity.this,ContactUsActivity.class));
				finish();
			}
		});
		tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(ContactUsListActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				//setResult(RESULT_OK,new Intent(ContactUsActivity.this,ContactUsActivity.class));
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ContactUsListActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ContactUsListActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				if (SPMasjid.getIntValue(ContactUsListActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(ContactUsListActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(ContactUsListActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(ContactUsListActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});

		lvSubject.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				startActivity(new Intent(ContactUsListActivity.this, ContactUsActivity.class).putExtra("subject_id", alSubject.get(position).getSubjectId()));
				
			}
		});
		/*lvSubject.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				subjectId = alSubject.get(position).getSubjectId();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});*/
		
	}
	
	class GetSubjectAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(ContactUsListActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.GetContactSubject, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						alSubject = new ArrayList<SubjectPojo>();
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);
							pojo = new SubjectPojo();
							pojo.setSubjectId(jsObject.optInt("id"));
							pojo.setSubjectName(jsObject.optString("title"));
							alSubject.add(pojo);
						}
						adapter = new GetSubjectAdapter(ContactUsListActivity.this, alSubject);
						lvSubject.setAdapter(adapter);
					}else {
						showMessage.showMsg(ContactUsListActivity.this, jsonObject.optString("message"));
					}
					
				} else {
					Toast.makeText(ContactUsListActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
}
