package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.adapter.ReasonSpinnerAdapter;
import com.aqsamasjid.pojo.ReasonPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;


@SuppressLint("NewApi") public class DonateActivity extends BaseActivity {

	//private Button btnDonateWithPaypal;
	private Button btnPayNow;
	private NetworkConnection networkConnection;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
	//	private ImageView ivLogoWithText;
	private EditText etDonationAmount;
	private EditText etEmail;
	private TextView tvAppLabel;

	//private String deviceId;
	private String id;
	private String createTime;
	private String platform;
	private String shortDescription;
	private String ammount;
	private String currencyCode;
	private String productionKey;
	private String sandboxKey;
	private String environment;
	private String merchantName;
	private String getCurrencyCode;
	private String getShortDesc;
	private Spinner spnReason;
	private String reasonValue;
	private ReasonSpinnerAdapter reasonAdapter;
	private ArrayList<ReasonPojo> alDummyAirport = new ArrayList<ReasonPojo>();
	ArrayList<ReasonPojo> listdata;
	protected long reason_id;
	private static String CONFIG_CLIENT_ID;
	private static String CONFIG_ENVIRONMENT ;

//	private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
//	private static final String CONFIG_CLIENT_ID = "AZrpZfP_6l-4z0zQ5Y1-t5iNSjYwY1E5yHbZbYtWGc5nWEo1uRNf3mjdzXFFDz1U207fL1wHtv2lgZWz";

	private static final int REQUEST_CODE_PAYMENT = 1;
	private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
	private static final int REQUEST_CODE_PROFILE_SHARING = 3;

	private static final String TAG = "paymentExample";
	private PayPalConfiguration config;
	private TextView tvEventText;
	public String payButtonUrl;
	private LinearLayout llEmail;
	private LinearLayout llAmount;
	private LinearLayout llSpinner;
	private ImageView imPaypalDonation;
	public String donationPageText;
	private TextView tvDontionText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_donate);
		initComponents();
		initListiners();
		initSideBar();
		/*TelephonyManager manager = (TelephonyManager) getApplicationContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceId = manager.getDeviceId();*/
		if (networkConnection.isOnline(getApplicationContext())) {
			new GetPaymentDetail().execute();
		} else {
			Toast.makeText(getBaseContext(),
					"Please check your network connection.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	/*private PayPalPayment getThingToBuy(String paymentIntent) {
		return new PayPalPayment(new BigDecimal(etDonationAmount.getText()
				.toString()), getCurrencyCode, getShortDesc, paymentIntent);
	}*/


	private PayPalPayment getStuffToBuy(String paymentIntent) {
		//--- include an item list, payment amount details
		PayPalItem[] items =
				{
						new PayPalItem(etEmail.getText().toString(), 1, new BigDecimal(
								etDonationAmount.getText().toString()),getCurrencyCode, "Donation"),
				};
		BigDecimal subtotal = PayPalItem.getItemTotal(items);
		BigDecimal shipping = new BigDecimal("0");
		BigDecimal tax = new BigDecimal("0");
		PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
		BigDecimal amount = subtotal.add(shipping).add(tax);
		PayPalPayment payment = new PayPalPayment(amount, getCurrencyCode,
				getShortDesc, paymentIntent);
		payment.items(items).paymentDetails(paymentDetails);

		Log.v("pay amoune", amount+"");

		//--- set other optional fields like invoice_number, custom field, and soft_descriptor
		payment.custom(etEmail.getText().toString());

		return payment;
	}
	@Override
	public void onDestroy() {
		// Stop service when done
		try {
			stopService(new Intent(this, PayPalService.class));
		}catch (Exception e){
			e.printStackTrace();
		}

		super.onDestroy();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		btnPayNow = (Button) findViewById(R.id.btn_pay_now);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		llEmail = (LinearLayout) findViewById(R.id.ll_email);
		llAmount = (LinearLayout) findViewById(R.id.ll_amount);
		llSpinner = (LinearLayout) findViewById(R.id.ll_spinner);
		etDonationAmount = (EditText) findViewById(R.id.et_donation_amount);
		etEmail = (EditText) findViewById(R.id.et_user_email);
//		etReasonAmount = (EditText) findViewById(R.id.et_reason_amount);
		spnReason = (Spinner) findViewById(R.id.spn_reason);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvEventText= (TextView) findViewById(R.id.tv_event_text);
		imPaypalDonation= (ImageView) findViewById(R.id.im_paypal_donation);
		tvDontionText= (TextView) findViewById(R.id.tv_dontion_text);
		tvAppLabel.setText(SPMasjid.getValue(DonateActivity.this, SPMasjid.Masjid_Name));
		reasonAdapter = new ReasonSpinnerAdapter(DonateActivity.this, alDummyAirport);
		spnReason.setAdapter(reasonAdapter);

		if (SPMasjid.getValue(DonateActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(DonateActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvEventText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(DonateActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(DonateActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvEventText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(DonateActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(DonateActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvEventText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}



		if (SPMasjid.getValue(DonateActivity.this, SPMasjid.donate_btn_BackColor).length()!=0&&(!SPMasjid.getValue(DonateActivity.this, SPMasjid.donate_btn_BackColor).equalsIgnoreCase(null)))
		{
			btnPayNow.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.donate_btn_BackColor)));
		}
		if (SPMasjid.getValue(DonateActivity.this, SPMasjid.donate_btn_alpha).length()!=0&&(!SPMasjid.getValue(DonateActivity.this, SPMasjid.donate_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.donate_btn_alpha))>0.0f)
		{
			btnPayNow.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.donate_btn_alpha)));
		}
		if (SPMasjid.getValue(DonateActivity.this, SPMasjid.donate_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(DonateActivity.this, SPMasjid.donate_btn_ForeColor).equalsIgnoreCase(null)))
		{
			btnPayNow.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.donate_btn_ForeColor)));
		}
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		tvAppLabel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llPrayer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llEvents.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DonateActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DonateActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});

		spnReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				// TODO Auto-generated method stub
				reasonValue =listdata.get(position).getTitle()+"";
				Log.v("Reason name", listdata.get(position).getTitle()+""+" -- "+listdata.get(position).getId()+"");
				reason_id=listdata.get(position).getId();



			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});


		btnPayNow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					if (networkConnection.isOnline(getApplicationContext())) {
						if (environment.equalsIgnoreCase("PayPalEnvironmentNoNetwork")&& payButtonUrl.length()>0)
						{
							Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(payButtonUrl));
							startActivity(browserIntent);
						}
						else {
							if (environment.equalsIgnoreCase("PayPalEnvironmentSandbox")) {
								CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
								CONFIG_CLIENT_ID = sandboxKey;
							} else {
								CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
								CONFIG_CLIENT_ID = productionKey;
							}
							config = new PayPalConfiguration()
									.environment(CONFIG_ENVIRONMENT)
									.clientId(CONFIG_CLIENT_ID)
									// The following are only used in
									// PayPalFuturePaymentActivity.
									.merchantName(merchantName)
									.acceptCreditCards(true)
									.merchantPrivacyPolicyUri(
											Uri.parse("https://www.example.com/privacy"))
									.merchantUserAgreementUri(
											Uri.parse("https://www.example.com/legal"));

							Intent in = new Intent(DonateActivity.this, PayPalService.class);
							in.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
							startService(in);

							if (etEmail.getText().toString().length() == 0) {
								showMsgDialog("Please enter E-mail.");
							} else if (!EmailValidator.isValid(etEmail.getText().toString())) {
								showMsgDialog("please enter valid E-mail.");
							} else if (etDonationAmount.getText().length() == 0) {
								showMsgDialog("Please enter amount.");
							} else {
								PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
								Intent intent = new Intent(DonateActivity.this, PaymentActivity.class);
								intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
								startActivityForResult(intent, REQUEST_CODE_PAYMENT);
							}
						}
					} else {
						Toast.makeText(getBaseContext(),
								"Please check your network connection.", Toast.LENGTH_SHORT)
								.show();
					}


				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Bundle extra = data.getExtras();

		if (requestCode == REQUEST_CODE_PAYMENT) {
			if (resultCode == Activity.RESULT_OK) {
				PaymentConfirmation confirm = data
						.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
				if (confirm != null) {
					try {
						Log.i(TAG, confirm.toJSONObject().toString(4));
						Log.i(TAG, confirm.getPayment().toJSONObject()
								.toString(4));

						JSONObject jsonObject = new JSONObject(confirm
								.toJSONObject().toString(4));
						JSONObject jsonObject2 = jsonObject
								.optJSONObject("response");
						id = jsonObject2.optString("id");
						createTime = jsonObject2.optString("create_time");

						JSONObject jsonObject3 = jsonObject
								.optJSONObject("client");
						platform = jsonObject3.optString("platform");

						JSONObject jsonObject4 = new JSONObject(confirm
								.getPayment().toJSONObject().toString(4));
						shortDescription = jsonObject4
								.optString("short_description");
						ammount = jsonObject4.optString("amount");
						currencyCode = jsonObject4.optString("currency_code");

						if (networkConnection.isOnline(getApplicationContext())) {
							new DonationAsyncTask().execute();
						} else {
							Toast.makeText(getBaseContext(),
									"Please check your network connection.",
									Toast.LENGTH_SHORT).show();
						}


						/**
						 * TODO: send 'confirm' (and possibly
						 * confirm.getPayment() to your server for verification
						 * or consent completion. See
						 * https://developer.paypal.com
						 * /webapps/developer/docs/integration
						 * /mobile/verify-mobile-payment/ for more details.
						 *
						 * For sample mobile backend interactions, see
						 * https://github
						 * .com/paypal/rest-api-sdk-python/tree/master
						 * /samples/mobile_backend
						 */

						// /// code for submit information

						/*Toast.makeText(
								getApplicationContext(),
								"PaymentConfirmation info received from PayPal",
								Toast.LENGTH_LONG).show();*/

					} catch (JSONException e) {
						Log.e(TAG, "an extremely unlikely failure occurred: ",
								e);
					}
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Log.i(TAG, "The user canceled.");
			} else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
				Log.i(TAG,
						"An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
			}
		} else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
			if (resultCode == Activity.RESULT_OK) {
				PayPalAuthorization auth = data
						.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
				if (auth != null) {
					try {
						Log.i("FuturePaymentExample", auth.toJSONObject()
								.toString(4));

						String authorization_code = auth.getAuthorizationCode();
						Log.i("FuturePaymentExample", authorization_code);

						sendAuthorizationToServer(auth);
						Toast.makeText(getApplicationContext(),
								"Future Payment code received from PayPal",
								Toast.LENGTH_LONG).show();

					} catch (JSONException e) {
						Log.e("FuturePaymentExample",
								"an extremely unlikely failure occurred: ", e);
					}
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Log.i("FuturePaymentExample", "The user canceled.");
			} else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
				Log.i("FuturePaymentExample",
						"Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
			}
		} else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
			if (resultCode == Activity.RESULT_OK) {
				PayPalAuthorization auth = data
						.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
				if (auth != null) {
					try {
						Log.i("ProfileSharingExample", auth.toJSONObject()
								.toString(4));

						String authorization_code = auth.getAuthorizationCode();
						Log.i("ProfileSharingExample", authorization_code);

						sendAuthorizationToServer(auth);
						Toast.makeText(getApplicationContext(),
								"Profile Sharing code received from PayPal",
								Toast.LENGTH_LONG).show();

					} catch (JSONException e) {
						Log.e("ProfileSharingExample",
								"an extremely unlikely failure occurred: ", e);
					}
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Log.i("ProfileSharingExample", "The user canceled.");
			} else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
				Log.i("ProfileSharingExample",
						"Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
			}
		}
	}

	private void sendAuthorizationToServer(PayPalAuthorization authorization) {

		/**
		 * TODO: Send the authorization response to your server, where it can
		 * exchange the authorization code for OAuth access and refresh tokens.
		 *
		 * Your server must then store these tokens, so that your server code
		 * can execute payments for this user in the future.
		 *
		 * A more complete example that includes the required app-server to
		 * PayPal-server integration is available from
		 * https://github.com/paypal/
		 * rest-api-sdk-python/tree/master/samples/mobile_backend
		 */

	}

	class DonationAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("token", WebUrl.TOKEN_KEY));
			urlParameter.add(new BasicNameValuePair("donor_email", etEmail.getText().toString()));
			urlParameter.add(new BasicNameValuePair("device_id",
					SPMasjid.getValue(DonateActivity.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("amount", ammount));
			urlParameter.add(new BasicNameValuePair("currency_code",
					currencyCode));
			urlParameter.add(new BasicNameValuePair("payment_create_time",
					createTime));
			urlParameter.add(new BasicNameValuePair("payment_id", id));
			urlParameter
					.add(new BasicNameValuePair("payment_type", "donation"));
			urlParameter.add(new BasicNameValuePair("platform", platform));
			urlParameter.add(new BasicNameValuePair("qty", "1"));

//			if (reasonValue.equalsIgnoreCase("other")) {
//				urlParameter.add(new BasicNameValuePair("reason", etReasonAmount
//						.getText().toString()));
//			} else {
			urlParameter.add(new BasicNameValuePair("reason", reasonValue));
			urlParameter.add(new BasicNameValuePair("reason_id", reason_id+""));

//			}

//			urlParameter.add(new BasicNameValuePair("reason", etReasonAmount
//					.getText().toString()));
			urlParameter.add(new BasicNameValuePair("short_description",
					shortDescription));
			progressDialog = new ProgressDialog(DonateActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("urlParameter", urlParameter + "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					WebUrl.DonationComplete, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						// showMsgDialog(jsonObject.optString("message"));
						startActivity(new Intent(DonateActivity.this, ThankyouActivity.class).putExtra("donate_message", jsonObject.optString("message")));
						etDonationAmount.setText("");
						etEmail.setText("");
//						etReasonAmount.setText("");
					} else {
						// showMsgDialog(jsonObject.optString("message"));
					}

				} else {
					Toast.makeText(DonateActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public void showMsgDialog(String message) {
		new AlertDialog.Builder(DonateActivity.this)

				.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				}).create().show();
	}

	class GetPaymentDetail extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("token", WebUrl.TOKEN_KEY));
			progressDialog = new ProgressDialog(DonateActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter Get", urlParameter + "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					WebUrl.GetPaymentDetail, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONObject jsObject = jsonObject.optJSONObject("data");
						productionKey = jsObject.optString("production_key");
						sandboxKey = jsObject.optString("sandbox_key");
						environment = jsObject.optString("environment");
						merchantName = jsObject.optString("marchent_name");
						getCurrencyCode = jsObject.optString("currency_code");
						donationPageText = jsObject.optString("donation_page_text");
						getShortDesc = jsObject.optString("short_desc");
						payButtonUrl = jsObject.optString("paypal_button_url");
						if (environment.equalsIgnoreCase("PayPalEnvironmentNoNetwork")&& payButtonUrl.length()>0) {
							llAmount.setVisibility(View.GONE);
							llEmail.setVisibility(View.GONE);
							llSpinner.setVisibility(View.GONE);
							imPaypalDonation.setVisibility(View.GONE);
							tvDontionText.setVisibility(View.VISIBLE);
							tvDontionText.setText(donationPageText);
						}
						JSONArray jsonArray = jsObject.optJSONArray("reasons");
						System.out.println(jsObject.optJSONArray("reasons"));

						listdata = new ArrayList<ReasonPojo>();
						JSONArray jArray = (JSONArray)jsonArray;
						if (jArray != null) {
							for (int i=0;i<jArray.length();i++){
								JSONObject jobject=jArray.optJSONObject(i);
								ReasonPojo pojo=new ReasonPojo();
								pojo.setId(jobject.optLong("id"));
								pojo.setTitle(jobject.optString("title"));
								listdata.add(pojo);
							}
						}
						reasonAdapter = new ReasonSpinnerAdapter(DonateActivity.this,listdata);
						spnReason.setAdapter(reasonAdapter);
					}

				} else {
					Toast.makeText(DonateActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exceptioncd
				e.printStackTrace();
			}
		}
	}

}