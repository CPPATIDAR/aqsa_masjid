package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.Webimageloader.ImageLoader;
import com.aqsamasjid.imageloader.MasjidImageLoader;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi") public class ServiceDetailActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private TextView tvServiceTitle;
//	private TextView tvServiceDetail;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private Button btnRegister;
	private Dialog dialog;
	private EditText etFirstname;
	private EditText etEmail;
	private EditText etMobile;
	private TextView tvTExtHeader;
	private ImageView ivBannerService;
	private NetworkConnection networkConnection;
	private MasjidImageLoader imageLoader;
	private ImageLoader webImageLoader;
	private WebView webView;
	private TextView tvAppLabel;
	private TextView tvServiceText;
	private LinearLayout llBannerDetailContainer;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service_detail);
		initComponents();
		initListiners();
		initSideBar();

		Intent intent = getIntent();
		tvServiceTitle.setText(intent.getStringExtra("ServiceTitle"));
//		tvServiceDetail.setText(intent.getStringExtra("ServiceDetail"));

		webView.loadData(intent.getStringExtra("ServiceDetail"), "text/html", "UTF-8");
		WebSettings webSettings = webView.getSettings();
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultFontSize((int)20);

		imageLoader = new MasjidImageLoader(ServiceDetailActivity.this);
		webImageLoader = new ImageLoader(ServiceDetailActivity.this);
		try {
			String imageUrl = intent.getStringExtra("ServiceBannerImage").toString();
			webImageLoader.DisplayImage(imageUrl, ivBannerService);

		} catch (Exception ex) {
			System.out.println("Error :  Image URL " + ex);
			ex.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		/*Intent intent = new Intent(ServiceDetailActivity.this,ServiceActivity.class);
		startActivity(intent);*/
		finish();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		tvServiceTitle = (TextView) findViewById(R.id.tv_service_title);

//		tvServiceDetail = (TextView) findViewById(R.id.tv_service_detail);
		btnRegister = (Button) findViewById(R.id.btn_register_volunter_service);
		ivBannerService = (ImageView) findViewById(R.id.iv_banner_service_detail);
//		tvServiceDetail.setMovementMethod(new ScrollingMovementMethod());
		webView = (WebView) findViewById(R.id.webview_servide_detail);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvServiceText= (TextView) findViewById(R.id.tv_serviice_text);
		llBannerDetailContainer= (LinearLayout) findViewById(R.id.ll_banner_detail_container);

		tvAppLabel.setText(SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.Masjid_Name));

		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvServiceText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}



		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_BackColor).equalsIgnoreCase(null))) {
			llBannerDetailContainer.setBackgroundColor(Color.parseColor(SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_BackColor)));
		}

		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha))>0.0f) {
			llBannerDetailContainer.setAlpha(Float.parseFloat(SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha)));

		}

		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor).equalsIgnoreCase(null))) {
			tvServiceTitle.setTextColor(Color.parseColor(SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor)));
			}


		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.application_btn_BackColor).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.application_btn_BackColor).equalsIgnoreCase(null)))
		{
			btnRegister.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_BackColor)));
		}
		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.application_btn_alpha).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.application_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha))>0.0f)
		{
			btnRegister.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha)));
		}
		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.application_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.application_btn_ForeColor).equalsIgnoreCase(null)))
		{
			btnRegister.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_ForeColor)));
		}


	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(ServiceDetailActivity.this, ServiceActivity.class));
				/*Intent intent = new Intent(ServiceDetailActivity.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});
		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ServiceDetailActivity.this,ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});*/
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(ServiceDetailActivity.this,HomeActivity.class);
				startActivity(intent);*/
				setResult(RESULT_OK,new Intent(ServiceDetailActivity.this, ServiceActivity.class));

				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ServiceDetailActivity.this, EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ServiceDetailActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(ServiceDetailActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(ServiceDetailActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(ServiceDetailActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(ServiceDetailActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});	
		btnRegister.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			//	dialogForRegistration();
				startActivity(new Intent(ServiceDetailActivity.this, ContactUsListActivity.class));
				finish();
			}
		});
	}
	
	protected void dialogForRegistration() {
		// TODO Auto-generated method stub
		
		dialog = new Dialog(ServiceDetailActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_registration);
		tvTExtHeader = (TextView) dialog.findViewById(R.id.tv_text_header);
		etFirstname = (EditText) dialog.findViewById(R.id.et_name);
		etEmail = (EditText) dialog.findViewById(R.id.et_email);
		etMobile = (EditText) dialog.findViewById(R.id.et_mobile);
		Button btnSubmit = (Button) dialog.findViewById(R.id.btn_submit);
		Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancal);
		
		tvTExtHeader.setText("Become a volunteer");
		
		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.header_BackColor).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.header_BackColor).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));
			btnSubmit.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));
			btnCancel.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));

		}
		
		
		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.header_BackAlpha).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.header_BackAlpha).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
			btnSubmit.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
			btnCancel.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));

		}
		
		if (SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.header_ForeColor).length()!=0&&(!SPMasjid.getValue(ServiceDetailActivity.this, SPMasjid.header_ForeColor).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));
			btnSubmit.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));
			btnCancel.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));

		}
		
		
		dialog.show();
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (etFirstname.getText().toString().length()==0) {
					showMsgDialog("Please enter first name");
				}else if (etEmail.getText().toString().length()==0) {
					showMsgDialog("Please enter E-mail.");
				}else if (!EmailValidator.isValid(etEmail.getText().toString())) {
					showMsgDialog("please enter valid E-mail.");
				}else if (etMobile.getText().toString().length()==0) {
					showMsgDialog("Please enter phone number.");
				}else if(etMobile.getText().toString().trim().length() != 10) {
					showMsgDialog("Please enter valid phone number.");
				}
				else {
					if (networkConnection.isOnline(getApplicationContext())) {
							new RegistrationAsyncTask().execute();
					} else {
						Toast.makeText(getBaseContext(),
								"Please check your network connection.", Toast.LENGTH_SHORT).show();
					}
					
				}
			}
		});
	}
	public void showMsgDialog(String message) {
		new AlertDialog.Builder(ServiceDetailActivity.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

					}
				}).create().show();
	}
	
	class RegistrationAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("firstname",etFirstname.getText().toString()));
			urlParameter.add(new BasicNameValuePair("email",etEmail.getText().toString()));
			urlParameter.add(new BasicNameValuePair("mobile",etMobile.getText().toString()));
			urlParameter.add(new BasicNameValuePair("type","service"));
			//urlParameter.add(new BasicNameValuePair("object_id",eventId));
			progressDialog = new ProgressDialog(ServiceDetailActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter Registration", urlParameter+"");
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.SignUpVoluentierUrl, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}else {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}
					
				} else {
					Toast.makeText(ServiceDetailActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
