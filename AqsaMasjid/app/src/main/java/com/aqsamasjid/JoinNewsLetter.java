package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class JoinNewsLetter extends BaseActivity {

	private EditText etName;
	private EditText etEmail;
	private Button btnUpdate;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private NetworkConnection networkConnection;
	//private String deviceId;
	private TextView tvTextUnsubscribe;
	private Button btnUnsubscribe;
	private LinearLayout llSubscribePanel;
	private LinearLayout llUnSubscribePanel;
	private TextView tvAppLabel;
	private TextView tvSubHeaderText;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_letter);
		initComponents();
		initListiners();
		initSideBar();
		/*TelephonyManager manager=(TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
		deviceId=manager.getDeviceId();*/
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		etName = (EditText) findViewById(R.id.et_name_news);
		etEmail = (EditText) findViewById(R.id.et_email_news);
		btnUpdate = (Button) findViewById(R.id.btn_keep_me_update);
		tvTextUnsubscribe = (TextView) findViewById(R.id.tv_unsubscribe_text);
		btnUnsubscribe = (Button) findViewById(R.id.btn_unsubscribe);
		llSubscribePanel = (LinearLayout) findViewById(R.id.ll_subscribe_panel);
		llUnSubscribePanel = (LinearLayout) findViewById(R.id.ll_unsubscribe);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);

		tvSubHeaderText= (TextView) findViewById(R.id.tv_event_detail_text);
		tvAppLabel.setText(SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.Masjid_Name));
		if (SPMasjid.getBooleanValue(JoinNewsLetter.this, SPMasjid.SubscribeNews)) {
			llUnSubscribePanel.setVisibility(View.VISIBLE);
			llSubscribePanel.setVisibility(View.GONE);
		}else {
			llSubscribePanel.setVisibility(View.VISIBLE);
			llUnSubscribePanel.setVisibility(View.GONE);
		}


		if (SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvSubHeaderText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvSubHeaderText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvSubHeaderText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}

		if (SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_BackColor).length()!=0&&(!SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_BackColor).equalsIgnoreCase(null)))
		{
			btnUpdate.setBackgroundColor(Color.parseColor(SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_BackColor)));
		}
		if (SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_alpha).length()!=0&&(!SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_alpha))>0.0f)
		{
			btnUpdate.setAlpha(Float.parseFloat(SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_alpha)));
		}
		if (SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_ForeColor).equalsIgnoreCase(null)))
		{
			btnUpdate.setTextColor(Color.parseColor(SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.application_btn_ForeColor)));
		}

	}

	private void initListiners() {
		// TODO Auto-generated method stub

		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(JoinNewsLetter.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});

		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});*/

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(JoinNewsLetter.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(JoinNewsLetter.this, EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(JoinNewsLetter.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		
		llDonate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(JoinNewsLetter.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(JoinNewsLetter.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(JoinNewsLetter.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(JoinNewsLetter.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
		
		btnUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (etName.getText().toString().length()==0) {
					showMsgDialog("Please enter name");
				}else if (etEmail.getText().toString().length()==0) {
					showMsgDialog("Please enter E-mail.");
				}else if (!EmailValidator.isValid(etEmail.getText().toString())) {
					showMsgDialog("please enter valid E-mail.");
				} else {
					if (networkConnection.isOnline(getApplicationContext())) {
						new NewsLetterAsyncTask().execute();
				} else {
					Toast.makeText(getBaseContext(),
							"Please check your network connection.", Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
		
		btnUnsubscribe.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (networkConnection.isOnline(getApplicationContext())) {
					new NewsLetterUnSubsribeAsyncTask().execute();
			} else {
				Toast.makeText(getBaseContext(),
						"Please check your network connection.", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		
	}
	
	class NewsLetterAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("token", WebUrl.TOKEN_KEY));
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("fullname",etName.getText().toString()));
			urlParameter.add(new BasicNameValuePair("email",etEmail.getText().toString()));
			urlParameter.add(new BasicNameValuePair("device_id", SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("subscribeMe","1"));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(JoinNewsLetter.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.JoinNewsLetter, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						llSubscribePanel.setVisibility(View.GONE);
						llUnSubscribePanel.setVisibility(View.VISIBLE);
						tvTextUnsubscribe.setText(jsonObject.optString("message"));
						SPMasjid.setBooleanValue(JoinNewsLetter.this, SPMasjid.SubscribeNews, true);
						SPMasjid.setValue(JoinNewsLetter.this, SPMasjid.SubscribeName, etName.getText().toString());
						SPMasjid.setValue(JoinNewsLetter.this, SPMasjid.SubscribeEmail, etEmail.getText().toString());
						showMsgDialog(jsonObject.optString("message"));
					}else {
						showMsgDialog(jsonObject.optString("message"));
					}
					
				} else {
					Toast.makeText(JoinNewsLetter.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public void showMsgDialog(String message) {
		new AlertDialog.Builder(JoinNewsLetter.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						etEmail.setText("");
						etName.setText("");
					}
				}).create().show();
	}
	
	class NewsLetterUnSubsribeAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("token", WebUrl.TOKEN_KEY));
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("fullname",
					SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.SubscribeName)));
			urlParameter.add(new BasicNameValuePair("email",
					SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.SubscribeEmail)));
			urlParameter.add(new BasicNameValuePair("device_id", SPMasjid.getValue(JoinNewsLetter.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("subscribeMe","0"));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(JoinNewsLetter.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.JoinNewsLetter, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						llSubscribePanel.setVisibility(View.VISIBLE);
						llUnSubscribePanel.setVisibility(View.GONE);
						tvTextUnsubscribe.setText(jsonObject.optString("message"));
						SPMasjid.setBooleanValue(JoinNewsLetter.this, SPMasjid.SubscribeNews, false);
						SPMasjid.setValue(JoinNewsLetter.this, SPMasjid.SubscribeName, "");
						SPMasjid.setValue(JoinNewsLetter.this, SPMasjid.SubscribeEmail, "");
						showMsgDialog(jsonObject.optString("message"));
					}else {
						showMsgDialog(jsonObject.optString("message"));
					}
					
				} else {
					Toast.makeText(JoinNewsLetter.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
}
