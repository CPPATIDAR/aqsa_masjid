package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.imageloader.MasjidImageLoader;
import com.aqsamasjid.pojo.EventPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;


@SuppressLint("NewApi") public class EventActivity extends BaseActivity {

	private ArrayList<EventPojo> alEvent;
	private NetworkConnection networkConnection;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private LinearLayout llParent;
	private LinearLayout llChild;
	private LayoutInflater inflater;
	private ImageView ivHome;
	private TextView tvAppLabel;

//	private ImageView ivLogoWithText;
	private MasjidImageLoader imageLoader;
	private PullToRefreshScrollView scrollView;
	private ScrollView mScrollView;
	private String nextDate;
	private int pageCount = 0;
	private TextView tvEventText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event);
		initComponents();
		initListiners();
		initSideBar();

		if (networkConnection.isOnline(getApplicationContext())) {
			new EventAsyncTask().execute();
			} else {
				Toast.makeText(getBaseContext(),
						"Please check your network connection.", Toast.LENGTH_SHORT).show();
		}
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		llParent = (LinearLayout) findViewById(R.id.ll_parent);
		inflater = this.getLayoutInflater();
		imageLoader = new MasjidImageLoader(EventActivity.this);
		//scrollView = (ScrollView) findViewById(R.id.scrollview);
		scrollView = (PullToRefreshScrollView) findViewById(R.id.scrollview);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvEventText= (TextView) findViewById(R.id.tv_event_text);

		tvAppLabel.setText(SPMasjid.getValue(EventActivity.this, SPMasjid.Masjid_Name));


		if (SPMasjid.getValue(EventActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(EventActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvEventText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(EventActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(EventActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvEventText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(EventActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(EventActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvEventText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}


	}

	@SuppressLint("NewApi") private void initListiners() {
		// TODO Auto-generated method stub

		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});*/

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(EventActivity.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EventActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});


		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(EventActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(EventActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(EventActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(EventActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});




		scrollView.setOnRefreshListener(new OnRefreshListener<ScrollView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
				new EventAsyncTask().execute();
			}
		});

		 mScrollView = scrollView.getRefreshableView();
	}

	class EventAsyncTask extends AsyncTask<Void, Void, Void>{

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private EventPojo pojo;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("next_date",nextDate));
			urlParameter.add(new BasicNameValuePair("page_number",pageCount+""));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(EventActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.EventUrl, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (response!=null) {

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");

						alEvent = new ArrayList<EventPojo>();

					///////////////////////////////////////////////////////////////////

//	for (int k = 0; k < 3; k++) {



						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);
							//Log.v("DTAE EVENT", jsObject.optString("date"));

						/*	String date = jsObject.optString("date");
							String str[] = date.split("-");

							Calendar calendar = Calendar.getInstance();
							calendar.set(Integer.parseInt(str[0]), Integer.parseInt(str[1])-1, Integer.parseInt(str[2]));
							SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
							String formattedDate = df.format(calendar.getTime());*/

							TextView tvDate = new TextView(EventActivity.this);
							tvDate.setText(jsObject.optString("date"));
							tvDate.setTextColor(Color.RED);
							tvDate.setTextSize(18);
							tvDate.setPadding(20, 10, 0, 0);

							llParent.addView(tvDate);

							nextDate = jsObject.optString("date");
							Log.v("nextDate", nextDate);

							JSONArray jsArray = jsObject.optJSONArray("events");

							for (int j = 0; j < jsArray.length(); j++) {
								pojo = new EventPojo();
								final JSONObject jsonObj = jsArray.optJSONObject(j);

								llChild = (LinearLayout) inflater.inflate(
										R.layout.event_item, null);

								TextView tvItemName = (TextView) llChild.findViewById(R.id.tv_title);
								TextView tvStartTime = (TextView) llChild.findViewById(R.id.tv_from_date);
								TextView tvEndTime = (TextView) llChild.findViewById(R.id.tv_to_date);
								ImageView ivItemIcon = (ImageView) llChild.findViewById(R.id.iv_icon_event);
								ImageView ivseperator= (ImageView) llChild.findViewById(R.id.im_seperator);
								pojo.setEventId(jsonObj.optString("id"));
								pojo.setEventName(jsonObj.optString("title"));
								//pojo.setEventDetail(jsonObj.optString("details"));

								if (jsonObj.optString("details").equalsIgnoreCase("null")) {
									pojo.setEventDetail("");
								}else {
									pojo.setEventDetail(jsonObj.optString("details"));
								}

								pojo.setFromDate(jsonObj.optString("from_date"));
								pojo.setToDate(jsonObj.optString("to_date"));
								pojo.setStartTime(jsonObj.optString("start_time"));
								pojo.setEndTime(jsonObj.optString("end_time"));
								pojo.setPosterUrl(jsonObj.optString("poster_url"));
								pojo.setWebsite(jsonObj.optString("website"));
								pojo.setHostName(jsonObj.optString("hostname"));
								pojo.setPosterImage(jsonObj.optString("poster_image"));
								pojo.setLogoImage(jsonObj.optString("logo_image"));
								pojo.setWeekDay(jsonObj.optString("repete_on_days"));

								System.out.println("Poster Image"+jsonObj.optString("poster_image"));

								tvItemName.setText(jsonObj.optString("title"));
								tvStartTime.setText(jsonObj.optString("start_time")+" - ");
								tvEndTime.setText(jsonObj.optString("end_time"));

								try {
									String imageUrl = jsonObj.optString("logo_image").toString();
									Log.v("IMAGE Logo URL", imageUrl);

									String[] strfile = imageUrl.split("[/]");
									String fName = strfile[strfile.length - 1];
									File image = new File(Environment.getExternalStorageDirectory()
											.getAbsolutePath() + "/Aqsa_Masjid/Image/ " + fName);
									if (image.exists()) {
										BitmapFactory.Options o = new BitmapFactory.Options();
										o.inJustDecodeBounds = true;
										BitmapFactory.decodeStream(new FileInputStream(image),
												null, o);
										// decode with inSampleSize
										BitmapFactory.Options o2 = new BitmapFactory.Options();
										o2.inSampleSize = 8;
										ivItemIcon.setImageBitmap(BitmapFactory.decodeStream(
												new FileInputStream(image), null, o2));
									} else {
										imageLoader.DisplayImage(imageUrl, ivItemIcon, fName,
												R.drawable.noti_loader);
									}
								} catch (Exception ex) {
									System.out.println("Error :  Image URL " + ex);
									ex.printStackTrace();
								}

								llChild.setTag(pojo);

								llChild.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View v) {
										EventPojo evenPojo = (EventPojo) v.getTag();

										Intent intent = new Intent(EventActivity.this, EventDetailActivity.class);
										intent.putExtra("EventId", evenPojo.getEventId());
										intent.putExtra("EventTitle", evenPojo.getEventName());
										intent.putExtra("EventDetail", evenPojo.getEventDetail());
										intent.putExtra("EventPosterImage", evenPojo.getPosterImage());
										intent.putExtra("EventFromDate", evenPojo.getFromDate());
										intent.putExtra("EventToDate", evenPojo.getToDate());
										intent.putExtra("EventStartTime", evenPojo.getStartTime());
										intent.putExtra("EventEndTime", evenPojo.getEndTime());
										intent.putExtra("EventHostName", evenPojo.getHostName());
//										intent.putExtra("Website", evenPojo.getWebsite());
										intent.putExtra("WeekDay", evenPojo.getWeekDay());
										intent.putExtra("RSVP", jsonObj.optString("rsvp"));
										startActivityForResult(intent, 0);
										//startActivity(intent);
										//finish();
										
//										Toast.makeText(EventActivity.this,evenPojo.getEventName().toString(), Toast.LENGTH_SHORT).show();
									}
								});
								if (j==jsArray.length()-1) {
									ivseperator.setVisibility(View.GONE);
								}
								llParent.addView(llChild);
								
								/*ImageView imageView = new ImageView(EventActivity.this);
								imageView.setBackgroundColor(Color.parseColor("#000000"));
								imageView.getLayoutParams().height = 5;
								llParent.addView(imageView);*/
								alEvent.add(pojo);
							
							}
							View view = new View(EventActivity.this);
							view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,2));
							view.setBackgroundColor(Color.parseColor("#505050"));
							llParent.addView(view);
						}
						
						pageCount++;	
						
						
						
						
//}						
						
						/////////////////////////////////////////////////////////////////////
	
						scrollView.onRefreshComplete();
						
					}	
				} else {
					Toast.makeText(EventActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if (arg1 == RESULT_OK && arg0 ==0){
			finish();
		}
		super.onActivityResult(arg0, arg1, arg2);
		
	}
}
