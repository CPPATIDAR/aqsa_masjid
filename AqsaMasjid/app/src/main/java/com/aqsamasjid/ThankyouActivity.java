package com.aqsamasjid;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class ThankyouActivity extends Activity {

	private Button btnOk;
	private String donateMessage;
	private TextView tvThankyou;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_thankyou);
		initComponent();
		initListners();
		
		
	}

	private void initListners() {
		btnOk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ThankyouActivity.this.finish();
				
			}
		});
		
	}

	private void initComponent() {
		btnOk=(Button)findViewById(R.id.btn_ok);
		tvThankyou=(TextView)findViewById(R.id.tv_thankyou);
		try {
			donateMessage=getIntent().getExtras().getString("donate_message");
			tvThankyou.setText(donateMessage+"");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
}
