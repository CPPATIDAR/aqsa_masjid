package com.aqsamasjid.utill;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.aqsamasjid.broadcastRecivers.AlramBroadCastReceiver;

import java.util.Calendar;

public class NamazReminder {
	public static void setAlarm(Context context,String namaz, String time, int namazId) {
		if(time!=null){
			
			try {
		System.out.println("tag Time string"+time);
		
		String str[] = time.split(" ");
		String am_pm = str[1];
		System.out.println(am_pm);
		
		String[] timeArray = str[0].split(":");
		Calendar AlarmCal = Calendar.getInstance();
		AlarmCal.setTimeInMillis(System.currentTimeMillis());

		System.out.println("theHour=" + timeArray[0]);
		System.out.println("theMinute=" + timeArray[1]);
		
		if (am_pm.equalsIgnoreCase("PM")) {
			AlarmCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0])+12); 
		} else {
			AlarmCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0])); 
		}
		
		
//		AlarmCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0])); // set user selection
		AlarmCal.set(Calendar.MINUTE, Integer.parseInt(timeArray[1])); // set user selection
		AlarmCal.set(Calendar.SECOND, 0);
		long timeMiniSec = AlarmCal.getTimeInMillis();
		long azanMiliSec = AlarmCal.getTimeInMillis();
		long curntMili=(System.currentTimeMillis());
		System.out.println("Time Compairation="+timeMiniSec+" cu"+curntMili+"diff"+(timeMiniSec-curntMili));
		if (timeMiniSec < curntMili+21*60*1000) {
			timeMiniSec = timeMiniSec + 24 * 60 * 60 * 1000;
		}
		if (azanMiliSec < curntMili) {
			azanMiliSec = timeMiniSec ;
//			azanMiliSec = timeMiniSec + 24 * 60 * 60 * 1000;
		}
		AlarmManager alarmManager;
		Intent reminderIntent ;
		PendingIntent pendingReminderIntent; 
		switch (namazId) {
		
		case WebUrl.FAJR:
		//case WebUrl.DHUHR:
		case WebUrl.ASR:
		case WebUrl.MAGHRIB:
		case WebUrl.ISHA:
				reminderIntent = new Intent(context, AlramBroadCastReceiver.class);
				reminderIntent.putExtra("Message", "20 minutes remaining for "+namaz+" Iqamah at Masjid.");
				pendingReminderIntent = PendingIntent.getBroadcast(
						context, (namazId * 100) + 20, reminderIntent, 0);
				alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
			} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
			} else {
				alarmManager.set(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
			}


			break;
			
		case WebUrl.SUNRISE:
			reminderIntent = new Intent(context, AlramBroadCastReceiver.class);
			reminderIntent.putExtra("Message", "20 minutes remaining for "+namaz+".");
			pendingReminderIntent = PendingIntent.getBroadcast(
					context, (namazId * 100) + 20, reminderIntent, 0);
			alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
			} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
			} else {
				alarmManager.set(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
			}
			System.out.println(namaz+"Setted"+(namazId * 100) + 20);
			break;

			case WebUrl.DHUHR:
				int dow = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
				if (dow != Calendar.FRIDAY) {
						reminderIntent = new Intent(context, AlramBroadCastReceiver.class);
						reminderIntent.putExtra("Message", "20 minutes remaining for " + namaz + " Iqamah at Masjid.");
						pendingReminderIntent = PendingIntent.getBroadcast(
								context, (namazId * 100) + 20, reminderIntent, 0);
						alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
						alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
					} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
					} else {
						alarmManager.set(AlarmManager.RTC_WAKEUP, timeMiniSec - 1000 * 60 * 20, pendingReminderIntent);
					}

				}
				break;
		default:
			break;  
		}
		
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		
		}else
		{
			Log.e("Time is", "Time is null");
		}
	}
	public static void  deleteAlarm(Context context,int namazId) {
		PendingIntent pendingAlarmIntent ;
		switch (namazId) {

		case WebUrl.FAJR:
		case WebUrl.SUNRISE:
		case WebUrl.DHUHR:
		case WebUrl.ASR:
		case WebUrl.MAGHRIB:
		case WebUrl.ISHA:
			pendingAlarmIntent = PendingIntent.getService(
					context, (namazId * 100) + 20, new Intent(context,
							AlramBroadCastReceiver.class), 0);
			((AlarmManager) context.getSystemService(context.ALARM_SERVICE))
					.cancel(pendingAlarmIntent);
			System.out.println("deleted"+(namazId * 100) + 20);
			break;
		default:
			break;  
		}
	}
}
