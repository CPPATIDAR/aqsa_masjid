package com.aqsamasjid.utill;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class SalatDataBase {
	
	Context context;
//	Activity activity;
	
	private SQLiteDatabase database;
	private MySqlLiteHelper dbHelper;

	private String[] allColumns = { MySqlLiteHelper.COLUMN_ID, MySqlLiteHelper.MASJID_ID, MySqlLiteHelper.P_MONTH, MySqlLiteHelper.P_YEAR,
			MySqlLiteHelper.P_DATE, MySqlLiteHelper.FAJR, MySqlLiteHelper.SUNRISE, MySqlLiteHelper.DHUHR, MySqlLiteHelper.ASR, MySqlLiteHelper.MAGHRIB,
			MySqlLiteHelper.ISHA, MySqlLiteHelper.ADAN_FAJR, MySqlLiteHelper.ADAN_DHUHR, MySqlLiteHelper.ADAN_ASR,
			MySqlLiteHelper.ADAN_MAGHRIB, MySqlLiteHelper.ADAN_ISHA, MySqlLiteHelper.ADAN_FAJR_PLACE,
			MySqlLiteHelper.ADAN_DHUHR_PLACE, MySqlLiteHelper.ADAN_ASR_PLACE, MySqlLiteHelper.ADAN_MAGHRIB_PLACE,
			MySqlLiteHelper.ADAN_ISHA_PLACE};

	private String[] F_allColumns = { MySqlLiteHelper.F_ID, MySqlLiteHelper.F_DATE, MySqlLiteHelper.F_KHUTBA, MySqlLiteHelper.F_SALAT_TIME,
			MySqlLiteHelper.F_REMARK, MySqlLiteHelper.F_MASJID_ID, MySqlLiteHelper.F_NAMAZ_PLACE};

	private String[] Ramdan_allColumns = { MySqlLiteHelper.Ramdan_ID, MySqlLiteHelper.Ramdan_Date, MySqlLiteHelper.Ramdan_Day, MySqlLiteHelper.Sehr_Time,
			MySqlLiteHelper.Eftar_Time, MySqlLiteHelper.Taravi_Time};


	private String[] Tasbi_allColumns = { MySqlLiteHelper.COLUMN_ID, MySqlLiteHelper.COLUMN_TASBI_ID, MySqlLiteHelper.COLUMN_TASBI_NAME, MySqlLiteHelper.COLUMN_TASBI_COUNT};

	private String[] Tasbi_count = {MySqlLiteHelper.COLUMN_TASBI_COUNT};

	public SalatDataBase(Context context) {
		dbHelper = new MySqlLiteHelper(context);
		this.context = context;
	}

	public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	  }

	public void close() {
	    dbHelper.close();
	  }

	 public void insertToTable(String masjid_id, String month,String year ,String date, String fajr, String sunrise, String dhuhr, String asr,
			 String maghrib, String isha, String fajrAdan, String fajrAdanPlace, String dhuhrAdan,
			 String dhuhrAdanPlace, String AsrAdan, String AsrAdanPlace, String magribAdan,
			 String magribAdanPlace, String ishaAdan, String ishaAdanPlace) {
		    ContentValues values = new ContentValues();
		    values.put(MySqlLiteHelper.MASJID_ID, masjid_id);
		    values.put(MySqlLiteHelper.P_MONTH, month);
		    values.put(MySqlLiteHelper.P_YEAR, year);
		    values.put(MySqlLiteHelper.P_DATE, date);
		    values.put(MySqlLiteHelper.FAJR, fajr);
		    values.put(MySqlLiteHelper.SUNRISE, sunrise);
		    values.put(MySqlLiteHelper.DHUHR, dhuhr);
		    values.put(MySqlLiteHelper.ASR, asr);
		    values.put(MySqlLiteHelper.MAGHRIB, maghrib);
		    values.put(MySqlLiteHelper.ISHA, isha);

		    values.put(MySqlLiteHelper.ADAN_FAJR, fajrAdan);
		    values.put(MySqlLiteHelper.ADAN_FAJR_PLACE, fajrAdanPlace);
		    values.put(MySqlLiteHelper.ADAN_DHUHR, dhuhrAdan);
		    values.put(MySqlLiteHelper.ADAN_DHUHR_PLACE, dhuhrAdanPlace);
		    values.put(MySqlLiteHelper.ADAN_ASR, AsrAdan);
		    values.put(MySqlLiteHelper.ADAN_ASR_PLACE, AsrAdanPlace);
		    values.put(MySqlLiteHelper.ADAN_MAGHRIB, magribAdan);
		    values.put(MySqlLiteHelper.ADAN_MAGHRIB_PLACE, magribAdanPlace);
		    values.put(MySqlLiteHelper.ADAN_ISHA, ishaAdan);
		    values.put(MySqlLiteHelper.ADAN_ISHA_PLACE, ishaAdanPlace);

		    database = dbHelper.getWritableDatabase();
		    database.insert(MySqlLiteHelper.SALAT_TIME, null,values);

		    Cursor cursor = database.query(MySqlLiteHelper.SALAT_TIME,
		        allColumns,null, null,
		        null, null, null);

		   while (cursor.moveToNext()) {

		}
		    cursor.close();
		    database.close();

	 }

	 public void insertToFridayTable(String id, String date,String khutba ,String juma_time, String remark, String masjid_id,String juma_place) {
		    ContentValues values = new ContentValues();
		    values.put(MySqlLiteHelper.F_ID, masjid_id);
		    values.put(MySqlLiteHelper.F_DATE, date);
		    values.put(MySqlLiteHelper.F_KHUTBA, khutba);
		    values.put(MySqlLiteHelper.F_SALAT_TIME, juma_time);
		    values.put(MySqlLiteHelper.F_REMARK, remark);
		    values.put(MySqlLiteHelper.F_MASJID_ID, masjid_id);
		    values.put(MySqlLiteHelper.F_NAMAZ_PLACE, juma_place);

		    database = dbHelper.getWritableDatabase();
		    database.insert(MySqlLiteHelper.JUMA_TIME, null,values);

		    Cursor cursor = database.query(MySqlLiteHelper.JUMA_TIME,
		        F_allColumns,null, null,
		        null, null, null);
		    Log.v("insert Friday cursor_count",  cursor.getColumnCount()+"");
		   while (cursor.moveToNext()) {

		}
		    cursor.close();
		    database.close();

	 }


	 public void insertToRamdanTable(String id, String date,String day ,String sehr_time, String eftar_time, String taravi_time) {
		    ContentValues values = new ContentValues();
		    values.put(MySqlLiteHelper.Ramdan_ID, id);
		    values.put(MySqlLiteHelper.Ramdan_Date, date);
		    values.put(MySqlLiteHelper.Ramdan_Day, day);
		    values.put(MySqlLiteHelper.Sehr_Time, sehr_time);
		    values.put(MySqlLiteHelper.Eftar_Time, eftar_time);
		    values.put(MySqlLiteHelper.Taravi_Time, taravi_time);

		    database = dbHelper.getWritableDatabase();
		    database.insert(MySqlLiteHelper.RAMDAN_SCHEDULE, null,values);

		    Cursor cursor = database.query(MySqlLiteHelper.RAMDAN_SCHEDULE,
		        Ramdan_allColumns,null, null,
		        null, null, null);
		   while (cursor.moveToNext()) {
		}
		    cursor.close();
		    database.close();
	 }


	 public void addTasbihat(String tasbi_id, String tasbi_name,String tasbi_count) {
		    ContentValues values = new ContentValues();
		    values.put(MySqlLiteHelper.COLUMN_TASBI_ID, tasbi_id);
		    values.put(MySqlLiteHelper.COLUMN_TASBI_NAME, tasbi_name);
		    values.put(MySqlLiteHelper.COLUMN_TASBI_COUNT, tasbi_count);



		    database = dbHelper.getWritableDatabase();
		    database.insert(MySqlLiteHelper.TABLE_TASBIHAT, null,values);

		    Cursor cursor = database.query(MySqlLiteHelper.TABLE_TASBIHAT,
		    		Tasbi_allColumns,null, null,
		        null, null, null);

		   while (cursor.moveToNext()) {

		}
		    cursor.close();
		    database.close();

	 }

	 public  boolean CheckIsTasbiAlreadyInDBorNot(String tasbi_id) {
		  database = dbHelper.getWritableDatabase();
		 Cursor cursor = database.query(MySqlLiteHelper.TABLE_TASBIHAT,
				 Tasbi_allColumns, MySqlLiteHelper.COLUMN_TASBI_ID +" = " + "'"+tasbi_id+ "'", null,
		        null, null, null);

		        if(cursor.getCount() <= 0){
		            cursor.close();
		            return false;
		        }
		    cursor.close();
		    return true;
		}


	 public void updateTasbihat(String tasbi_id,String tasbi_count) {
		    ContentValues values = new ContentValues();

		    values.put(MySqlLiteHelper.COLUMN_TASBI_COUNT, tasbi_count);



		    database = dbHelper.getWritableDatabase();
		    database.update(MySqlLiteHelper.TABLE_TASBIHAT,values, MySqlLiteHelper.COLUMN_TASBI_ID+" = "+tasbi_id, null);

		    Cursor cursor = database.query(MySqlLiteHelper.TABLE_TASBIHAT,
		    		Tasbi_allColumns,null, null,
		        null, null, null);

		   while (cursor.moveToNext()) {

		}
		    cursor.close();
		    database.close();


	 }


	 public long getTasbihat(String tasbi_id ) {
		  database = dbHelper.getWritableDatabase();
		 long count=0;
		    Cursor cursor = database.query(MySqlLiteHelper.TABLE_TASBIHAT,Tasbi_allColumns, MySqlLiteHelper.COLUMN_TASBI_ID +" = " + "'"+tasbi_id+ "'", null, null, null, null);


		   while (cursor.moveToNext()) {
			    count=cursor.getLong(3);
		}
		    cursor.close();
		    database.close();
			return count;

	 }

	  public Cursor GetAllUserRecord(){
		  database = dbHelper.getWritableDatabase();
		  Cursor cursor = database.query(MySqlLiteHelper.SALAT_TIME,
				  allColumns,null, null,
		          null, null, null);
		  return cursor;
	  }

	  public Cursor GetRamdanAllRecord(){
		  database = dbHelper.getWritableDatabase();
		  Cursor cursor = database.query(MySqlLiteHelper.RAMDAN_SCHEDULE, Ramdan_allColumns,null, null,
		          null, null, null);
		  return cursor;
	  }

	 public void deleteAllRecords(){
		  database = dbHelper.getWritableDatabase();
		  database.delete(MySqlLiteHelper.SALAT_TIME, null, null);
		  database.delete(MySqlLiteHelper.JUMA_TIME, null, null);
		  database.close();
	  }

	 public void deleteRamdanRecords(){
		  database = dbHelper.getWritableDatabase();
		  database.delete(MySqlLiteHelper.RAMDAN_SCHEDULE, null, null);
		  database.close();
	  }

	 public Cursor getSalatTimeDay(String todayDate){
		database = dbHelper.getWritableDatabase();
		Cursor cursor = database.query(MySqlLiteHelper.SALAT_TIME,
				allColumns, MySqlLiteHelper.P_DATE +" = " + "'"+todayDate+ "'", null,
				null, null, null);
		return cursor;
	}

	 public Cursor getFridayTime(String fridayDate){

		 database = dbHelper.getWritableDatabase();
		 Cursor cursor = database.query(MySqlLiteHelper.JUMA_TIME,
				  F_allColumns, MySqlLiteHelper.F_DATE +" = " + "'"+fridayDate+ "'", null,
		          null, null, null);
		  return cursor;
		  }

	 public Cursor getSalatTimeSwap(String todayDate){
		 database = dbHelper.getWritableDatabase();
		 Cursor cursor = database.query(MySqlLiteHelper.SALAT_TIME,
				  allColumns, MySqlLiteHelper.P_DATE +" >= " + "'"+todayDate+ "'", null,
		          null, null, null);
		 	return cursor; 
	  }
}
