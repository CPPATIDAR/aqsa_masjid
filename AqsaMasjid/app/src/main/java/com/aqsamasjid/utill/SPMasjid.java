package com.aqsamasjid.utill;

import android.content.Context;

public class SPMasjid {
	
	public static final String MASJID_ID = "masjid_id";
	public static final String MASJID_TOKEN = "token";
	public static final String STATUS = "user_status";
	public static final String BaseUrl = "base_url";
	public static final String BaseUrlFlag = "base_url_flag";
	public static final String MasjidSyncDate = "masjid_sync_date";
	public static final String PrayerSyncDate = "prayer_sync_date";
	public static final String RamdanSyncDate = "ramdan_sync_date";
	public static final String IS_RAMADAN = "is_ramadan";
	public static final String CallUserSetting = "call_user_setting";
	public static final String Masjid_Name= "masjid_name";
	public static final String SubscribeNews= "subscribe_news";
	public static final String SubscribeName= "subscribe_name";
	public static final String SubscribeEmail= "subscribe_email";
	public static final String DEVICE_ID= "device_id";
	public static final String AllNoti = "all_noti";
	public static final String PrayerNoti = "prayer_noti";
	public static final String EventNoti = "event_noti";
	public static final String Broad_Cast_Noti = "broad_cast_noti";
	public static final String Announce_Noti = "announce_noti";
	public static final String Sound_Noti = "sound_noti";
	public static final String ThemeSyncDate = "theme_sync_date";
	public static final String ServiceSyncDate = "service_sync_date";
	
	public static final String URL_Youtube_Stream = "you_tube_stream";
	public static final String URL_Youtube_CHANNEL = "you_tube_channel";
	public static final String URL_FACEBOOK = "facebook_url";
	public static final String URL_TWITTER = "twitter_url";
	public static final String GET_DIRECTION = "get_direction";
	public static final String RAMDAN_COUNTDOWN_FLAG = "countdown_flag";
	public static final String ADHAN_LABEL = "adhan_label";
	public static final String IQAMAH_LABEL = "iqamah_label";
	public static final String KHUTBA_LABEL = "khutba_label";
	public static final String SALAT_LABEL = "salat_label";
	public static final String BANNER_IMAGE_ARRAYLIST = "banner_image_arraylist";
	

	public static final String LATITUDE = "lattitude";
	public static final String LONGITUDE = "longitude";
	public static final String BANNER_IMAGE = "banner_image";
	public static final String MASJID_ARRAY = "masjidArray";
	public static final String MASJID_ARRAY_COUNT = "masjidArray_count";
	public static final String NAME = "name";
	public static final String STREET = "street";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String ZIPCODE = "zipcode";
	public static final String ISLAMIC_DATE_OFFSET= "islamic_date_offset";
	public static final String IS_SHOW_ISLAMIC_DATE= "is_show_islamic_date";
	public static final String RAMDAN_LABEL = "ramdan_label";
	public static final String RAMDAN_DATE = "ramdan_date";
	public static final String RAMDAN_DATE_ORIGINAL = "original_ramdanstart_date";
	public static final String MOBLIE_NO= "mobile";
	
//	public static final String Parse_Installation_ID = "parse_install_id";
	public static final String GCM_Installation_ID = "gcm_install_id";
	public static final String FCM_ANDROID_TOKEN = "android_fcm_token";
	public static final String TOKEN_CHECK = "token_check";

//	public static final String EMAIL= "email";
	public static final String FAJR_TEXT= "Fajr";
	public static final String SUNRISE_TEXT= "Sunrise";
	public static final String DHUHR_TEXT= "Zawal";
	public static final String ASR_TEXT= "Asr";
	public static final String MAGRID_TEXT= "Magrib";
	public static final String ISHA_TEXT= "Isha";
	public static final String SEHR_TIME= "sehr_time";
	public static final String IFTAR_TIME= "iftar_time";
	public static final String TARAVI_TIME= "travi_time";
	
	public static final String DATABASE_VERSION = "version1";
	
	
	
	public static final String header_BackColor= "header_BackColor";
	public static final String header_ForeColor= "header_ForeColor";
	public static final String home_header_navigation_BackColor= "home_header_navigation_BackColor";
	public static final String home_header_navigation_ForeColor= "home_header_navigation_ForeColor";
	public static final String home_banner_navigation_BackColor= "home_banner_navigation_BackColor";
	public static final String home_banner_navigation_ForeColor= "home_banner_navigation_ForeColor";
	public static final String home_fajr_prayer_BackColor= "home_fajr_prayer_BackColor";
	public static final String home_fajr_prayer_ForeColor= "home_fajr_prayer_ForeColor";
	
	
	public static final String home_sunrise_prayer_BackColor= "home_sunrise_prayer_BackColor";
	public static final String home_sunrise_prayer_ForeColor= "home_sunrise_prayer_ForeColor";
	public static final String home_sunrise_prayer_BackAlpha= "home_sunrise_prayer_BackAlpha";
	public static final String home_dhuhr_prayer_BackColor= "home_dhuhr_prayer_BackColor";
	public static final String home_dhuhr_prayer_ForeColor= "home_dhuhr_prayer_ForeColor";
	public static final String home_dhuhr_prayer_BackAlpha= "home_dhuhr_prayer_BackAlpha";
	public static final String home_asr_prayer_BackColor= "home_asr_prayer_BackColor";
	public static final String home_asr_prayer_ForeColor= "home_asr_prayer_ForeColor";
	public static final String home_asr_prayer_BackAlpha= "home_asr_prayer_BackAlpha";
	public static final String home_maghrib_prayer_BackColor= "home_maghrib_prayer_BackColor";
	public static final String home_maghrib_prayer_ForeColor= "home_maghrib_prayer_ForeColor";
	public static final String home_maghrib_prayer_BackAlpha= "home_maghrib_prayer_BackAlpha";
	public static final String home_remain_fajr_prayer_ForeColor= "home_remain_fajr_prayer_ForeColor";
	public static final String home_remain_sunrise_prayer_ForeColor= "home_remain_sunrise_prayer_ForeColor";
	public static final String home_remain_dhuhr_prayer_ForeColor= "home_remain_dhuhr_prayer_ForeColor";
	public static final String home_remain_asr_prayer_ForeColor= "home_remain_asr_prayer_ForeColor";
	
	public static final String home_remain_maghrib_prayer_ForeColor= "home_remain_maghrib_prayer_ForeColor";
	public static final String home_isha_prayer_BackColor= "home_isha_prayer_BackColor";
	public static final String home_isha_prayer_ForeColor= "home_isha_prayer_ForeColor";
	public static final String home_remain_isha_prayer_ForeColor= "home_remain_isha_prayer_ForeColor";	
	public static final String home_jumuah_prayer_BackColor= "home_jumuah_prayer_BackColor";
	
	public static final String home_jumuah_prayer_ForeColor= "home_jumuah_prayer_ForeColor";
	public static final String home_btn_prayer_BackColor= "home_btn_prayer_BackColor";
	public static final String home_btn_prayer_ForeColor= "home_btn_prayer_ForeColor";
	public static final String home_btn_event_BackColor= "home_btn_event_BackColor";	
	public static final String home_btn_event_ForeColor= "home_btn_event_ForeColor";
	
	public static final String home_btn_services_BackColor= "home_btn_services_BackColor";
	public static final String home_btn_services_ForeColor= "home_btn_services_ForeColor";	
	public static final String home_btn_donation_BackColor= "home_btn_donation_BackColor";
	public static final String home_btn_donation_ForeColor= "home_btn_donation_ForeColor";
	public static final String inner_header_navigation_BackColor= "inner_header_navigation_BackColor";
	
	public static final String inner_header_navigation_ForeColor= "inner_header_navigation_ForeColor";
	public static final String application_btn_BackColor= "application_btn_BackColor";	
	public static final String application_btn_ForeColor= "application_btn_ForeColor";
	public static final String rsvp_btn_BackColor= "rsvp_btn_BackColor";
	public static final String rsvp_btn_ForeColor= "rsvp_btn_ForeColor";
	
	
	public static final String donate_btn_ForeColor= "donate_btn_ForeColor";
	public static final String donate_btn_BackColor= "donate_btn_BackColor";

	
	public static final String header_BackAlpha= "header_BackAlpha";
	public static final String home_header_navigation_BackAlpha= "home_header_navigation_BackAlpha";
	public static final String home_banner_navigation_BackAlpha= "home_banner_navigation_BackAlpha";
	public static final String home_fajr_prayer_BackAlpha= "home_fajr_prayer_BackAlpha";
	public static final String home_isha_prayer_BackAlpha= "home_isha_prayer_BackAlpha";
	public static final String home_jumuah_prayer_BackAlpha= "home_jumuah_prayer_BackAlpha";
	
	public static final String home_btn_prayer_BackAlpha= "home_btn_prayer_BackAlpha";
	public static final String home_btn_event_BackAlpha= "home_btn_event_BackAlpha";
	public static final String home_btn_services_BackAlpha= "home_btn_services_BackAlpha";
	public static final String home_btn_donation_BackAlpha= "home_btn_donation_BackAlpha";	
	public static final String inner_header_navigation_BackAlpha= "inner_header_navigation_BackAlpha";
	public static final String application_btn_alpha= "application_btn_alpha";
	public static final String donate_btn_alpha= "donate_btn_alpha";
	
	public static final String rsvp_btn_alpha= "rsvp_btn_alpha";
	
	public static final String DONATION_TYPE = "donation_type";



	
	
	
	public static long getLongValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getLong(key, 0);
	}

	public static void setLongValue(Context context, String key, Long value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putLong(key, value).commit();
	}
	
	
	
	public static String getValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getString(key, "");
	}

	public static void setValue(Context context, String key, String value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putString(key, value).commit();
	}

	public static int getIntValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getInt(key, 0);
	}

	public static void setIntValue(Context context, String key, int value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putInt(key, value).commit();
	}

	public static boolean getBooleanValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getBoolean(key, false);
	}

	public static void setBooleanValue(Context context, String key,
			boolean value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putBoolean(key, value).commit();
	}

	
	public static float getFloatValue(Context context, String key) {

		return context.getSharedPreferences("USER", Context.MODE_PRIVATE)
				.getFloat(key, 0.0f);
	}

	public static void setFloatValue(Context context, String key,
			float value) {

		context.getSharedPreferences("USER", Context.MODE_PRIVATE).edit()
				.putFloat(key, value).commit();
	}
	
}
