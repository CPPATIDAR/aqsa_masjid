package com.aqsamasjid.utill;


public class WebUrl {
	public static final int ANDROID_APP_VERSION = 1;
	public static String BaseUrl = "https://api.masjidfundly.com/webservices/";
	public static final String MasjidUpdate = BaseUrl+"masjidUpdates";
	public static final String GetMasjidUrl = BaseUrl+"getMasjid";
	public static final String ServiceUrl = BaseUrl+"Services";
	public static final String EventUrl = BaseUrl+"Events";
	public static final String PrayerTimeUrl = BaseUrl+"prayerTimings";
	public static final String SignUpVoluentierUrl = BaseUrl+"SignupVoluentter";
	public static final String RSPVNow = BaseUrl+"insertRSPV";
	public static final String RamdanScheduleTiming = BaseUrl+"ramdanTimeing";
	public static final String JoinNewsLetter = BaseUrl+"subscribeNewsletter";
	public static final String UserSetting = BaseUrl+"userSetting";
	public static final String DonationComplete = BaseUrl+"donationComplete";
	public static final String ANNOUNCEMENT = BaseUrl+"announcement";
	public static final String checkSyncStatus = BaseUrl+"checkSyncStatus";
	public static final String GetPaymentDetail = BaseUrl+"getPayPalSetting";
	
	public static final String SaveAskImamQuestion = BaseUrl+"saveAskImamQuestion";
	public static final String GetAllQuestionAnswer = BaseUrl+"getAskImamQuestion";
	public static final String GetCategory = BaseUrl+"getCategory";
	public static final String ABOUT_US = BaseUrl+"aboutUs";
	public static final String GetContactSubject = BaseUrl+"getContactSubjects";
	public static final String Save_ContactAS = BaseUrl+"contactRequest";
	public static final String GET_THEME_SETTING= BaseUrl+"getThemeSetting";
	public static final String GET_MASJID_LOCAL_BUSINESS= BaseUrl+"getMasjidLocalBusinesses";
	public static final String GET_TASBI_URL = BaseUrl+"getTasbihatName";

	public static final int MASJID_ID = 52;
	public static final String TOKEN_KEY= "Xu6lPjRcxYcJf5qxzywB6vlDAkJ7c68U";
	public static final String COPYRIGHT_URL= "";
	public static int TimerTenMinute = 600000;
	public static final String SENDER_ID="221247087586";
	public static final String ALL_NOTIFICATION= "All_Notification";
	public static final String PRAYER_NOTIFICATION= "Prayer_Notification";
	public static final String EVENT_NOTIFICATION= "Event_Notification";
	public static final String BROAD_CAST_NOTIFICATION= "Broad_Cast_Notification";
	public static final String ANNOUNCEMENT_NOTIFICATION= "Announcement_Notification";
	public static final String UNSUBSCRIBE_ME= "Unsubscribe_Me";
	public static final String GET_HEADER_INFO= BaseUrl+"getHeaderInfo";
	public static final String DONATION_PAYMENT= "https://api.masjidfundly.com/donation/donate/"+TOKEN_KEY+"/"+MASJID_ID+"?";

	public static final int FAJR = 100000;
	public static final int SUNRISE = 100001;
	public static final int DHUHR = 100002;
	public static final int ASR = 100003;
	public static final int MAGHRIB = 100004;
	public static final int ISHA = 100005;


}
