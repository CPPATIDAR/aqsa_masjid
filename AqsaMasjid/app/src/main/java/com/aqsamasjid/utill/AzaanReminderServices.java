package com.aqsamasjid.utill;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.aqsamasjid.R;
import com.aqsamasjid.ResetAlarms;
import com.aqsamasjid.SplashActivity;

import java.util.Random;

import static com.aqsamasjid.AqsaMasjid.CHANNEL_ID;


public class AzaanReminderServices extends Service {

	@Override
	public void onCreate() {
		// Toast.makeText(this, "MyAlarmService.onCreate()",
		// Toast.LENGTH_LONG).show();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Toast.makeText(this, "MyAlarmService.onDestroy()",
		// Toast.LENGTH_LONG).show();
	}


	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		
		try {
		
		Bundle extras = intent.getExtras();
		if (extras!=null) {
			
		String data = extras.getString("Message");
		System.out.println("Data===="+data);

		System.out.println("AzaanReminderServices is called ");

			String title = getApplicationContext().getString(R.string.app_name);
			Intent notificationIntent = new Intent(
					getApplicationContext(), SplashActivity.class);
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);

			PendingIntent pendingIntent = PendingIntent.getActivity(
					getApplicationContext(), 0, notificationIntent, 0);

			NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
			builder.setSmallIcon(R.drawable.ic_launcher)
					.setContentTitle(title)
					.setContentText(data)
					.setPriority(NotificationCompat.PRIORITY_HIGH)
					.setContentIntent(pendingIntent)
					.setAutoCancel(true);

			NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
			notificationManager.notify(new Random().nextInt(), builder.build());


		ResetAlarms.reset(AzaanReminderServices.this);

		}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
