package com.aqsamasjid.utill;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.security.KeyStore;
import java.util.ArrayList;

public class CustomHttpClient {



	public static HttpClient getNewHttpClient() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new IgnoreCertsSSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

	public static String executeHttpPost(String url,
			ArrayList<NameValuePair> postParameters) {
		try {
			HttpClient client = getNewHttpClient(); //new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setEntity(new UrlEncodedFormEntity(postParameters));
			HttpResponse result = client.execute(post);
			
			
			
			String value = EntityUtils.toString(result.getEntity());
			String s = "";
			for (NameValuePair param : postParameters) {
				s = s + param.getName() + " = " + param.getValue() + " ";
			}
			if (value != null) {
				System.out.println("From " + url + " paramertes " + s + " get "
						+ value);
				return value;
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static String executeHttpGet(String url) {
		
			try {
				HttpClient client = getNewHttpClient(); //new DefaultHttpClient();
				HttpGet get = new HttpGet(url);
				HttpResponse result = client.execute(get);
				String value = EntityUtils.toString(result.getEntity());
				
				
				if (value != null) {
					System.out.println("From " + url + " get " + value);
					return value;
				}else{
					System.out.println("From " + url + " get null"  );
					return null;
				}
			} catch (Exception e) {
				System.out.println("From " + url + " get Exception" + e.getMessage());
				e.printStackTrace();
				
				System.out.print(e.getMessage());
				return null;
			}
				
	
	}
}