package com.aqsamasjid.utill;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySqlLiteHelper extends SQLiteOpenHelper{

	public static final String SALAT_TIME = "salat_time";
	public static final String JUMA_TIME = "juma_time";
	public static final String RAMDAN_SCHEDULE = "ramdan_schedule";
	public static final String TABLE_TASBIHAT = "tbl_tasbihat";


	public static final String COLUMN_ID = "id";
	public static final String MASJID_ID = "tbl_masjid_id";
	public static final String P_MONTH = "p_month";
	public static final String P_YEAR = "p_year";
	public static final String P_DATE = "p_date";
	public static final String FAJR = "fajr";
	public static final String SUNRISE = "sunrise";
	public static final String DHUHR = "dhuhr";
	public static final String ASR = "asr";
	public static final String MAGHRIB = "maghrib";
	public static final String ISHA = "isha";

	public static final String F_ID = "id";
	public static final String F_DATE = "p_date";
	public static final String F_KHUTBA = "khutba";
	public static final String F_SALAT_TIME = "salat_time";
	public static final String F_REMARK = "remark";
	public static final String F_MASJID_ID = "tbl_masjid_id";
	public static final String F_NAMAZ_PLACE = "juma_place";

	public static final String Ramdan_ID = "ramadan_id";
	public static final String Ramdan_Date = "ramdan_date";
	public static final String Ramdan_Day = "ramdan_day";
	public static final String Sehr_Time = "sehr_time";
	public static final String Eftar_Time = "eftar_time";
	public static final String Taravi_Time = "taravi_time";

	public static final String ADAN_FAJR = "adan_fajr";
	public static final String ADAN_DHUHR = "adan_dhuhr";
	public static final String ADAN_ASR = "adan_asr";
	public static final String ADAN_MAGHRIB = "adan_maghrib";
	public static final String ADAN_ISHA = "adan_isha";

	public static final String ADAN_FAJR_PLACE = "adan_fajr_place";
	public static final String ADAN_DHUHR_PLACE = "adan_dhuhr_place";
	public static final String ADAN_ASR_PLACE = "adan_asr_place";
	public static final String ADAN_MAGHRIB_PLACE = "adan_maghrib_place";
	public static final String ADAN_ISHA_PLACE = "adan_isha_place";

	public static final String COLUMN_TASBI_ID = "tasbi_id";
	public static final String COLUMN_TASBI_NAME = "tasbi_name";
	public static final String COLUMN_TASBI_COUNT = "tasbi_count";


	private static final String DATABASE_NAME = "MasjidApp.db";
	private static final int DATABASE_VERSION = 2;
	private Context context;

	public MySqlLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	private static final String DATABASE_CREATE = "create table IF NOT EXISTS "
			+ SALAT_TIME + "(" + COLUMN_ID
			+ " integer primary key autoincrement,"+ MASJID_ID + " text not null," + P_MONTH + " text not null," + P_YEAR + " text not null, "
			+ P_DATE + " date, " + FAJR
			+ " text not null, " + SUNRISE
			+ " text not null, " + DHUHR
			+ " text not null, " + ASR
			+ " text not null, " + MAGHRIB
			+ " text not null, " + ISHA
			+ " text not null);";

	private static final String DATABASE_FRIDAY = "create table IF NOT EXISTS "
			+ JUMA_TIME + "("+ F_ID + " text not null," + F_DATE + " text not null," + F_KHUTBA + " text not null, "
			+ F_REMARK+ " text not null, "
			+ F_SALAT_TIME + " text not null, "
			+ F_MASJID_ID + " text not null );";

	private static final String DATABASE_RAMDAN = "create table IF NOT EXISTS "
			+ RAMDAN_SCHEDULE + "("+ Ramdan_ID + " text not null," + Ramdan_Date + " text not null," + Ramdan_Day + " text not null, "
			+ Sehr_Time+ " text not null, "
			+ Eftar_Time + " text not null, "
			+ Taravi_Time + " text not null );";

	private static final String DATABASE_TASBIHAT= "create table IF NOT EXISTS "
			+ TABLE_TASBIHAT + "(" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_TASBI_ID + " text not null," + COLUMN_TASBI_NAME + " text not null, " + COLUMN_TASBI_COUNT + " text not null);";



	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DATABASE_CREATE);
		db.execSQL(DATABASE_FRIDAY);
		db.execSQL(DATABASE_RAMDAN);
		db.execSQL(DATABASE_TASBIHAT);
	}

	public void alterTable(){
		SQLiteDatabase db = getWritableDatabase();
		//db.execSQL(ALTER_SALAT_TIME);
		Log.e("ALTERTABLE==","Called");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_fajr` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_dhuhr` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_asr` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_maghrib` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_isha` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_fajr_place` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_dhuhr_place` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_asr_place` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_maghrib_place` VARCHAR(45) NOT NULL DEFAULT ''");
		db.execSQL("ALTER TABLE salat_time ADD COLUMN `adan_isha_place` VARCHAR(45) NOT NULL DEFAULT ''");

		db.execSQL("ALTER TABLE juma_time ADD COLUMN `juma_place` VARCHAR(45) NOT NULL DEFAULT ''");
		SPMasjid.setIntValue(context, SPMasjid.DATABASE_VERSION, 2);
	}


	public void deleteTable(){
		SQLiteDatabase db = getWritableDatabase();
		//db.execSQL(ALTER_SALAT_TIME);
		db.execSQL("delete from salat_time");
		db.execSQL("delete from juma_time");
		SPMasjid.setIntValue(context, SPMasjid.DATABASE_VERSION, 2);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		Log.w(MySqlLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");

		onCreate(db);

	}



}
