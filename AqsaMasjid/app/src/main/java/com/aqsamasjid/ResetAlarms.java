package com.aqsamasjid;

import android.content.Context;
import android.database.Cursor;

import com.aqsamasjid.utill.NamazReminder;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.SalatDataBase;
import com.aqsamasjid.utill.WebUrl;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ResetAlarms {

	 private static	String FajrTime;
	 private static	String SunriseTime;
	 private static String DhuhrTime;
	 private static String AsrTime;
	 private static String MaghribTime;
	 private static String IshaTime;
	 static SalatDataBase salatDataBase;
	 private static String todayDate;
	 
	 
	public static void reset(Context context) {
		
		salatDataBase = new SalatDataBase(context);
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		todayDate = dateformat.format(cal.getTime());
		Cursor cur = salatDataBase.getSalatTimeDay(todayDate);

			if (cur.moveToNext()) {

				// Set Notification from Adhan timing
			/*	FajrTime = cur.getString(11);
				SunriseTime = cur.getString(6);
				DhuhrTime = cur.getString(12);
				AsrTime = cur.getString(13);
				MaghribTime = cur.getString(14);
				IshaTime = cur.getString(15);*/

				// Set Notification from Iqamah timing
				FajrTime = cur.getString(5);
				SunriseTime = cur.getString(6);
				DhuhrTime = cur.getString(7);
				AsrTime = cur.getString(8);
				MaghribTime = cur.getString(9);
				IshaTime = cur.getString(10);
			}


		cur.close();
		
		if (!SPMasjid.getBooleanValue(context, SPMasjid.PrayerNoti)) {
            System.out.println("FROM RESETALARAMS");
			NamazReminder.setAlarm(context,"Fajr", FajrTime, WebUrl.FAJR);
			NamazReminder.setAlarm(context,"Sunrise", SunriseTime, WebUrl.SUNRISE);
			NamazReminder.setAlarm(context,"Dhuhr", DhuhrTime, WebUrl.DHUHR);
			NamazReminder.setAlarm(context,"Asr", AsrTime, WebUrl.ASR);
			NamazReminder.setAlarm(context,"Maghrib", MaghribTime, WebUrl.MAGHRIB);
			NamazReminder.setAlarm(context,"Isha", IshaTime, WebUrl.ISHA);
		}
		
	}
}
