package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.Webimageloader.ImageLoader;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class AnnouncementDetailActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private TextView tvProgramTitle;
//	private TextView tvProgramDetail;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private Button btnRegister;
	private Dialog dialog;
	private EditText etFirstname;
	private EditText etEmail;
	private EditText etMobile;
	private TextView tvTExtHeader;
	private NetworkConnection networkConnection;
	private ImageLoader webImageLoader;
	private ImageView ivBannerImage;
	private WebView webView;
	private TextView tvAppLabel;
	private TextView tvProgramText;
	private LinearLayout llProgramTitleContainer;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_announcement_detail);
		initComponents();
		initListiners();
		initSideBar();

		Intent intent = getIntent();
		tvProgramTitle.setText(intent.getStringExtra("Title"));
//		tvProgramDetail.setText(intent.getStringExtra("Detail"));
		webView.loadData(intent.getStringExtra("Detail"), "text/html", "UTF-8");
		WebSettings webSettings = webView.getSettings();
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultFontSize((int)20);
		
		webImageLoader = new ImageLoader(AnnouncementDetailActivity.this);

		try {
			String imageUrl = intent.getStringExtra("Banner_Image").toString();
			webImageLoader.DisplayImage(imageUrl, ivBannerImage);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		/*Intent intent = new Intent(AnnouncementDetailActivity.this,
				AnnouncementActivity.class);
		startActivity(intent);*/
		finish();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		tvProgramTitle = (TextView) findViewById(R.id.tv_program_title);
//		tvProgramDetail = (TextView) findViewById(R.id.tv_program_detail);
		btnRegister = (Button) findViewById(R.id.btn_register_volunter_program);
		ivBannerImage = (ImageView) findViewById(R.id.iv_banner_program_detail);
		tvProgramText= (TextView) findViewById(R.id.tv_program_text);
		llProgramTitleContainer = (LinearLayout) findViewById(R.id.ll_program_title_container);
//		tvProgramDetail.setMovementMethod(new ScrollingMovementMethod());
		webView = (WebView) findViewById(R.id.webview_announment);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.Masjid_Name));
	
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvProgramText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvProgramText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvProgramText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
	
		
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_BackColor).equalsIgnoreCase(null))) {
			llProgramTitleContainer.setBackgroundColor(Color.parseColor(SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_BackColor)));
		}
		
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_banner_navigation_BackAlpha))>0.0f) {
			llProgramTitleContainer.setAlpha(Float.parseFloat(SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha)));

		}
		
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor).equalsIgnoreCase(null))) {
			tvProgramTitle.setTextColor(Color.parseColor(SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor)));
		
		}
		
		
		
		
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.application_btn_BackColor).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.application_btn_BackColor).equalsIgnoreCase(null)))
		{
			btnRegister.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_BackColor)));
		}
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.application_btn_alpha).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.application_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha))>0.0f)
		{
			btnRegister.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha)));
		}
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.application_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.application_btn_ForeColor).equalsIgnoreCase(null)))
		{
			btnRegister.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_ForeColor)));
		}
		
		
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent intent = new
				// Intent(AnnouncementDetailActivity.this,HomeActivity.class);
				// startActivity(intent);
				setResult(RESULT_OK,new Intent(AnnouncementDetailActivity.this, AnnouncementActivity.class));
				finish();
			}
		});
		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AnnouncementDetailActivity.this,
						AnnouncementActivity.class);
				startActivity(intent);
				finish();
			}
		});*/
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent intent = new
				// Intent(AnnouncementDetailActivity.this,HomeActivity.class);
				// startActivity(intent);
				setResult(RESULT_OK,new Intent(AnnouncementDetailActivity.this, AnnouncementActivity.class));

				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AnnouncementDetailActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AnnouncementDetailActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getIntValue(AnnouncementDetailActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(AnnouncementDetailActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(AnnouncementDetailActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(AnnouncementDetailActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
		btnRegister.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialogForRegistration();
			}
		});
	}

	protected void dialogForRegistration() {
		// TODO Auto-generated method stub

		dialog = new Dialog(AnnouncementDetailActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_registration);
		tvTExtHeader = (TextView) dialog.findViewById(R.id.tv_text_header);
		etFirstname = (EditText) dialog.findViewById(R.id.et_name);
		etEmail = (EditText) dialog.findViewById(R.id.et_email);
		etMobile = (EditText) dialog.findViewById(R.id.et_mobile);
		Button btnSubmit = (Button) dialog.findViewById(R.id.btn_submit);
		Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancal);

		tvTExtHeader.setText("Become a volunteer");
		
		
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.header_BackColor).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.header_BackColor).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));
			btnSubmit.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));
			btnCancel.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));

		}
		
		
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.header_BackAlpha).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.header_BackAlpha).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
			btnSubmit.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
			btnCancel.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));

		}
		
		if (SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.header_ForeColor).length()!=0&&(!SPMasjid.getValue(AnnouncementDetailActivity.this, SPMasjid.header_ForeColor).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));
			btnSubmit.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));
			btnCancel.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));

		}
		

		dialog.show();

		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (etFirstname.getText().toString().length() == 0) {
					showMsgDialog("Please enter first name");
				} else if (etEmail.getText().toString().length() == 0) {
					showMsgDialog("Please enter E-mail.");
				} else if (!EmailValidator
						.isValid(etEmail.getText().toString())) {
					showMsgDialog("please enter valid E-mail.");
				} else if (etMobile.getText().toString().length() == 0) {
					showMsgDialog("Please enter phone number.");
				} else if (etMobile.getText().toString().trim().length() != 10) {
					showMsgDialog("Please enter valid phone number.");
				} else {
					if (networkConnection.isOnline(getApplicationContext())) {
						new RegistrationAsyncTask().execute();
					} else {
						Toast.makeText(getBaseContext(),
								"Please check your network connection.",
								Toast.LENGTH_SHORT).show();
					}

				}
			}
		});
	}

	public void showMsgDialog(String message) {
		new AlertDialog.Builder(AnnouncementDetailActivity.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

					}
				}).create().show();
	}

	class RegistrationAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("firstname", etFirstname
					.getText().toString()));
			urlParameter.add(new BasicNameValuePair("email", etEmail.getText()
					.toString()));
			urlParameter.add(new BasicNameValuePair("mobile", etMobile
					.getText().toString()));
			urlParameter.add(new BasicNameValuePair("type", "program"));
			progressDialog = new ProgressDialog(AnnouncementDetailActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter Registration", urlParameter + "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					WebUrl.SignUpVoluentierUrl, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					} else {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}

				} else {
					Toast.makeText(AnnouncementDetailActivity.this,
							"Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
