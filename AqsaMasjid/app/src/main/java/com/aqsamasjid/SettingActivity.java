package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NamazReminder;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.SalatDataBase;
import com.aqsamasjid.utill.WebUrl;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

@SuppressLint("NewApi")
public class SettingActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private NetworkConnection networkConnection;
	//private String deviceId;
	private String AllNoti;
	private String EventNoti;
	private String PrayerNoti;
	private String BroadCastNoti;
	private String AnnounceNoti;
	private SalatDataBase salatDataBase ;
	private String todayDate;
	private Calendar currentTime;
	private ImageView ivAllSetting;
	private ImageView ivPrayerSetting;
	private ImageView ivEventSetting;
	private ImageView ivBroadCastSetting;
	private ImageView ivAnnouncementSetting;
	private ImageView ivSoundSetting;
	private TextView tvAppLabel;
	protected String SoundNoti;
	private TextView tvSettingText;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		initComponents();
		initListiners();
		initSideBar();
		/*TelephonyManager manager = (TelephonyManager) getApplicationContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceId = manager.getDeviceId();*/
		
		Log.v("parse id", SPMasjid.getValue(SettingActivity.this, SPMasjid.GCM_Installation_ID));
		
		
		if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.AllNoti)) {
			ivAllSetting.setImageResource(R.drawable.unselect);
			ivPrayerSetting.setImageResource(R.drawable.unselect);
			ivEventSetting.setImageResource(R.drawable.unselect);
			ivBroadCastSetting.setImageResource(R.drawable.unselect);
			ivAnnouncementSetting.setImageResource(R.drawable.unselect);
			ivSoundSetting.setImageResource(R.drawable.unselect);
			AllNoti = "0";
			EventNoti = "0";
			PrayerNoti = "0";
			BroadCastNoti = "0";
			AnnounceNoti = "0";
			SoundNoti = "0";
		} else {
			ivPrayerSetting.setImageResource(R.drawable.select);
			ivEventSetting.setImageResource(R.drawable.select);
			ivBroadCastSetting.setImageResource(R.drawable.select);
			ivAnnouncementSetting.setImageResource(R.drawable.select);
			ivSoundSetting.setImageResource(R.drawable.select);
			AllNoti = "1";
			EventNoti = "1";
			PrayerNoti = "1";
			BroadCastNoti = "1";
			AnnounceNoti = "1";
			SoundNoti = "1";
		}
		
		
		
		if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.PrayerNoti)) {
			ivPrayerSetting.setImageResource(R.drawable.unselect);
			PrayerNoti = "0";
		} else {
			ivPrayerSetting.setImageResource(R.drawable.select);
			PrayerNoti = "1";
		}
		if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.EventNoti)) {
			ivEventSetting.setImageResource(R.drawable.unselect);
			EventNoti = "0";
		} else {
			EventNoti = "1";
			ivEventSetting.setImageResource(R.drawable.select);
		}
		if (SPMasjid.getBooleanValue(SettingActivity.this,	SPMasjid.Broad_Cast_Noti)) {
			ivBroadCastSetting.setImageResource(R.drawable.unselect);
			BroadCastNoti = "0";
			
			
		} else {
			
			BroadCastNoti = "1";
			ivBroadCastSetting.setImageResource(R.drawable.select);
		}
		if (SPMasjid.getBooleanValue(SettingActivity.this,
				SPMasjid.Announce_Noti)) {
			ivAnnouncementSetting.setImageResource(R.drawable.unselect);
			AnnounceNoti = "0";
		} else {
			AnnounceNoti = "1";
			ivAnnouncementSetting.setImageResource(R.drawable.select);
		}
		
		if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.Sound_Noti)) {
			SoundNoti = "0";
			ivSoundSetting.setImageResource(R.drawable.unselect);
		} else {
			SoundNoti = "1";
			ivSoundSetting.setImageResource(R.drawable.select);
		}
	}

	private void initComponents() {
		// TODO Auto-generated method stub
	/*	String refreshedToken = FirebaseInstanceId.getInstance().getToken();
		Log.d("FirebaseIDService", "Refreshed token: " + refreshedToken);
		if(((refreshedToken!=null)&&!refreshedToken.equalsIgnoreCase(""))){
			SPMasjid.setValue(SettingActivity.this, SPMasjid.FCM_ANDROID_TOKEN,refreshedToken+"");
		}*/

		FirebaseInstanceId.getInstance().getInstanceId()
				.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
					@Override
					public void onComplete(@NonNull Task<InstanceIdResult> task) {
						if (!task.isSuccessful()) {
							Log.w("getInstanceId failed", task.getException());
							return;
						}

						// Get new Instance ID token
						String token = task.getResult().getToken();

						SPMasjid.setValue(SettingActivity.this, SPMasjid.FCM_ANDROID_TOKEN,token+"");

					}
				});

		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		
		ivAllSetting = (ImageView) findViewById(R.id.iv_all_setting);
		ivPrayerSetting = (ImageView) findViewById(R.id.iv_prayer_setting);
		ivEventSetting = (ImageView) findViewById(R.id.iv_event_setting);
		ivAnnouncementSetting = (ImageView) findViewById(R.id.iv_announcement_setting);
		ivBroadCastSetting = (ImageView) findViewById(R.id.iv_broad_cast_setting);
		ivSoundSetting = (ImageView) findViewById(R.id.iv_sound_setting);
		tvSettingText = (TextView) findViewById(R.id.tv_setting_text);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(SettingActivity.this, SPMasjid.Masjid_Name));

		salatDataBase = new SalatDataBase(getApplicationContext());
		
		if (SPMasjid.getValue(SettingActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(SettingActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvSettingText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(SettingActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(SettingActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvSettingText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(SettingActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(SettingActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvSettingText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (networkConnection.isOnline(getApplicationContext())) {
			new NotificationAsyncTask().execute();
		} else {
		Toast.makeText(getBaseContext(),
				"Please check your network connection.", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});*/

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SettingActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SettingActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(SettingActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(SettingActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(SettingActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(SettingActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
		
		
		ivAllSetting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.AllNoti)) {
					ivAllSetting.setImageResource(R.drawable.select);
					ivPrayerSetting.setImageResource(R.drawable.select);
					ivEventSetting.setImageResource(R.drawable.select);
					ivBroadCastSetting.setImageResource(R.drawable.select);
					ivAnnouncementSetting.setImageResource(R.drawable.select);
					ivSoundSetting.setImageResource(R.drawable.select);
					
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.AllNoti, false);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.PrayerNoti, false);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.EventNoti, false);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Broad_Cast_Noti, false);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Announce_Noti, false);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Sound_Noti,false);
					
					setSalatTiming();
					
					AllNoti = "1";
					EventNoti = "1";
					PrayerNoti = "1";
					BroadCastNoti = "1";
					AnnounceNoti = "1";
					SoundNoti="1";
				} else {
					ivAllSetting.setImageResource(R.drawable.unselect);
					ivPrayerSetting.setImageResource(R.drawable.unselect);
					ivEventSetting.setImageResource(R.drawable.unselect);
					ivBroadCastSetting.setImageResource(R.drawable.unselect);
					ivAnnouncementSetting.setImageResource(R.drawable.unselect);
					ivSoundSetting.setImageResource(R.drawable.unselect);
					
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.AllNoti, true);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.PrayerNoti, true);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.EventNoti, true);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Broad_Cast_Noti, true);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Announce_Noti, true);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Sound_Noti,true);
					
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.FAJR);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.SUNRISE);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.DHUHR);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.ASR);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.MAGHRIB);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.ISHA);
					
					AllNoti = "0";
					EventNoti = "0";
					PrayerNoti = "0";
					BroadCastNoti = "0";
					AnnounceNoti = "0";
					SoundNoti="0";
					Log.v("AllNoti", AllNoti);
				}
			}
		});
		
		ivPrayerSetting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.PrayerNoti)) {
					ivPrayerSetting.setImageResource(R.drawable.select);
					
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.PrayerNoti, false);
					PrayerNoti = "1";
					Log.v("PrayerNoti", PrayerNoti);
					
					setSalatTiming();
					isAllSelected();
				} else {
					ivPrayerSetting.setImageResource(R.drawable.unselect);
					PrayerNoti = "0";
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.PrayerNoti, true);
					Log.v("PrayerNoti", PrayerNoti);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.FAJR);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.SUNRISE);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.DHUHR);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.ASR);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.MAGHRIB);
					NamazReminder.deleteAlarm(SettingActivity.this, WebUrl.ISHA);
					isAllSelected();
				}
			}
		});
		
		ivEventSetting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.EventNoti)) {
					ivEventSetting	.setImageResource(R.drawable.select);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.EventNoti, false);
					
					EventNoti = "1";
					Log.v("EventNoti", EventNoti);
					isAllSelected();
				} else {
					ivEventSetting.setImageResource(R.drawable.unselect);
					EventNoti = "0";
					Log.v("EventNoti", EventNoti);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.EventNoti, true);
					isAllSelected();
				}
			}
		});
		
		
		ivBroadCastSetting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.Broad_Cast_Noti)) {
					ivBroadCastSetting.setImageResource(R.drawable.select);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Broad_Cast_Noti, false);
					
					BroadCastNoti = "1";
					Log.v("BroadCastNoti", BroadCastNoti);
					isAllSelected();
				} else {
					ivBroadCastSetting.setImageResource(R.drawable.unselect);
					BroadCastNoti = "0";
					Log.v("BroadCastNoti", BroadCastNoti);
					SPMasjid.setBooleanValue(SettingActivity.this,	SPMasjid.Broad_Cast_Noti, true);
					isAllSelected();
				}
			}
		});
		
		ivAnnouncementSetting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.Announce_Noti)) {
					ivAnnouncementSetting.setImageResource(R.drawable.select);
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Announce_Noti, false);
					AnnounceNoti = "1";
					Log.v("AnnounceNoti", AnnounceNoti);
					isAllSelected();
				} else {
					ivAnnouncementSetting.setImageResource(R.drawable.unselect);
					AnnounceNoti = "0";
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Announce_Noti, true);
					Log.v("AnnounceNoti", AnnounceNoti);
					isAllSelected();
				}
			}
		});
		
		
		ivSoundSetting.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getBooleanValue(SettingActivity.this, SPMasjid.Sound_Noti)) {
					ivSoundSetting.setImageResource(R.drawable.select);
					SoundNoti = "1";
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Sound_Noti, false);
					isAllSelected();
				} else {
					ivSoundSetting.setImageResource(R.drawable.unselect);
					SoundNoti = "0";
					SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.Sound_Noti, true);
					isAllSelected();
				}
			}
		});
		
	}

	class NotificationAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("device_id", SPMasjid.getValue(SettingActivity.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("client_type", "android"));
			urlParameter.add(new BasicNameValuePair("device_ios_notification",""));
			urlParameter.add(new BasicNameValuePair("android_gcm_token",
					SPMasjid.getValue(SettingActivity.this, SPMasjid.GCM_Installation_ID)));
			urlParameter.add(new BasicNameValuePair("android_fcm_token", SPMasjid.getValue(SettingActivity.this, SPMasjid.FCM_ANDROID_TOKEN)));
			urlParameter.add(new BasicNameValuePair(
					"device_android_notification", SPMasjid.getValue(SettingActivity.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("all_notification",AllNoti));
			urlParameter.add(new BasicNameValuePair("prayer_notification",
					PrayerNoti));
			urlParameter.add(new BasicNameValuePair("event_notification",
					EventNoti));
			urlParameter.add(new BasicNameValuePair("broadcast_notification",
					BroadCastNoti));
			urlParameter.add(new BasicNameValuePair("annoucment_notification",
					AnnounceNoti));
			Log.v("urlParameter", urlParameter + "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.UserSetting,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {

				if (response != null) {
					Log.v("Response", response);

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

					}
				} else {
					Toast.makeText(SettingActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	private void setSalatTiming() {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		todayDate = dateformat.format(cal.getTime());
		Log.v("Today Datt", todayDate);
		
		Cursor cur = salatDataBase.getSalatTimeDay(todayDate);
		String fajrTime = null;
		String sunriseTime = null;
		String dhuhrTime = null;
		String asrTime = null;
		String maghribTime = null;
		String ishaTime = null;
		
		try {
		
		if (cur.moveToNext()) {
			
			fajrTime = cur.getString(5);
			sunriseTime = cur.getString(6);
			dhuhrTime = cur.getString(7);
			asrTime = cur.getString(8);
			maghribTime = cur.getString(9);
			ishaTime = cur.getString(10);
		}
		cur.close();
		
		Log.v("fajrTime", fajrTime);
		Log.v("sunriseTime", sunriseTime);
		Log.v("dhuhrTime", dhuhrTime);
		Log.v("asrTime", asrTime);
		Log.v("maghribTime", maghribTime);
		Log.v("ishaTime", ishaTime);
		
		NamazReminder.setAlarm(SettingActivity.this, "Fajr", fajrTime, WebUrl.FAJR);
		NamazReminder.setAlarm(SettingActivity.this, "Sunrise", sunriseTime, WebUrl.SUNRISE);
		NamazReminder.setAlarm(SettingActivity.this, "Dhuhr", dhuhrTime, WebUrl.DHUHR);
		NamazReminder.setAlarm(SettingActivity.this, "Asr", asrTime, WebUrl.ASR);
		NamazReminder.setAlarm(SettingActivity.this, "Maghrib", maghribTime, WebUrl.MAGHRIB);
		NamazReminder.setAlarm(SettingActivity.this, "Isha", ishaTime, WebUrl.ISHA);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	
	protected long timeCalculate(String namazTime) {
		long time=0;
		String test = namazTime;
		currentTime = Calendar.getInstance();
		currentTime.setTimeInMillis(System.currentTimeMillis());
		String hour = null, min = null;
		String am_pm = null;
		try {
			String[] arr = test.split(" ");
			am_pm = arr[1];
			
			String strTime[] = arr[0].split(":");
			hour = strTime[0];
			min = strTime[1];
			
			Log.v("Hello3", "" + hour);
			Log.v("Hello4", "" + min);
			Log.v("Hello5", "" + am_pm);
		} catch (Exception e) {
			e.printStackTrace();
		}
//		int intHour = Integer.parseInt(hour);// +12*60*60*1000 ;
//		int intMin = Integer.parseInt(min);

		int intHour;
		if (am_pm.equalsIgnoreCase("AM")) {
			 intHour = Integer.parseInt(hour)+12;
		} else {
			 intHour = Integer.parseInt(hour);
		}
		int intMin = Integer.parseInt(min);
		
		
		
		currentTime.set(Calendar.HOUR_OF_DAY, intHour);
		currentTime.set(Calendar.MINUTE, intMin);
		currentTime.set(Calendar.SECOND, 0);
		time=currentTime.getTimeInMillis();
		Log.v("Salat time", time+"");
		return time;
	}


	private void isAllSelected() {
		if (PrayerNoti.equalsIgnoreCase("1") && EventNoti.equalsIgnoreCase("1")
				&& BroadCastNoti.equalsIgnoreCase("1")
				&& AnnounceNoti.equalsIgnoreCase("1")
				&& SoundNoti.equalsIgnoreCase("1")) {
			AllNoti = "1";
			ivAllSetting.setImageResource(R.drawable.select);
			SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.AllNoti,
					false);
		} else {
			AllNoti = "0";
			ivAllSetting.setImageResource(R.drawable.unselect);
			SPMasjid.setBooleanValue(SettingActivity.this, SPMasjid.AllNoti,
					true);
		}

	}
}