package com.aqsamasjid;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.aqsamasjid.utill.SPMasjid;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@TargetApi(Build.VERSION_CODES.HONEYCOMB) public class MyFragment extends Fragment {

	private TextView tvFajr;
	private TextView tvSunrise;
	private TextView tvDhuhr;
	private TextView tvAsr;
	private TextView tvMaghrib;
	private TextView tvIsha;

	public static final String DATE = "date";
	public static final String FAJR = "fajr";
	public static final String SUNRISE = "sunrise";
	public static final String DHUHR = "dhuhr";
	public static final String ASR = "asr";
	public static final String MAGHRIB = "maghrib";
	public static final String ISHA = "isha";
	public static final String ISTODAY = "is_today";
	
	public static final String FAJR_ADAN = "fajr_adan";
	public static final String DHUHR_ADAN = "dhuhr_adan";
	public static final String ASR_ADAN = "asr_adan";
	public static final String MAGHRIB_ADAN = "maghrib_adan";
	public static final String ISHA_ADAN = "isha_adan";
	
	public static final String FAJR_ADAN_PLACE = "fajr_adan_place";
	public static final String DHUHR_ADAN_PLACE = "dhuhr_adan_place";
	public static final String ASR_ADAN_PLACE = "asr_adan_place";
	public static final String MAGHRIB_ADAN_PLACE = "maghrib_adan_place";
	public static final String ISHA_ADAN_PLACE = "isha_adan_place";
	

	private TextView tvFajrText;
	private TextView tvSunriseText;
	private TextView tvDhuhrText;
	private TextView tvAsrText;
	private TextView tvMaghribText;
	private TextView tvIshaText;
	private Typeface typeface_Normal;

	private LinearLayout llRemainingFajr;
	private LinearLayout llRemainingSunrise;
	private LinearLayout llRemainingDhuhr;
	private LinearLayout llRemainingAsr;
	private LinearLayout llRemainingMaghrib;
	private LinearLayout llRemainingIsha;
	private TextView tvFajrRemainTime;
	private TextView tvSunriseRemainTime;
	private TextView tvDhuhrRemainTime;
	private TextView tvAsrRemainTime;
	private TextView tvMaghribRemainTime;
	private TextView tvIshaRemainTime;
	private TextView tvAdanText;
	private TextView tvIqamaText;
	
	private TextView tvFajrAdan;
	private TextView tvDhuhrAdan;
	private TextView tvAsrAdan;
	private TextView tvMaghribAdan;
	private TextView tvIshaAdan;
	
	private TextView tvFajrAdanPlace;
	private TextView tvDhuhrAdanPlace;
	private TextView tvAsrAdanPlace;
	private TextView tvMaghribAdanPlcae;
	private TextView tvIshaAdanPlace;

	private TextView tvTimer;
	private long nextNamaz = 0;
	private String currentDate;
	private TextView tvTodayDate;
	private TextView tvToday;
	private ImageView ivCalendar;
	private HomeActivity homeActiviy;
	String curDate;
private Typeface typeface_bold;
private RelativeLayout rlDateShow;
private Context context;
private LinearLayout llPanelIsha;
private LinearLayout llPanelFajr;
private TextView tvHijiriDate;
private LinearLayout llDateParent;
private LinearLayout llPanelSunrise;
private TextView tvSunriseAdan;
private LinearLayout llPanelDhuhr;
private LinearLayout llPanelAsr;
private LinearLayout llPanelMaghrib;
	private long fajr;
	private long sunRise;
	private long dhuhr;
	private long asr;
	private long maghrib;
	private long isha;



	public static final MyFragment newInstance(String date, String fajr,
                                               String sunrise, String dhuhr, String asr, String maghrib,
                                               String isha, boolean isToday, String adanFajr, String adanDhuhr, String adanAsr, String adanMaghrib, String adanIsha,
                                               String adanFajrPlace, String adanDhuhrPlace, String adanAsrPlace,
                                               String adanMaghribPlace, String adanIshaPlace) {

/*public static final MyFragment newInstance(String date, String fajr,
		String sunrise, String dhuhr, String asr, String maghrib,
		String isha, boolean isToday) {*/
		MyFragment f = new MyFragment();
		Bundle bdl = new Bundle(1);
		bdl.putString(DATE, date);
		bdl.putString(FAJR, fajr);
		bdl.putString(SUNRISE, sunrise);
		bdl.putString(DHUHR, dhuhr);
		bdl.putString(ASR, asr);
		bdl.putString(MAGHRIB, maghrib);
		bdl.putString(ISHA, isha);
		bdl.putBoolean(ISTODAY, isToday);
		
		bdl.putString(FAJR_ADAN, adanFajr);
		bdl.putString(DHUHR_ADAN, adanDhuhr);
		bdl.putString(ASR_ADAN, adanAsr);
		bdl.putString(MAGHRIB_ADAN, adanMaghrib);
		bdl.putString(ISHA_ADAN, adanIsha);
		bdl.putString(FAJR_ADAN_PLACE, adanFajrPlace);
		bdl.putString(DHUHR_ADAN_PLACE, adanDhuhrPlace);
		bdl.putString(ASR_ADAN_PLACE, adanAsrPlace);
		bdl.putString(MAGHRIB_ADAN_PLACE, adanMaghribPlace);
		bdl.putString(ISHA_ADAN_PLACE, adanIshaPlace);
		f.setArguments(bdl);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (homeActiviy != null) {
			homeActiviy.matchFridaytime(curDate);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// String message = getArguments().getString(FAJR);
		// String msg = getArguments().getString(SUNRISE);
		context=container.getContext();
		homeActiviy = (HomeActivity) getActivity();
		typeface_Normal = Typeface.createFromAsset(getActivity().getAssets(), "arial_0.ttf");
		typeface_bold = Typeface.createFromAsset(getActivity().getAssets(), "arialbd.ttf");
		View v = inflater.inflate(R.layout.myfragment_layout, container, false);
		tvHijiriDate= (TextView) v.findViewById(R.id.tv_hijiri_date);
		tvFajr = (TextView) v.findViewById(R.id.tv_fajr);
		tvSunrise = (TextView) v.findViewById(R.id.tv_sunrise);
		tvDhuhr = (TextView) v.findViewById(R.id.tv_dhuhr);
		tvAsr = (TextView) v.findViewById(R.id.tv_asr);
		tvMaghrib = (TextView) v.findViewById(R.id.tv_maghrib);
		tvIsha = (TextView) v.findViewById(R.id.tv_isha);

		tvFajrText = (TextView) v.findViewById(R.id.tv_fajr_text);
		tvSunriseText = (TextView) v.findViewById(R.id.tv_sunrise_text);
		tvDhuhrText = (TextView) v.findViewById(R.id.tv_dhuhr_text);
		tvAsrText = (TextView) v.findViewById(R.id.tv_asr_text);
		tvMaghribText = (TextView) v.findViewById(R.id.tv_maghrib_text);
		tvIshaText = (TextView) v.findViewById(R.id.tv_isha_text);
		tvTodayDate = (TextView) v.findViewById(R.id.tv_todayDate);
		ivCalendar = (ImageView) v.findViewById(R.id.iv_frag_cal);
		tvAdanText = (TextView) v.findViewById(R.id.tv_adan_text);
		tvIqamaText = (TextView) v.findViewById(R.id.tv_iqama_text);
		
		tvFajrAdan = (TextView) v.findViewById(R.id.tv_fajr_adan);
		tvDhuhrAdan = (TextView) v.findViewById(R.id.tv_dhuhr_adan);
		tvAsrAdan = (TextView) v.findViewById(R.id.tv_asr_adan);
		tvMaghribAdan = (TextView) v.findViewById(R.id.tv_maghrib_adan);
		tvIshaAdan = (TextView) v.findViewById(R.id.tv_isha_adan);
		
		tvFajrAdanPlace = (TextView) v.findViewById(R.id.tv_fajr_Adan_place);
		tvDhuhrAdanPlace = (TextView) v.findViewById(R.id.tv_dhuhr_place);
		tvAsrAdanPlace = (TextView) v.findViewById(R.id.tv_asr_place);
		tvMaghribAdanPlcae = (TextView) v.findViewById(R.id.tv_maghrib_place);
		tvIshaAdanPlace = (TextView) v.findViewById(R.id.tv_isha_place);

		llRemainingFajr = (LinearLayout) v.findViewById(R.id.ll_remaining_fajr);
		llRemainingSunrise = (LinearLayout) v
				.findViewById(R.id.ll_remaining_sunrise);
		llRemainingDhuhr = (LinearLayout) v
				.findViewById(R.id.ll_remaining_dhuhr);
		llRemainingAsr = (LinearLayout) v.findViewById(R.id.ll_remaining_asr);
		llRemainingMaghrib = (LinearLayout) v
				.findViewById(R.id.ll_remaining_maghrib);
		llRemainingIsha = (LinearLayout) v.findViewById(R.id.ll_remaining_isha);

		tvFajrRemainTime = (TextView) v.findViewById(R.id.tv_remain_time_fajr);
		tvSunriseRemainTime = (TextView) v
				.findViewById(R.id.tv_remain_time_sunrise);
		tvDhuhrRemainTime = (TextView) v
				.findViewById(R.id.tv_remain_time_dhuhr);
		tvAsrRemainTime = (TextView) v.findViewById(R.id.tv_remain_time_asr);
		tvMaghribRemainTime = (TextView) v
				.findViewById(R.id.tv_remain_time_maghrib);
		tvIshaRemainTime = (TextView) v.findViewById(R.id.tv_remain_time_isha);
		tvToday = (TextView) v.findViewById(R.id.today_button);
		
		
		rlDateShow= (RelativeLayout) v.findViewById(R.id.rl_date_show);
		llPanelFajr= (LinearLayout) v.findViewById(R.id.ll_panel_fajr);
		llPanelIsha= (LinearLayout) v.findViewById(R.id.ll_panel_isha);
		llDateParent= (LinearLayout) v.findViewById(R.id.ll_date_parent);
		llPanelSunrise= (LinearLayout) v.findViewById(R.id.ll_panel_sunrise);
		llPanelDhuhr= (LinearLayout) v.findViewById(R.id.ll_panel_dhuhr);
		llPanelAsr= (LinearLayout) v.findViewById(R.id.ll_panel_asr);
		llPanelMaghrib= (LinearLayout) v.findViewById(R.id.ll_panel_maghrib);
		tvSunriseAdan= (TextView) v.findViewById(R.id.tv_sunrise_adan);
		
		tvAdanText.setText(SPMasjid.getValue(context, SPMasjid.ADHAN_LABEL));
		tvIqamaText.setText(SPMasjid.getValue(context, SPMasjid.IQAMAH_LABEL));
		tvAdanText.setPaintFlags(tvAdanText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		tvIqamaText.setPaintFlags(tvIqamaText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		
		
			
		if (SPMasjid.getValue(context, SPMasjid.home_banner_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_banner_navigation_BackColor).equalsIgnoreCase(null))) {
			llDateParent.setBackgroundColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_banner_navigation_BackColor)));
		}
		
		if (SPMasjid.getValue(context, SPMasjid.home_banner_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_banner_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_banner_navigation_BackAlpha))>0.0f) {
			llDateParent.setAlpha(Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_banner_navigation_BackAlpha)));

		}
		
		if (SPMasjid.getValue(context, SPMasjid.home_banner_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_banner_navigation_ForeColor).equalsIgnoreCase(null))) {
			tvAdanText.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_banner_navigation_ForeColor)));
			tvIqamaText.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_banner_navigation_ForeColor)));
			tvTodayDate.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_banner_navigation_ForeColor)));
			tvHijiriDate.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_banner_navigation_ForeColor)));
			
		}
		
		
		if (SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_BackColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_BackColor).equalsIgnoreCase(null))) {
			//rlDateShow.setBackgroundColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_BackColor)));
			llPanelFajr.setBackgroundColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_BackColor)));
		}
		
		if (SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_BackAlpha).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_BackAlpha))>0.0f) {
			llPanelFajr.setAlpha(Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_BackAlpha)));

		}
		
		if (SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvFajrText.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_ForeColor)));
			tvFajrAdan.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_ForeColor)));
			tvFajr.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_fajr_prayer_ForeColor)));
			
		}
		
		if (SPMasjid.getValue(context, SPMasjid.home_remain_fajr_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_remain_fajr_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvFajrRemainTime.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_remain_fajr_prayer_ForeColor)));
			}

		if (SPMasjid.getValue(context, SPMasjid.home_isha_prayer_BackColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_isha_prayer_BackColor).equalsIgnoreCase(null))) {
			llPanelIsha.setBackgroundColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_isha_prayer_BackColor)));
			}

		if (SPMasjid.getValue(context, SPMasjid.home_isha_prayer_BackAlpha).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_isha_prayer_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_isha_prayer_BackAlpha))>0.0f) {
			llPanelIsha.setAlpha(Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_isha_prayer_BackAlpha)));
			}
		if (SPMasjid.getValue(context, SPMasjid.home_isha_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_isha_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvIshaText.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_isha_prayer_ForeColor)));
			tvIshaAdan.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_isha_prayer_ForeColor)));
			tvIsha.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_isha_prayer_ForeColor)));
			}
		if (SPMasjid.getValue(context, SPMasjid.home_remain_isha_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_remain_isha_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvIshaRemainTime.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_remain_isha_prayer_ForeColor)));
			}

		
		
		if (SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_BackColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_BackColor).equalsIgnoreCase(null))) {
			llPanelSunrise.setBackgroundColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_BackColor)));
			}

		if (SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_BackAlpha).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_BackAlpha))>0.0f) {
			llPanelSunrise.setAlpha(Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_BackAlpha)));
			}
		if (SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvSunriseText.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_ForeColor)));
			tvSunriseAdan.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_ForeColor)));
			tvSunrise.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_sunrise_prayer_ForeColor)));
			}
		
		if (SPMasjid.getValue(context, SPMasjid.home_remain_sunrise_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_remain_sunrise_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvSunriseRemainTime.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_remain_sunrise_prayer_ForeColor)));
			}
		
		if (SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_BackColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_BackColor).equalsIgnoreCase(null))) {
			llPanelDhuhr.setBackgroundColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_BackColor)));
			}

		if (SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_BackAlpha).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_BackAlpha))>0.0f) {
			llPanelDhuhr.setAlpha(Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_BackAlpha)));
			}
		if (SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvDhuhrText.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_ForeColor)));
			tvDhuhrAdan.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_ForeColor)));
			tvDhuhr.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_dhuhr_prayer_ForeColor)));
			}
		
		if (SPMasjid.getValue(context, SPMasjid.home_remain_dhuhr_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_remain_dhuhr_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvDhuhrRemainTime.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_remain_dhuhr_prayer_ForeColor)));
			}
		
		
		if (SPMasjid.getValue(context, SPMasjid.home_asr_prayer_BackColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_asr_prayer_BackColor).equalsIgnoreCase(null))) {
			llPanelAsr.setBackgroundColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_asr_prayer_BackColor)));
			}

		if (SPMasjid.getValue(context, SPMasjid.home_asr_prayer_BackAlpha).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_asr_prayer_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_asr_prayer_BackAlpha))>0.0f) {
			llPanelAsr.setAlpha(Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_asr_prayer_BackAlpha)));
			}
		if (SPMasjid.getValue(context, SPMasjid.home_asr_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_asr_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvAsrText.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_asr_prayer_ForeColor)));
			tvAsrAdan.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_asr_prayer_ForeColor)));
			tvAsr.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_asr_prayer_ForeColor)));
			}
		
		if (SPMasjid.getValue(context, SPMasjid.home_remain_asr_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_remain_asr_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvAsrRemainTime.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_remain_asr_prayer_ForeColor)));
			}
		
		
		if (SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_BackColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_BackColor).equalsIgnoreCase(null))) {
			llPanelMaghrib.setBackgroundColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_BackColor)));
			}

		if (SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_BackAlpha).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_BackAlpha))>0.0f) {
			llPanelMaghrib.setAlpha(Float.parseFloat(SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_BackAlpha)));
			}
		if (SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvMaghribText.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_ForeColor)));
			tvMaghribAdan.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_ForeColor)));
			tvMaghrib.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_maghrib_prayer_ForeColor)));
			}
		
		if (SPMasjid.getValue(context, SPMasjid.home_remain_maghrib_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(context, SPMasjid.home_remain_maghrib_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvMaghribRemainTime.setTextColor(Color.parseColor(SPMasjid.getValue(context, SPMasjid.home_remain_maghrib_prayer_ForeColor)));
			}
		
		
	

		
		
		tvFajrText.setTypeface(typeface_bold);
		tvSunriseText.setTypeface(typeface_bold);
		tvDhuhrText.setTypeface(typeface_bold);
		tvAsrText.setTypeface(typeface_bold);
		tvMaghribText.setTypeface(typeface_bold);
		tvIshaText.setTypeface(typeface_bold);

		tvFajr.setTypeface(typeface_bold);
		tvDhuhr.setTypeface(typeface_bold);
		tvAsr.setTypeface(typeface_bold);
		tvMaghrib.setTypeface(typeface_bold);
		tvIsha.setTypeface(typeface_bold);
		tvSunrise.setTypeface(typeface_bold);
		tvToday.setTypeface(typeface_bold);
		
		
		tvFajr.setText(getArguments().getString(FAJR));
		tvSunrise.setText(getArguments().getString(SUNRISE));
		tvDhuhr.setText(getArguments().getString(DHUHR));
		tvAsr.setText(getArguments().getString(ASR));
		tvMaghrib.setText(getArguments().getString(MAGHRIB));
		tvIsha.setText(getArguments().getString(ISHA));
		
		tvFajrAdan.setText(getArguments().getString(FAJR_ADAN));
		tvDhuhrAdan.setText(getArguments().getString(DHUHR_ADAN));
		tvAsrAdan.setText(getArguments().getString(ASR_ADAN));
		tvMaghribAdan.setText(getArguments().getString(MAGHRIB_ADAN));
		tvIshaAdan.setText(getArguments().getString(ISHA_ADAN));
		
		if (getArguments().getString(FAJR_ADAN_PLACE).length() !=0) {
			tvFajrAdanPlace.setVisibility(View.VISIBLE);
			tvFajrAdanPlace.setText(getArguments().getString(FAJR_ADAN_PLACE));
		}
		if (getArguments().getString(DHUHR_ADAN_PLACE).length() !=0) {
			tvDhuhrAdanPlace.setVisibility(View.VISIBLE);
			tvDhuhrAdanPlace.setText(getArguments().getString(DHUHR_ADAN_PLACE));
		}
		if (getArguments().getString(ASR_ADAN_PLACE).length() !=0) {
			tvAsrAdanPlace.setVisibility(View.VISIBLE);
			tvAsrAdanPlace.setText(getArguments().getString(ASR_ADAN_PLACE));
		}
		if (getArguments().getString(MAGHRIB_ADAN_PLACE).length() !=0) {
			tvMaghribAdanPlcae.setVisibility(View.VISIBLE);
			tvMaghribAdanPlcae.setText(getArguments().getString(MAGHRIB_ADAN_PLACE));
		}
		if (getArguments().getString(ISHA_ADAN_PLACE).length() !=0) {
			tvIshaAdanPlace.setVisibility(View.VISIBLE);
			tvIshaAdanPlace.setText(getArguments().getString(ISHA_ADAN_PLACE));
		}
		
		if (getArguments().getBoolean(ISTODAY)) {
			try {
				tvTimer = getTimerView();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			new TimerAsyncTask().execute();
		} else {
			tvToday.setVisibility(View.VISIBLE);
			ivCalendar.setVisibility(View.GONE);
		}
		// TextView messageTextView = (TextView)v.findViewById(R.id.textView);
		// messageTextView.setText(message);

		Calendar cal = Calendar.getInstance();
		curDate = getArguments().getString(DATE);
		String str[] = curDate.split("-");
		cal.set(Integer.parseInt(str[0]), Integer.parseInt(str[1]) - 1,
				Integer.parseInt(str[2]));
		//cal.setTimeInMillis(System.currentTimeMillis());
		int day = cal.get(Calendar.DAY_OF_WEEK);
		String weekDay = null;

		switch (day) {
		case Calendar.SUNDAY:
			weekDay = "Sun";
			break;
		case Calendar.MONDAY:
			weekDay = "Mon";
			break;
		case Calendar.TUESDAY:
			weekDay = "Tue";
			break;
		case Calendar.WEDNESDAY:
			weekDay = "Wed";
			break;
		case Calendar.THURSDAY:
			weekDay = "Thu";
			break;
		case Calendar.FRIDAY:
			weekDay = "Fri";
			break;
		case Calendar.SATURDAY:
			weekDay = "Sat";
			break;

		default:
			break;
		}

		SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
		String formattedDate = df.format(cal.getTime());
		tvTodayDate.setText(weekDay + ", " + formattedDate);

			if (SPMasjid.getIntValue(context, SPMasjid.IS_SHOW_ISLAMIC_DATE)==1) {
				tvHijiriDate.setVisibility(View.VISIBLE);
			/*	tvHijiriDate.setText(DateHigri.writeIslamicDate(
						cal.get(Calendar.DAY_OF_MONTH)+(SPMasjid.getIntValue(context,SPMasjid.ISLAMIC_DATE_OFFSET)), cal.get(Calendar.MONTH),
						cal.get(Calendar.YEAR)));*/
				
				cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)+(SPMasjid.getIntValue(context, SPMasjid.ISLAMIC_DATE_OFFSET)));
				tvHijiriDate.setText(HijriCalendar.getSimpleDate(cal));
				
			} else {
				tvHijiriDate.setVisibility(View.INVISIBLE );
				
			}

		
		tvToday.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				homeActiviy.setFirstIndexViewPager();
			}
		});
		
		// homeActiviy.varshit(getArguments().getString(DATE));
		//homeActiviy.matchFridaytime(getArguments().getString(DATE));
		return v;
	}

	private long getNamazTimeInMilis(String time) {

		System.out.println("time to convetrt ======>" + time);
		try {
			Calendar vCal = Calendar.getInstance();
			vCal.setTimeInMillis(System.currentTimeMillis());
			String str[] = time.split(":");
			String st[] = str[1].split(" ");
			int hr = Integer.parseInt(str[0]);
			int min = Integer.parseInt(st[0]);
			if (st[1].equalsIgnoreCase("AM")) {
				if (hr == 12) {
					hr = 0;
				}
			}
			else if (st[1].equalsIgnoreCase("PM")) {
				if (hr == 12) {
					hr = hr;
				}else {
					hr += 12;
				}
			}
			else {
				hr += 12;
			}
			vCal.set(Calendar.HOUR_OF_DAY, hr);
			vCal.set(Calendar.MINUTE, min);
			vCal.set(Calendar.SECOND, 0);
			return vCal.getTimeInMillis();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	private TextView getTimerView() {
		TextView tvTmp;
		long currentTime = System.currentTimeMillis();
		//if(SPMasjid.getIntValue(context,SPMasjid.PRAYERNOTIFICATION).equals("1")){
			 fajr = getNamazTimeInMilis(tvFajrAdan.getText().toString());
			 sunRise = getNamazTimeInMilis(tvSunriseAdan.getText().toString());
			 dhuhr = getNamazTimeInMilis(tvDhuhrAdan.getText().toString());
			 asr = getNamazTimeInMilis(tvAsrAdan.getText().toString());
			 maghrib = getNamazTimeInMilis(tvMaghribAdan.getText().toString());
			 isha = getNamazTimeInMilis(tvIshaAdan.getText().toString());
		/*}
		else{
			 fajr = getNamazTimeInMilis(tvFajr.getText().toString());
			 sunRise = getNamazTimeInMilis(tvSunrise.getText().toString());
			 dhuhr = getNamazTimeInMilis(tvDhuhr.getText().toString());
			 asr = getNamazTimeInMilis(tvAsr.getText().toString());
			 maghrib = getNamazTimeInMilis(tvMaghrib.getText().toString());
			 isha = getNamazTimeInMilis(tvIsha.getText().toString());
		}*/


		if (currentTime < fajr) {
			nextNamaz = fajr;
			tvTmp = tvFajrRemainTime;
			llRemainingFajr.setVisibility(View.VISIBLE);
			llRemainingSunrise.setVisibility(View.INVISIBLE);
			llRemainingDhuhr.setVisibility(View.INVISIBLE);
			llRemainingAsr.setVisibility(View.INVISIBLE);
			llRemainingMaghrib.setVisibility(View.INVISIBLE);
			llRemainingIsha.setVisibility(View.INVISIBLE);
		} else if (currentTime < sunRise) {
			nextNamaz = sunRise;
			tvTmp = tvSunriseRemainTime;
			llRemainingSunrise.setVisibility(View.VISIBLE);
			llRemainingFajr.setVisibility(View.INVISIBLE);
			llRemainingDhuhr.setVisibility(View.INVISIBLE);
			llRemainingAsr.setVisibility(View.INVISIBLE);
			llRemainingMaghrib.setVisibility(View.INVISIBLE);
			llRemainingIsha.setVisibility(View.INVISIBLE);
		} else if (currentTime < dhuhr) {
			nextNamaz = dhuhr;
			tvTmp = tvDhuhrRemainTime;
			llRemainingDhuhr.setVisibility(View.VISIBLE);
			llRemainingFajr.setVisibility(View.INVISIBLE);
			llRemainingSunrise.setVisibility(View.INVISIBLE);
			llRemainingAsr.setVisibility(View.INVISIBLE);
			llRemainingMaghrib.setVisibility(View.INVISIBLE);
			llRemainingIsha.setVisibility(View.INVISIBLE);
		} else if (currentTime < asr) {
			nextNamaz = asr;
			tvTmp = tvAsrRemainTime;
			llRemainingAsr.setVisibility(View.VISIBLE);
			llRemainingFajr.setVisibility(View.INVISIBLE);
			llRemainingSunrise.setVisibility(View.INVISIBLE);
			llRemainingDhuhr.setVisibility(View.INVISIBLE);
			llRemainingMaghrib.setVisibility(View.INVISIBLE);
			llRemainingIsha.setVisibility(View.INVISIBLE);
		} else if (currentTime < maghrib) {
			nextNamaz = maghrib;
			tvTmp = tvMaghribRemainTime;
			llRemainingMaghrib.setVisibility(View.VISIBLE);
			llRemainingFajr.setVisibility(View.INVISIBLE);
			llRemainingSunrise.setVisibility(View.INVISIBLE);
			llRemainingDhuhr.setVisibility(View.INVISIBLE);
			llRemainingAsr.setVisibility(View.INVISIBLE);
			llRemainingIsha.setVisibility(View.INVISIBLE);
		} else if (currentTime < isha) {
			nextNamaz = isha;
			tvTmp = tvIshaRemainTime;
			llRemainingIsha.setVisibility(View.VISIBLE);
			llRemainingFajr.setVisibility(View.INVISIBLE);
			llRemainingSunrise.setVisibility(View.INVISIBLE);
			llRemainingDhuhr.setVisibility(View.INVISIBLE);
			llRemainingAsr.setVisibility(View.INVISIBLE);
			llRemainingMaghrib.setVisibility(View.INVISIBLE);
		} else {
			nextNamaz = fajr + AlarmManager.INTERVAL_DAY;
			llRemainingFajr.setVisibility(View.VISIBLE);
			llRemainingSunrise.setVisibility(View.INVISIBLE);
			llRemainingDhuhr.setVisibility(View.INVISIBLE);
			llRemainingAsr.setVisibility(View.INVISIBLE);
			llRemainingMaghrib.setVisibility(View.INVISIBLE);
			llRemainingIsha.setVisibility(View.INVISIBLE);
			tvTmp = tvFajrRemainTime;
		}
		return tvTmp;
	}

	class TimerAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(480);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			try {
			Date date = new Date(nextNamaz - System.currentTimeMillis());
			long diff = (nextNamaz - System.currentTimeMillis()) / 1000;
			long hr = diff / 3600;
			diff = diff - (hr * 3600);
			long min = diff / 60;
			long sec = diff % 60;
			// formattter
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
			// Pass date object
			String formatted = formatter.format(date);
			// Log.v("New Time for fimnd ", formatted);
			tvTimer.setText(formatted + "");
			
			//  New Code for Remaining Time
			if (diff<0) {
				tvTimer.setVisibility(View.GONE);
			}

			tvTimer.setText(String.format("%02d:%02d:%02d", hr, min, sec));
			if (nextNamaz - System.currentTimeMillis() > 1000) {
				new TimerAsyncTask().execute();
			} else {
				tvTimer = getTimerView();
				new TimerAsyncTask().execute();
			}
			
		} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onDestroyView() {
		
	    FragmentActivity a = getActivity();
	    if (a != null ) {
	        try {
	            a.getSupportFragmentManager().beginTransaction().remove(MyFragment.this).commit();
	        } catch(IllegalStateException e) {
	        
	        }
	    }
	    super.onDestroyView();
	}
}
