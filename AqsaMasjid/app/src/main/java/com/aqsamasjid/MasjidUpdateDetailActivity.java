package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.Webimageloader.ImageLoader;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class MasjidUpdateDetailActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private TextView tvServiceTitle;
//	private TextView tvServiceDetail;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
//	private Button btnRegister;
	private Dialog dialog;
	private EditText etEmail;
	private EditText etMobile;
	private TextView tvTExtHeader;
	private ImageView ivBannerService;
	private NetworkConnection networkConnection;
//	private MasjidImageLoader imageLoader;
	private ImageLoader webImageLoader;
	private WebView webView;
	private TextView tvAppLabel;
	private TextView tvServiceText;
	private LinearLayout llTitleContainer;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_masjid_update_detail);
		initComponents();
		initListiners();
		initSideBar();

		Intent intent = getIntent();
		tvServiceTitle.setText(intent.getStringExtra("ServiceTitle"));
		webView.loadDataWithBaseURL(null, intent.getStringExtra("ServiceDetail"), "text/html", "UTF-8", null);
		WebSettings webSettings = webView.getSettings();
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);


		webImageLoader = new ImageLoader(MasjidUpdateDetailActivity.this);
		try {
			String imageUrl = intent.getStringExtra("ServiceBannerImage").toString();
			webImageLoader.DisplayImage(imageUrl, ivBannerService);

/*			Log.v("IMAGE Poster URL", imageUrl);
			String[] strfile = imageUrl.split("[/]");
			String fName = strfile[strfile.length - 1];
			File image = new File(Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/Aqsa_Masjid/Image/ " + fName);
			if (image.exists()) {
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(image),
						null, o);
				// decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = 8;
				ivBannerService.setImageBitmap(BitmapFactory.decodeStream(
						new FileInputStream(image), null, o2));
			} else {
				imageLoader.DisplayImage(imageUrl, ivBannerService, fName,
						R.drawable.noti_loader);
			}
*/		} catch (Exception ex) {
			System.out.println("Error :  Image URL " + ex);
			ex.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		llTitleContainer= (LinearLayout) findViewById(R.id.ll_title_container);
		tvServiceTitle = (TextView) findViewById(R.id.tv_service_title);
		ivBannerService = (ImageView) findViewById(R.id.iv_banner_service_detail);
		webView  = (WebView) findViewById(R.id.webview_masjid_update_detail);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.Masjid_Name));
		tvServiceText= (TextView) findViewById(R.id.tv_serviice_text);

		if (SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.home_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.home_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.home_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.home_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackAlpha))>0.0f)
		{
			tvServiceText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.home_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.home_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_ForeColor)));
		}


		if (SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			llTitleContainer.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			llTitleContainer.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(MasjidUpdateDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiceTitle.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}



	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(MasjidUpdateDetailActivity.this, MasjidUpdateActivity.class));
				/*Intent intent = new Intent(ServiceDetailActivity.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});
		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MasjidUpdateDetailActivity.this,MasjidUpdateActivity.class);
				startActivity(intent);
				finish();
			}
		});*/
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(MasjidUpdateDetailActivity.this, MasjidUpdateActivity.class));
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MasjidUpdateDetailActivity.this, EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MasjidUpdateDetailActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(MasjidUpdateDetailActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(MasjidUpdateDetailActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(MasjidUpdateDetailActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(MasjidUpdateDetailActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});	
	}
	
	protected void dialogForRegistration() {
		// TODO Auto-generated method stub
		
		dialog = new Dialog(MasjidUpdateDetailActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_registration);
		tvTExtHeader = (TextView) dialog.findViewById(R.id.tv_text_header);
		etEmail = (EditText) dialog.findViewById(R.id.et_email);
		etMobile = (EditText) dialog.findViewById(R.id.et_mobile);
		Button btnSubmit = (Button) dialog.findViewById(R.id.btn_submit);
		Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancal);
		
		tvTExtHeader.setText("Become a volunteer");
		
		dialog.show();
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (etEmail.getText().toString().length()==0) {
					showMsgDialog("Please enter E-mail.");
				}else if (!EmailValidator.isValid(etEmail.getText().toString())) {
					showMsgDialog("please enter valid E-mail.");
				}else if (etMobile.getText().toString().length()==0) {
					showMsgDialog("Please enter phone number.");
				}else if(etMobile.getText().toString().trim().length() != 10) {
					showMsgDialog("Please enter valid phone number.");
				}
				else {
					if (networkConnection.isOnline(getApplicationContext())) {
							new RegistrationAsyncTask().execute();
					} else {
						Toast.makeText(getBaseContext(),
								"Please check your network connection.", Toast.LENGTH_SHORT).show();
					}
					
				}
			}
		});
	}
	public void showMsgDialog(String message) {
		new AlertDialog.Builder(MasjidUpdateDetailActivity.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

					}
				}).create().show();
	}
	
	class RegistrationAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("email",etEmail.getText().toString()));
			urlParameter.add(new BasicNameValuePair("mobile",etMobile.getText().toString()));
			urlParameter.add(new BasicNameValuePair("type","service"));
			progressDialog = new ProgressDialog(MasjidUpdateDetailActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter Registration", urlParameter+"");
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.SignUpVoluentierUrl, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}else {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}
					
				} else {
					Toast.makeText(MasjidUpdateDetailActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
