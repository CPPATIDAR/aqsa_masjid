package com.aqsamasjid;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.ShowDialog;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class AskQuestionActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private NetworkConnection networkConnection;
	private EditText etEmail;
	private EditText etAskQuestion;
	private Button btnSave;
	private TextView tvAppLabel;
	private ShowDialog showMessage;
	private TextView tvServiiceText;
//	private String data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ask_imam);
		initComponents();
		initListiners();
		initSideBar();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		showMessage = new ShowDialog();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		etEmail = (EditText) findViewById(R.id.et_ask_imam_email);
		etAskQuestion = (EditText) findViewById(R.id.et_ask_imam_ask_question);
		btnSave = (Button) findViewById(R.id.btn_save);
		tvServiiceText= (TextView) findViewById(R.id.tv_serviice_text);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.Masjid_Name));

		if (SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvServiiceText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvServiiceText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiiceText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}



		if (SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.application_btn_BackColor).length()!=0&&(!SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.application_btn_BackColor).equalsIgnoreCase(null)))
		{
			btnSave.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_BackColor)));
		}
		if (SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.application_btn_alpha).length()!=0&&(!SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.application_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha))>0.0f)
		{
			btnSave.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha)));
		}
		if (SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.application_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(AskQuestionActivity.this, SPMasjid.application_btn_ForeColor).equalsIgnoreCase(null)))
		{
			btnSave.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_ForeColor)));
		}


	}

	private void initListiners() {
		// TODO Auto-generated method stub

		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*setResult(RESULT_OK,new Intent(AskImamActivity.this,AnnouncementActivity.class));
				finish();*/
				setResult(RESULT_OK,new Intent(AskQuestionActivity.this, AskImamLatestQAActivity.class));
				finish();
			}
		});

		tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(AskQuestionActivity.this, AskImamLatestQAActivity.class));
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AskQuestionActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AskQuestionActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getIntValue(AskQuestionActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(AskQuestionActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(AskQuestionActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(AskQuestionActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
		
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (etEmail.getText().toString().length()==0) {
					showMessage.showMsg(AskQuestionActivity.this, "Please enter E-mail.");
				}else if (!EmailValidator.isValid(etEmail.getText().toString())) {
					showMessage.showMsg(AskQuestionActivity.this, "please enter valid E-mail.");
				}else if (etAskQuestion.getText().toString().length()==0) {
					showMessage.showMsg(AskQuestionActivity.this, "Please enter ask question.");
				}
				else {
					if (networkConnection.isOnline(getApplicationContext())) {
							new AakQuestionAsyncTask().execute();
					} else {
						Toast.makeText(getBaseContext(),
								"Please check your network connection.", Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
	}
	
	class AakQuestionAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("email",etEmail.getText().toString()));
			urlParameter.add(new BasicNameValuePair("question",etAskQuestion.getText().toString()));
			urlParameter.add(new BasicNameValuePair("public","no"));
			progressDialog = new ProgressDialog(AskQuestionActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter Registration", urlParameter+"");
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.SaveAskImamQuestion, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						Toast.makeText(getApplicationContext(), jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
						finish();
						//showMessage.showMsg(AskQuestionActivity.this, jsonObject.optString("message"));
						
					}else {
						showMessage.showMsg(AskQuestionActivity.this, jsonObject.optString("message"));
					}
					
				} else {
					Toast.makeText(AskQuestionActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	
}
