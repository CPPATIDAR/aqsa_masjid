package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

@SuppressLint("NewApi") public class AboutUsActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private NetworkConnection networkConnection;
	private WebView webView;
	private TextView tvAppLabel;
	private TextView tvAboutUsText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);
		initComponents();
		initListiners();
		initSideBar();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		/*Intent intent = new Intent(AnnouncementDetailActivity.this,
				AnnouncementActivity.class);
		startActivity(intent);*/
		finish();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		webView = (WebView) findViewById(R.id.webview_aboutus);
		tvAboutUsText = (TextView) findViewById(R.id.tv_about_us_text);
		tvAppLabel.setText(SPMasjid.getValue(AboutUsActivity.this, SPMasjid.Masjid_Name));
		
		
		if (SPMasjid.getValue(AboutUsActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(AboutUsActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvAboutUsText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(AboutUsActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(AboutUsActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvAboutUsText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(AboutUsActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(AboutUsActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvAboutUsText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		if (networkConnection.isOnline(getApplicationContext())) {
			new AboutUsAsyncTask().execute();
		} else {
			Toast.makeText(getBaseContext(),
					"Please check your network connection.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(AboutUsActivity.this, AboutUsActivity.class));
				finish();
			}
		});
		/*tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AnnouncementDetailActivity.this,
						AnnouncementActivity.class);
				startActivity(intent);
				finish();
			}
		});*/
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(AboutUsActivity.this, AnnouncementActivity.class));
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AboutUsActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AboutUsActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getIntValue(AboutUsActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(AboutUsActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(AboutUsActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(AboutUsActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
	}

	

	public void showMsgDialog(String message) {
		new AlertDialog.Builder(AboutUsActivity.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

					}
				}).create().show();
	}
	
	class AboutUsAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(AboutUsActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.ABOUT_US, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
						webView.loadData(response, "text/html", "UTF-8");
						WebSettings webSettings = webView.getSettings();
						webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
						webSettings.setBuiltInZoomControls(true);
						webSettings.setSupportZoom(true);
						webSettings.setDefaultFontSize((int)20);
				} else {
					Toast.makeText(AboutUsActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
