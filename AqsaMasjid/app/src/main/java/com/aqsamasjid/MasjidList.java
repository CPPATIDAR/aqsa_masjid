package com.aqsamasjid;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aqsamasjid.adapter.MasjidAddressAdapter;
import com.aqsamasjid.pojo.MasjidPojo;
import com.aqsamasjid.utill.SPMasjid;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MasjidList extends BaseActivity {

	private ImageView ivHome;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private TextView tvAppLabel;
	private ListView lvMasjid;
	private ArrayList<MasjidPojo> alMasjidAddress;
	private JSONArray masjidJsonArray;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_masjid_list);
		initComponent();
		initListener();
		initSideBar();
				
		
	}

	private void initListener() {
		// TODO Auto-generated method stub
		
		ivHome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MasjidList.this, EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MasjidList.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(MasjidList.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(MasjidList.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(MasjidList.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(MasjidList.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}				finish();
			}
		});	
		
		
		
	}

	private void initComponent() {
		alMasjidAddress=new ArrayList<MasjidPojo>();
		lvMasjid=(ListView)findViewById(R.id.lv_masjid);
		ivHome = (ImageView) findViewById(R.id.iv_home);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(MasjidList.this, SPMasjid.Masjid_Name));
	
		
		
		
		try {
			masjidJsonArray=new JSONArray(getIntent().getExtras().getString("masjidJsonArray"));
	
			System.out.println("masjidJsonArray=="+masjidJsonArray);
			for (int i = 0; i < masjidJsonArray.length(); i++) {
				JSONObject jsObject = masjidJsonArray.optJSONObject(i);
				
				
	
				
				MasjidPojo pojo=new MasjidPojo();
				pojo.setId(jsObject.optLong("id"));
				pojo.setName(jsObject.optString("name"));
				pojo.setMasjid_name(jsObject.optString("masjid_name"));
				pojo.setStreet(jsObject.optString("street"));
				pojo.setCity(jsObject.optString("city"));
				pojo.setCountry(jsObject.optString("country"));
				pojo.setState(jsObject.optString("state"));
				pojo.setZipcode(jsObject.optString("zipcode"));
				pojo.setActive(jsObject.optBoolean("isActive"));
				pojo.setLogoFile(jsObject.optString("logo_file"));
				pojo.setLogoImage(jsObject.optString("logo_image"));
				pojo.setTimezone(jsObject.optString("timezone"));
				pojo.setBannerImage(jsObject.optString("banner_image"));
				pojo.setYoutubeUrl(jsObject.optString("youtube_url"));
				pojo.setYoutubeLiveStreamUrl(jsObject.optString("youtube_live_stream_url"));
				pojo.setFacebookUrl(jsObject.optString("facebook_url"));
				pojo.setTwitterUrl(jsObject.optString("twitter_url"));
				pojo.setRamdanCountdown(jsObject.optInt("ramdan_countdown"));
				pojo.setRamdanstartDate(jsObject.optString("ramdanstart_date"));
				pojo.setLattitude(jsObject.optString("lattitude"));
				pojo.setLongitude(jsObject.optString("longitude"));
				pojo.setCountdownLabel(jsObject.optString("countdown_label"));
				pojo.setIsRamadan(jsObject.optString("is_ramadan"));
				
				alMasjidAddress.add(pojo);
				
				
				
				
		}
		
			lvMasjid.setAdapter(new MasjidAddressAdapter(MasjidList.this, alMasjidAddress));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}

