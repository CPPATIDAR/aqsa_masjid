package com.aqsamasjid.imageloader;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class FileCache {
    
    private File cacheDir;
    
    public FileCache(Context context){
        
    	File SDCardRoot;
		if (android.os.Build.VERSION.SDK_INT > 22) {
			SDCardRoot = context.getCacheDir();

		} else {
			SDCardRoot = Environment.getExternalStorageDirectory();
		}
			cacheDir = new File(SDCardRoot.getAbsolutePath()
				+ "/Aqsa_Masjid/Image");

        if(!cacheDir.exists())
            cacheDir.mkdirs();
		
    	
    }
    
    public File getFile(String url,String filename){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
    //    String filename="varshit.jpg";//String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
        //String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);
        return f;
        
    }
    
    public void clear(){
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        for(File f:files)
            f.delete();
    }

}