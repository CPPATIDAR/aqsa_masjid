package com.aqsamasjid.imageloader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.widget.ImageView;

import com.aqsamasjid.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MasjidImageLoader 
{
    
    MemoryCache memoryCache=new MemoryCache();
    FileCache fileCache;
    int stub_id; 
    private Map<ImageView, String> imageViews=Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService; 
    
    public MasjidImageLoader(Context context)
    {
        fileCache=new FileCache(context);
        executorService=Executors.newFixedThreadPool(5);
    }
    
    public void DisplayImage(String url, ImageView imageView,String filename, int Loader)///R.drawable.lancher_icon
    {
    	try {
		
    	stub_id = Loader;
    	stub_id= R.drawable.blank;
//    	stub_id=R.drawable.lancher_icon;
    	System.out.println("url:"+url);
        imageViews.put(imageView, url);
        Bitmap bitmap=memoryCache.get(url);
        if(bitmap!=null)
        {
            imageView.setImageBitmap(bitmap);
            System.out.println("Inside bitmap"+url);
        }
        else
        {
            queuePhoto(url, imageView,filename);
         
            if (imageView!=null) {
            	   imageView.setImageResource(stub_id);
            	//  Ion.with(imageView).load("android.resource://com.aajnodin/" + R.drawable.lavalamp);
                  System.out.println("Inside Default image"+url);
			}
          
        }
    	} catch (Exception e) {
			e.printStackTrace();
		}
    }
   
	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 30;
		int targetHeight = 30;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
				Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);

		Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		Bitmap sourceBitmap = scaleBitmapImage;

		mPaint.setStyle(Paint.Style.FILL);
		mPaint.setColor(Color.parseColor("#ffffff"));
		mPaint.setShadowLayer(4.0f, 0.0f, 2.0f, Color.WHITE);

		canvas.drawARGB(0, 0, 0, 0);
		canvas.drawCircle(targetBitmap.getWidth() / 2,
				targetBitmap.getHeight() / 2, targetBitmap.getWidth() / 1.9f,
				mPaint);

		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);

		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), mPaint);
		return targetBitmap;

	} 
    final int stub_id1 = R.drawable.blank;
    public void DisplayProfile(String url, ImageView imageView,String filename)
    {
    	System.out.println("url:"+url);
        imageViews.put(imageView, url);
        Bitmap bitmap=memoryCache.get(url);
        if(bitmap!=null)
        {
            imageView.setImageBitmap(getRoundedShape(bitmap));
       
        }
        else
        {
            queuePhoto(url, imageView,filename);
           
            if (imageView!=null) {
            	 imageView.setImageResource(stub_id1);
            	// Ion.with(imageView).load("android.resource://com.aajnodin/" + R.drawable.lavalamp);
			}
           
        }
    }
   
    private void queuePhoto(String url, ImageView imageView,String filename)
    {
        PhotoToLoad p=new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p,filename));
    }
    
    public Bitmap getBitmap(String url, String filename) 
    {
        File f=fileCache.getFile(url,filename);
        
        //from SD cache
        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;
        
        //from web
        try {
            Bitmap bitmap=null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            bitmap = decodeFile(f);
            return bitmap;
        } catch (Exception ex){
           ex.printStackTrace();
           return null;
        }
    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f)
    {
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inPurgeable = true;
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);
            
            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE=150;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=1;
            }
            
            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            //o2.inPreferredConfig=null; 
            
            //o2.inScreenDensity=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }
    
    //Task for the queue
    private class PhotoToLoad
    {
        public String url;
        public ImageView imageView;
        public PhotoToLoad(String u, ImageView i){
            url=u; 
            imageView=i;
        }
    }
    
    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;
        String filename;
        PhotosLoader(PhotoToLoad photoToLoad, String filename){
            this.photoToLoad=photoToLoad;
            this.filename=filename;
        }
        
       
        public void run() {
            if(imageViewReused(photoToLoad))
                return;
            Bitmap bmp=getBitmap(photoToLoad.url,filename);
            memoryCache.put(photoToLoad.url, bmp);
            if(imageViewReused(photoToLoad))
                return;
            BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
            Activity a=(Activity)photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }
    
    boolean imageViewReused(PhotoToLoad photoToLoad){
        String tag=imageViews.get(photoToLoad.imageView);
        if(tag==null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }
    
    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        public BitmapDisplayer(Bitmap b, PhotoToLoad p){bitmap=b;photoToLoad=p;}
        public void run()
        {
            if(imageViewReused(photoToLoad))
                return;
            if(bitmap!=null)
                photoToLoad.imageView.setImageBitmap(bitmap);
            else{
            	try {
            	
            		if (photoToLoad.imageView!=null) {
            			 photoToLoad.imageView.setImageResource(stub_id);
//            			Ion.with(photoToLoad.imageView).load("android.resource://com.aajnodin/" + R.drawable.lavalamp);
					}
            		 
				} catch (Exception e) {
					e.printStackTrace();
				}
               
        }
            }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }
    public static enum ScalingLogic 
    {
        CROP, FIT
    }
    public static Rect calculateSrcRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
            ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.CROP) {
            final float srcAspect = (float)srcWidth / (float)srcHeight;
            final float dstAspect = (float)dstWidth / (float)dstHeight;

            if (srcAspect > dstAspect) {
                final int srcRectWidth = (int)(srcHeight * dstAspect);
                final int srcRectLeft = (srcWidth - srcRectWidth) / 2;
                return new Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth, srcHeight);
            } else {
                final int srcRectHeight = (int)(srcWidth / dstAspect);
                final int scrRectTop = (int)(srcHeight - srcRectHeight) / 2;
                return new Rect(0, scrRectTop, srcWidth, scrRectTop + srcRectHeight);
            }
        } else {
            return new Rect(0, 0, srcWidth, srcHeight);
        }
    }
    public static Bitmap createScaledBitmap(Bitmap unscaledBitmap, int dstWidth, int dstHeight,
            ScalingLogic scalingLogic) {
        Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(),
                dstWidth, dstHeight, scalingLogic);
        Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(),
                dstWidth, dstHeight, scalingLogic);
        Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(), dstRect.height(),
                Config.ARGB_8888);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }
    public static Rect calculateDstRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
            ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.FIT) {
            final float srcAspect = (float)srcWidth / (float)srcHeight;
            final float dstAspect = (float)dstWidth / (float)dstHeight;

            if (srcAspect > dstAspect) {
                return new Rect(0, 0, dstWidth, (int)(dstWidth / srcAspect));
            } else {
                return new Rect(0, 0, (int)(dstHeight * srcAspect), dstHeight);
            }
        } else {
            return new Rect(0, 0, dstWidth, dstHeight);
        }
    }
}
