package com.aqsamasjid;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.SalatDataBase;
import com.aqsamasjid.utill.ShowDialog;


public class TasbiActivity extends BaseActivity {

	private com.aqsamasjid.utill.DonutProgress DonutProgress;
	private com.aqsamasjid.pojo.TasbiPojo TasbiPojo;
	private NetworkConnection networkConnection;
	private ShowDialog showMessage;
	private ImageView ivHome;
	private LinearLayout llPrayer;
	private LinearLayout llServices;
	private LinearLayout llEvents;
	private LinearLayout llDonate;
	private TextView tvAppLabel;
	private TextView tvServiceText;
	private TextView tvTasbihatArabic;
	private TextView tvTasbihatEnglish;
	private Typeface typeface_bold;
	private TextView tvTasbhiCount;
	private SalatDataBase dataBase;
	private long counter=0;
	private int pbcounter=0;
	private TextView tvTasbhiTimes;
	private TextView tvTasbhiRecited;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tasbi);
		initComponents();
		initListiners();
		initSideBar();		
		
		DonutProgress.setProgress(0);
		//tvTasbhiCount.setText(dataBase.getTasbihat(TasbiPojo.getId()+"")+"");
		
		if (dataBase.CheckIsTasbiAlreadyInDBorNot(TasbiPojo.getId()+"")) {
			counter=dataBase.getTasbihat(TasbiPojo.getId()+"");
			tvTasbhiCount.setText(counter+"");
		} else {
			counter=0;
			tvTasbhiCount.setText(counter+"");
		}
		DonutProgress.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View view) {
	            	pbcounter++;
					DonutProgress.setProgress(pbcounter);

	            	DonutProgress.setMax(100);
	            	
	            	counter++;
					DonutProgress.setText("");
					new CountDownTimer(200,100){
						@Override
						public void onTick(long l) {

						}

						@Override
						public void onFinish() {
							DonutProgress.setText(pbcounter + "");

						}
					}.start();
					tvTasbhiCount.setText(counter + "");
	            	if (dataBase.CheckIsTasbiAlreadyInDBorNot(TasbiPojo.getId()+"")) {
	            		dataBase.updateTasbihat(TasbiPojo.getId()+"", counter+"");
					} else {
						dataBase.addTasbihat(TasbiPojo.getId()+"", TasbiPojo.getEnglishName()+"", counter+"");
					}
	            	
	            }
	        });
		
		
	}
	
	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		showMessage = new ShowDialog();
		 dataBase=new SalatDataBase(TasbiActivity.this);
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
	
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvServiceText= (TextView) findViewById(R.id.tv_serviice_text);
		DonutProgress=(com.aqsamasjid.utill.DonutProgress)findViewById(R.id.donut_progress);
		tvTasbihatArabic= (TextView) findViewById(R.id.tv_tabhi_arabic);
		tvTasbihatEnglish=(TextView)findViewById(R.id.tv_tabhi_english);
		tvTasbhiCount=(TextView)findViewById(R.id.tv_tabhi_recited_count);
		tvTasbhiTimes=(TextView)findViewById(R.id.tv_tabhi_times);
		tvTasbhiRecited=(TextView)findViewById(R.id.tv_tabhi_recited);
		
		
		
		typeface_bold = Typeface.createFromAsset(TasbiActivity.this.getAssets(),
				"arialbd.ttf");
		tvTasbihatArabic.setTypeface(typeface_bold);
		tvTasbihatEnglish.setTypeface(typeface_bold);
		tvTasbhiCount.setTypeface(typeface_bold);
		tvTasbhiTimes.setTypeface(typeface_bold);
		tvTasbhiRecited.setTypeface(typeface_bold);
		
		tvAppLabel.setText(SPMasjid.getValue(TasbiActivity.this, SPMasjid.Masjid_Name));


		
		if (SPMasjid.getValue(TasbiActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(TasbiActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(TasbiActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(TasbiActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvServiceText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(TasbiActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(TasbiActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		try {
			TasbiPojo=(com.aqsamasjid.pojo.TasbiPojo) getIntent().getExtras().getSerializable("Tabihat");

			tvTasbihatArabic.setText(TasbiPojo.getArabicName());
			tvTasbihatEnglish.setText(TasbiPojo.getEnglishName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(TasbiActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

				//setResult(RESULT_OK,new Intent(TasbiActivity.this,ContactUsActivity.class));
				finish();
			}
		});
		tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(TasbiActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				//setResult(RESULT_OK,new Intent(TasbiActivity.this,ContactUsActivity.class));
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TasbiActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TasbiActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if (SPMasjid.getIntValue(TasbiActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(TasbiActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(TasbiActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(TasbiActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
		
	
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}

}
