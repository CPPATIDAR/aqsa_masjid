package com.aqsamasjid.pojo;

public class RamdanSchedulePojo {

	private int day;
	private String date;
	private String sehrTime;
	private String iftarTime;
	private String taraviTime;
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getSehrTime() {
		return sehrTime;
	}
	public void setSehrTime(String sehrTime) {
		this.sehrTime = sehrTime;
	}
	public String getIftarTime() {
		return iftarTime;
	}
	public void setIftarTime(String iftarTime) {
		this.iftarTime = iftarTime;
	}
	public String getTaraviTime() {
		return taraviTime;
	}
	public void setTaraviTime(String taraviTime) {
		this.taraviTime = taraviTime;
	}
}
