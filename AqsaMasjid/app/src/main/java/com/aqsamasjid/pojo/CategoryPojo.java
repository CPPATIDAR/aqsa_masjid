package com.aqsamasjid.pojo;

public class CategoryPojo {
	
	private long categiryId;
	private String categoryName;
	private int categoryCount;
	
	public long getCategiryId() {
		return categiryId;
	}
	public void setCategiryId(long categiryId) {
		this.categiryId = categiryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getCategoryCount() {
		return categoryCount;
	}
	public void setCategoryCount(int categoryCount) {
		this.categoryCount = categoryCount;
	}

}
