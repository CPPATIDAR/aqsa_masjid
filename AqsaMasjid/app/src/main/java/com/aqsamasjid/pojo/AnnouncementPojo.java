package com.aqsamasjid.pojo;

public class AnnouncementPojo {

	private int id;
	private String title;
	private String detail;
	private String logoImage;
	private String bannerImage;
	private String detailNoTag;

	public String getDetailNoTag() {
		return detailNoTag;
	}

	public void setDetailNoTag(String detailNoTag) {
		this.detailNoTag = detailNoTag;
	}
	
	public String getLogoImage() {
		return logoImage;
	}
	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}
	public String getBannerImage() {
		return bannerImage;
	}
	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
}
