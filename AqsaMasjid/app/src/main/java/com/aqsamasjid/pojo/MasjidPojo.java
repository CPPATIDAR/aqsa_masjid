package com.aqsamasjid.pojo;

public class MasjidPojo {
	
	private long id;
	private String name;
	private String masjid_name;
	private String street;
	private String city;
	private String state;
	private String zipcode;
	private String country;
	private boolean isActive;
	private String logoFile;
	private String createdDate;
	private String deviceToken;
	private String logoImage;
	private String bannerImage;
	private String timezone;
	private String youtubeUrl;
	private String youtubeLiveStreamUrl;
	private String facebookUrl;
	private String twitterUrl;
	private int ramdanCountdown;
	private String ramdanstartDate;
	private String lattitude;
	private String longitude;
	private String countdownLabel;
	private String isRamadan;
	
	
	
	
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMasjid_name() {
		return masjid_name;
	}
	public void setMasjid_name(String masjid_name) {
		this.masjid_name = masjid_name;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getLogoFile() {
		return logoFile;
	}
	public void setLogoFile(String logoFile) {
		this.logoFile = logoFile;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public String getLogoImage() {
		return logoImage;
	}
	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}
	public String getBannerImage() {
		return bannerImage;
	}
	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getYoutubeUrl() {
		return youtubeUrl;
	}
	public void setYoutubeUrl(String youtubeUrl) {
		this.youtubeUrl = youtubeUrl;
	}
	public String getYoutubeLiveStreamUrl() {
		return youtubeLiveStreamUrl;
	}
	public void setYoutubeLiveStreamUrl(String youtubeLiveStreamUrl) {
		this.youtubeLiveStreamUrl = youtubeLiveStreamUrl;
	}
	public String getFacebookUrl() {
		return facebookUrl;
	}
	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}
	public String getTwitterUrl() {
		return twitterUrl;
	}
	public void setTwitterUrl(String twitterUrl) {
		this.twitterUrl = twitterUrl;
	}
	public int getRamdanCountdown() {
		return ramdanCountdown;
	}
	public void setRamdanCountdown(int ramdanCountdown) {
		this.ramdanCountdown = ramdanCountdown;
	}
	public String getRamdanstartDate() {
		return ramdanstartDate;
	}
	public void setRamdanstartDate(String ramdanstartDate) {
		this.ramdanstartDate = ramdanstartDate;
	}
	public String getLattitude() {
		return lattitude;
	}
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getCountdownLabel() {
		return countdownLabel;
	}
	public void setCountdownLabel(String countdownLabel) {
		this.countdownLabel = countdownLabel;
	}
	public String getIsRamadan() {
		return isRamadan;
	}
	public void setIsRamadan(String isRamadan) {
		this.isRamadan = isRamadan;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	

}

