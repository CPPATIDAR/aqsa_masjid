package com.aqsamasjid.pojo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TasbiPojo implements Serializable{
	
	private long Id;
	private String englishName;
	private String arabicName;
	private String engDescription;
	private String arabicDescription;
	
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getArabicName() {
		return arabicName;
	}
	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}
	public String getEngDescription() {
		return engDescription;
	}
	public void setEngDescription(String engDescription) {
		this.engDescription = engDescription;
	}
	public String getArabicDescription() {
		return arabicDescription;
	}
	public void setArabicDescription(String arabicDescription) {
		this.arabicDescription = arabicDescription;
	}
	
	

}
