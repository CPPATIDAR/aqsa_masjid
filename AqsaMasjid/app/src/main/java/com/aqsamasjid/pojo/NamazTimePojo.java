package com.aqsamasjid.pojo;

public class NamazTimePojo {

	private String khutbahTime;
	private String salatTime;
	private String number;
	
	public String getKhutbahTime() {
		return khutbahTime;
	}
	public void setKhutbahTime(String khutbahTime) {
		this.khutbahTime = khutbahTime;
	}
	public String getSalatTime() {
		return salatTime;
	}
	public void setSalatTime(String salatTime) {
		this.salatTime = salatTime;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	
}
