package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.adapter.TasbiAdapter;
import com.aqsamasjid.imageloader.MasjidImageLoader;
import com.aqsamasjid.pojo.TasbiPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressLint("NewApi")
public class TasbiListActivity extends BaseActivity {

	private ArrayList<TasbiPojo> alTasbi;
	private NetworkConnection networkConnection;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private LinearLayout llParent;
	private LinearLayout llChild;
	private LayoutInflater inflater;
	private ImageView ivHome;
	private TextView tvAppLabel;

	private MasjidImageLoader imageLoader;
	private ListView lvTasbi;
	private String nextDate;
	private int pageCount = 0;
	private TextView tvEventText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tasbi_list);
		initComponents();
		initListiners();
		initSideBar();

		if (networkConnection.isOnline(getApplicationContext())) {
			new TasbiAsyncTask().execute();
		} else {
			Toast.makeText(getBaseContext(),
					"Please check your network connection.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void initComponents() {
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		llParent = (LinearLayout) findViewById(R.id.ll_parent);
		inflater = this.getLayoutInflater();
		lvTasbi = (ListView) findViewById(R.id.lv_tasbi_list);
		tvAppLabel = (TextView) findViewById(R.id.tv_masjid_label);
		tvEventText = (TextView) findViewById(R.id.tv_event_text);

		tvAppLabel.setText(SPMasjid.getValue(TasbiListActivity.this,
				SPMasjid.Masjid_Name));

		if (SPMasjid.getValue(TasbiListActivity.this,
				SPMasjid.inner_header_navigation_BackColor).length() != 0
				&& (!SPMasjid.getValue(TasbiListActivity.this,
						SPMasjid.inner_header_navigation_BackColor)
						.equalsIgnoreCase(null))) {
			tvEventText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(
					getApplicationContext(),
					SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(TasbiListActivity.this,
				SPMasjid.inner_header_navigation_BackAlpha).length() != 0
				&& (!SPMasjid.getValue(TasbiListActivity.this,
						SPMasjid.inner_header_navigation_BackAlpha)
						.equalsIgnoreCase(null))
				&& Float.parseFloat(SPMasjid.getValue(getApplicationContext(),
						SPMasjid.inner_header_navigation_BackAlpha)) > 0.0f) {
			tvEventText.setAlpha(Float.parseFloat(SPMasjid.getValue(
					getApplicationContext(),
					SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(TasbiListActivity.this,
				SPMasjid.inner_header_navigation_ForeColor).length() != 0
				&& (!SPMasjid.getValue(TasbiListActivity.this,
						SPMasjid.inner_header_navigation_ForeColor)
						.equalsIgnoreCase(null))) {
			tvEventText.setTextColor(Color.parseColor(SPMasjid.getValue(
					getApplicationContext(),
					SPMasjid.inner_header_navigation_ForeColor)));
		}

	}

	@SuppressLint("NewApi")
	private void initListiners() {
		// TODO Auto-generated method stub

		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(TasbiListActivity.this,
						HomeActivity.class).setFlags(
						Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(
						Intent.FLAG_ACTIVITY_CLEAR_TOP));

				finish();
			}
		});

		/*
		 * ivLogoWithText.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub //finish(); } });
		 */

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * Intent intent = new
				 * Intent(TasbiListActivity.this,HomeActivity.class);
				 * startActivity(intent);
				 */
				startActivity(new Intent(TasbiListActivity.this,
						HomeActivity.class).setFlags(
						Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(
						Intent.FLAG_ACTIVITY_CLEAR_TOP));
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TasbiListActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();

			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TasbiListActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (SPMasjid.getIntValue(TasbiListActivity.this,
						SPMasjid.DONATION_TYPE) == 0) {
					Intent intent = new Intent(TasbiListActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(TasbiListActivity.this,
						SPMasjid.DONATION_TYPE) == 1) {
					Intent intent = new Intent(TasbiListActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}

				finish();
			}
		});

		lvTasbi.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				startActivity(new Intent(TasbiListActivity.this,
						TasbiActivity.class).putExtra("Tabihat",
						(Serializable) alTasbi.get(position)));
			}
		});

	}

	class TasbiAsyncTask extends AsyncTask<Void, Void, Void> {

		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private TasbiPojo pojo;
		private ProgressDialog progressDialog;
		private TasbiAdapter adapter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID + ""));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(TasbiListActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.GET_TASBI_URL,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (response != null) {
					JSONArray jsonArray = new JSONArray(response);

					alTasbi = new ArrayList<TasbiPojo>();

					for (int i = 0; i < jsonArray.length(); i++) {
						jsObject = jsonArray.optJSONObject(i);
						pojo = new TasbiPojo();
						pojo.setId(jsObject.optLong("id"));
						pojo.setEnglishName(jsObject.optString("name"));
						pojo.setEngDescription(jsObject
								.optString("description"));
						pojo.setArabicName(jsObject.optString("arabic_name"));
						pojo.setArabicDescription(jsObject
								.optString("arabic_descrip"));
						alTasbi.add(pojo);

					}

					adapter = new TasbiAdapter(TasbiListActivity.this, alTasbi);
					lvTasbi.setAdapter(new TasbiAdapter(TasbiListActivity.this,
							alTasbi));

				} else {
					Toast.makeText(TasbiListActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if (arg1 == RESULT_OK && arg0 == 0) {
			finish();
		}
		super.onActivityResult(arg0, arg1, arg2);

	}

	@Override
	protected void onResume() {
		if (alTasbi != null) {

			lvTasbi.setAdapter(null);
			lvTasbi.setAdapter(new TasbiAdapter(TasbiListActivity.this, alTasbi));

		}

		super.onResume();
	}
}
