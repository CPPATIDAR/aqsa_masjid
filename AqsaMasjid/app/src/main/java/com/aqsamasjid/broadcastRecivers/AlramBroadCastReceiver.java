package com.aqsamasjid.broadcastRecivers;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.aqsamasjid.R;
import com.aqsamasjid.ResetAlarms;
import com.aqsamasjid.SplashActivity;

import java.util.Random;

import static com.aqsamasjid.AqsaMasjid.CHANNEL_ID;

public class AlramBroadCastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
      /*  if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {

            Intent serviceIntent = new Intent(context, AlramIntentService.class);
            context.startService(serviceIntent);
        } else {
*/
        String data = intent.getExtras().getString("Message");

        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(
                context, SplashActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                context, 0, notificationIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setContentText(data)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(new Random().nextInt(), builder.build());
        ResetAlarms.reset(context);

       // }

    }
}