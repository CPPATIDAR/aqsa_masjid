package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.ShowDialog;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi") public class AdvertisementActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private NetworkConnection networkConnection;
	private WebView webView;
	private TextView tvAppLabel;
	private TextView tvAdvertisementText;
	private ShowDialog showMessage;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_advertisement);
		initComponents();
		initListiners();
		initSideBar();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		/*Intent intent = new Intent(AnnouncementDetailActivity.this,
				AnnouncementActivity.class);
		startActivity(intent);*/
		finish();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		webView = (WebView) findViewById(R.id.webview_advertisement);
		tvAdvertisementText = (TextView) findViewById(R.id.tv_advertisement_text);
		tvAppLabel.setText(SPMasjid.getValue(AdvertisementActivity.this, SPMasjid.Masjid_Name));
		showMessage = new ShowDialog();
		
		
		if (SPMasjid.getValue(AdvertisementActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(AdvertisementActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvAdvertisementText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(AdvertisementActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(AdvertisementActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvAdvertisementText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(AdvertisementActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(AdvertisementActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvAdvertisementText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		if (networkConnection.isOnline(getApplicationContext())) {
			new GetMasjidLocalBusinessAsyncTask().execute();
		} else {
			Toast.makeText(getBaseContext(),
					"Please check your network connection.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(AdvertisementActivity.this, AdvertisementActivity.class));
				finish();
			}
		});
		/*tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AnnouncementDetailActivity.this,
						AnnouncementActivity.class);
				startActivity(intent);
				finish();
			}
		});*/
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(AdvertisementActivity.this, AnnouncementActivity.class));
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AdvertisementActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AdvertisementActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getIntValue(AdvertisementActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(AdvertisementActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(AdvertisementActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(AdvertisementActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
	}

	

	public void showMsgDialog(String message) {
		new AlertDialog.Builder(AdvertisementActivity.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

					}
				}).create().show();
	}
	
	class GetMasjidLocalBusinessAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(AdvertisementActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.GET_MASJID_LOCAL_BUSINESS, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			/*try {
				if (response!=null) {
						webView.loadData(response, "text/html", "UTF-8");
						WebSettings webSettings = webView.getSettings();
						webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
						webSettings.setBuiltInZoomControls(true);
						webSettings.setSupportZoom(true);
						webSettings.setDefaultFontSize((int)20);
				} else {
					Toast.makeText(AdvertisementActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}*/
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {

						if (jsonObject.optString("data").length()>0) {
							webView.loadData(jsonObject.optString("data"), "text/html", "UTF-8");
							WebSettings webSettings = webView.getSettings();
							webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
							webSettings.setBuiltInZoomControls(true);
							webSettings.setSupportZoom(true);
							webSettings.setDefaultFontSize((int)20);
						}
						else {
							//Toast.makeText(AdvertisementActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
							showMessage.showMsg(AdvertisementActivity.this, "No Business Found.");
						}
					}

				} else {
					Toast.makeText(AdvertisementActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
