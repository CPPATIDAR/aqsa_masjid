package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aqsamasjid.adapter.FridayAdapter;
import com.aqsamasjid.pojo.NamazTimePojo;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.SalatDataBase;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class FridayActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private String fridayDate;
	private SalatDataBase salatDataBase ;
	private ArrayList<NamazTimePojo> alJuma;
	private FridayAdapter adapterFridy;
	private NamazTimePojo pojo;
	private ListView listView;
	private TextView tvJumadatetext;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private TextView tvAppLabel;
private TextView tvSalatText;
private TextView tvKhubahText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friday);
		initComponents();
		initListiners();
		initSideBar();
		Intent intent = getIntent();
		fridayDate = intent.getStringExtra("FRIDAY_DATE");
		Log.v("Fridsay date", fridayDate);
		tvJumadatetext.setText("Jumuah on Fri,"+" "+intent.getStringExtra("FriConvertDate"));
		alJuma = new ArrayList<NamazTimePojo>();

		Cursor cur = salatDataBase.getFridayTime(fridayDate);
		while (cur.moveToNext()) {
			Log.v("fri namaz 2", cur.getString(2));
			Log.v("fri namaz 3", cur.getString(3));
			Log.v("fri namaz 4", cur.getString(4));
			pojo  = new NamazTimePojo();
			pojo.setKhutbahTime(cur.getString(2));
			pojo.setSalatTime(cur.getString(3));
			pojo.setNumber(cur.getString(4));
			alJuma.add(pojo);
		}
		adapterFridy = new FridayAdapter(FridayActivity.this, alJuma);
		listView.setAdapter(adapterFridy);
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		tvJumadatetext = (TextView) findViewById(R.id.tv_jumuah_text);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(FridayActivity.this, SPMasjid.Masjid_Name));
		listView = (ListView) findViewById(R.id.listview_friday);
		salatDataBase = new SalatDataBase(getApplicationContext());
		tvSalatText = (TextView) findViewById(R.id.tv_salat_text);
		tvKhubahText = (TextView) findViewById(R.id.tv_khubah_text);
		tvKhubahText.setText(SPMasjid.getValue(FridayActivity.this, SPMasjid.KHUTBA_LABEL));
		tvSalatText.setText(SPMasjid.getValue(FridayActivity.this, SPMasjid.SALAT_LABEL));


		if (SPMasjid.getValue(FridayActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(FridayActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvJumadatetext.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(FridayActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(FridayActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvJumadatetext.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(FridayActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(FridayActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvJumadatetext.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}

	}

	private void initListiners() {
		// TODO Auto-generated method stub

		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(FridayActivity.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});

		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});*/

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(FridayActivity.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(FridayActivity.this, EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(FridayActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		
		llDonate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(FridayActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(FridayActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(FridayActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(FridayActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});	

	}
}
