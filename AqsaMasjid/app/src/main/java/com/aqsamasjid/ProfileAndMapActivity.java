package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aqsamasjid.utill.SPMasjid;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


@SuppressLint("NewApi")
public class ProfileAndMapActivity extends BaseActivity implements OnMapReadyCallback {

	private GoogleMap googleMap;
	private TextView tvDirectionMap;
	private TextView tvAddress;
	private TextView tvTitle;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private LinearLayout llParent;
	private LayoutInflater inflater;
	private LinearLayout llChild;
	private double latitute;
	private double longitude;
	private TextView tvAppLabel;
	private RelativeLayout rlDirectionContainer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_and_map);
		initComponents();
		initListiners();
		initSideBar();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		tvAddress = (TextView) findViewById(R.id.tv_address);
		tvTitle = (TextView) findViewById(R.id.tv_masjid_title);
		tvDirectionMap = (TextView) findViewById(R.id.tv_direction_map);
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		llParent = (LinearLayout) findViewById(R.id.ll_parent);
		rlDirectionContainer = (RelativeLayout) findViewById(R.id.rl_direction_container);
		
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.Masjid_Name));
		inflater = this.getLayoutInflater();
		
		tvTitle.setText(SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.NAME));
		
		 latitute =	 Double.parseDouble(SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.LATITUDE));
		 longitude = Double.parseDouble(SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.LONGITUDE));

		tvAddress.setText(SPMasjid.getValue(ProfileAndMapActivity.this,
                SPMasjid.STREET)
                + " "
                + SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.CITY)
                + " "
                + SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.STATE)
                + " "
                + SPMasjid.getValue(ProfileAndMapActivity.this,
                SPMasjid.ZIPCODE));

		try {
			// Loading map
			initilizeMap();

		} catch (Exception e) {
			e.printStackTrace();
		}


		/*for (int i = 0; i < 3; i++) {
			setUser();
		}
*/
		

		if (SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_header_navigation_BackColor).length() != 0&& (!SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_header_navigation_BackColor).equalsIgnoreCase(null))) {
			tvTitle.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackColor)));
		}

		if (SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_header_navigation_BackAlpha).length() != 0&& (!SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_header_navigation_BackAlpha).equalsIgnoreCase(null))&& Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackAlpha)) > 0.0f) {
			tvTitle.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackAlpha)));
		}

		if (SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_header_navigation_ForeColor).length() != 0&& (!SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_header_navigation_ForeColor).equalsIgnoreCase(null))) {
			tvTitle.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_ForeColor)));
			
		}
		
		
		if (SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_banner_navigation_BackColor).length() != 0&& (!SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_banner_navigation_BackColor).equalsIgnoreCase(null))) {
			rlDirectionContainer.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_banner_navigation_BackColor)));
		}

		if (SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_banner_navigation_BackAlpha).length() != 0&& (!SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_banner_navigation_BackAlpha).equalsIgnoreCase(null))&& Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_banner_navigation_BackAlpha)) > 0.0f) {
			rlDirectionContainer.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_banner_navigation_BackAlpha)));
		}

		if (SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_banner_navigation_ForeColor).length() != 0&& (!SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.home_banner_navigation_ForeColor).equalsIgnoreCase(null))) {
			tvAddress.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_banner_navigation_ForeColor)));
		//	tvDirectionMap.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(),SPMasjid.home_banner_navigation_ForeColor)));

			
		}
		
		
	}

	/*private void initilizeMap() {


		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		*//*if (googleMap == null) {
			googleMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map_location)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}*//*
	}*/
	private void initilizeMap() {
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map_location);
		mapFragment.getMapAsync(this);

	}
	
	private void setUser(){
		llChild = (LinearLayout) inflater.inflate(R.layout.profile_user_info, null);
		TextView tvUser = (TextView) llChild.findViewById(R.id.tv_user_name);
		TextView tvEmail = (TextView) llChild.findViewById(R.id.tv_user_email);
		tvUser.setText("Hello");
		tvEmail.setText("A@b.com");
		llParent.addView(llChild);
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		
		ivHome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		/*ivLogoWithText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});*/
		llPrayer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ProfileAndMapActivity.this, EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ProfileAndMapActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(ProfileAndMapActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(ProfileAndMapActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(ProfileAndMapActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(ProfileAndMapActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});	
		
		tvDirectionMap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse("google.navigation:ll=" + latitute + ","
								+ longitude + "&title=Route")));

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		initilizeMap();
	}


	@Override
	public void onMapReady(GoogleMap googleMap) {
		this.googleMap = googleMap;
        MarkerOptions marker = new MarkerOptions().position(new LatLng(
                latitute, longitude));

        // GREEN color icon
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker)).title(SPMasjid.getValue(ProfileAndMapActivity.this, SPMasjid.NAME));
        // adding marker
        googleMap.addMarker(marker);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitute, longitude)).zoom(16).build();

        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

		// Add a marker in Sydney and move the camera
		/*LatLng sydney = new LatLng(-34, 151);
		this.googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
		this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

	}
}
