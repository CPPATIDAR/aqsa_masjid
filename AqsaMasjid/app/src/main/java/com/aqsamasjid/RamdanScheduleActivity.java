package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.SalatDataBase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@SuppressLint("NewApi") public class RamdanScheduleActivity extends BaseActivity {

	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private LinearLayout llParent;
	private LayoutInflater inflater;
	private LinearLayout llChild;
	private SalatDataBase salatDataBase;
	private String currentDate;
	private long currentTime;
	private TextView tvAppLabel;
	private TextView tvRamdanScheduleText;
	private LinearLayout llRamdanScheduleHeader;
	private TextView tvRamdanText;
	private TextView tvRamdanDateText;
	private TextView tvRamdanSehrTimeText;
	private TextView tvRamdanIftarTimeText;
	private TextView tvRamdanTraviTime;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ramdan_schedule);
		initComponents();
		initListiners();
		initSideBar();
		setRamdanSchedule();
	}

	private void setRamdanSchedule() {
		// TODO Auto-generated method stub
		Cursor cursor = salatDataBase.GetRamdanAllRecord();

		while (cursor.moveToNext()) {

			llChild = (LinearLayout) inflater.inflate(
					R.layout.item_ramdan_schedule, null);

			TextView tvDay = (TextView) llChild.findViewById(R.id.tv_ramdan_day);
			TextView tvDate = (TextView) llChild.findViewById(R.id.tv_ramdan_date);
			TextView tvSehrTime = (TextView) llChild.findViewById(R.id.tv_ramdan_sehr_time);
			TextView tvIftarTime = (TextView) llChild.findViewById(R.id.tv_ramdan_iftar_time);
			TextView tvTraviTime = (TextView) llChild.findViewById(R.id.tv_ramdan_travi_time);
			ImageView ivTick = (ImageView) llChild.findViewById(R.id.iv_right_arrow);

			tvDay.setText(cursor.getString(2));
			tvDate.setText(cursor.getString(1));
			tvSehrTime.setText(cursor.getString(3));
			tvIftarTime.setText(cursor.getString(4));
			tvTraviTime.setText(cursor.getString(5));

			Calendar cal = Calendar.getInstance();
			String date = cursor.getString(1);
//			Log.v("date]", date);
			String str[] = date.split("-");
//			Log.v("str[0]", str[0]);
//			Log.v("str[1]", str[1]);
//			Log.v("str[2]", str[2]);
			int month =monthInt(str[0]);
//			Log.v("month", month+"");
					
			
			cal.set(Integer.parseInt(str[2]), month, Integer.parseInt(str[1]));
			/*SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
			String localDate = dateformat.format(cal.getTime());
			Log.v("localDate date", localDate);*/
			
			long calTime = cal.getTimeInMillis(); 
			

			if (calTime<System.currentTimeMillis()) {
				ivTick.setVisibility(View.VISIBLE);
			}else {
				ivTick.setVisibility(View.GONE);
			}
			
			if (cursor.getString(1).equalsIgnoreCase(convertIntoDateFormat(currentDate))) {
				llChild.setBackgroundColor(Color.parseColor("#fbc465"));
			}

			llParent.addView(llChild);
			Log.v("DATE",cursor.getString(1));
		}
		cursor.close();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llRamdanScheduleHeader = (LinearLayout) findViewById(R.id.ll_ramdan_schedule);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		llParent = (LinearLayout) findViewById(R.id.ll_parent);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvRamdanScheduleText = (TextView) findViewById(R.id.tv_ramdan_schedule_text);
		
		tvRamdanText = (TextView) findViewById(R.id.tv_ramdan);
		tvRamdanDateText = (TextView) findViewById(R.id.tv_ramdan_date);
		tvRamdanSehrTimeText = (TextView) findViewById(R.id.tv_ramdan_sehr_time);
		tvRamdanIftarTimeText = (TextView) findViewById(R.id.tv_ramdan_iftar_time);
		tvRamdanTraviTime = (TextView) findViewById(R.id.tv_ramdan_travi_time);
		
		tvAppLabel.setText(SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.Masjid_Name));
		inflater = this.getLayoutInflater();
		salatDataBase = new SalatDataBase(getApplicationContext());
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		currentDate = dateformat.format(cal.getTime());
//		Log.v("Current date", currentDate);
		
		currentTime = getCurrentDateInMilis(currentDate);
		if (SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvRamdanScheduleText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvRamdanScheduleText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvRamdanScheduleText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		
		
		
		if (SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.home_jumuah_prayer_BackColor).length() != 0&& (!SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.home_jumuah_prayer_BackColor).equalsIgnoreCase(null))) {
	
			llRamdanScheduleHeader.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_BackColor)));

		}

		if (SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.home_jumuah_prayer_BackAlpha).length() != 0&& (!SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.home_jumuah_prayer_BackAlpha).equalsIgnoreCase(null))&& Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_BackAlpha)) > 0.0f) {
			llRamdanScheduleHeader.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_BackAlpha)));
		}

		if (SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.home_jumuah_prayer_ForeColor).length() != 0&& (!SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.home_jumuah_prayer_ForeColor).equalsIgnoreCase(null))) {
			tvRamdanText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_ForeColor)));
			tvRamdanDateText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_ForeColor)));
			tvRamdanSehrTimeText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_ForeColor)));
			tvRamdanIftarTimeText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_ForeColor)));
			tvRamdanTraviTime.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_ForeColor)));
		}
		
		tvRamdanSehrTimeText.setText(SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.SEHR_TIME));
		tvRamdanIftarTimeText.setText(SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.IFTAR_TIME));
		tvRamdanTraviTime.setText(SPMasjid.getValue(RamdanScheduleActivity.this, SPMasjid.TARAVI_TIME));
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});*/

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(RamdanScheduleActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(RamdanScheduleActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(RamdanScheduleActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(RamdanScheduleActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(RamdanScheduleActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(RamdanScheduleActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
	}

	public static String convertIntoDateFormat(String date) {
		if (date.length() > 0) {

			String[] arrDate = date.split("[-]");
			String day = arrDate[2];
			int mm = Integer.parseInt(arrDate[1]);
			String yyyy = arrDate[0];
			String month = "";
			switch (mm) {

			case 1:
				month = "Jan";
				break;
			case 2:
				month = "Feb";
				break;
			case 3:
				month = "Mar";
				break;
			case 4:
				month = "Apr";
				break;
			case 5:
				month = "May";
				break;
			case 6:
				month = "Jun";
				break;
			case 7:
				month = "Jul";
				break;
			case 8:
				month = "Aug";
				break;
			case 9:
				month = "Sept";
				break;
			case 10:
				month = "Oct";
			case 11:
				month = "Nov";
			case 12:
				month = "Dec";
				break;

			default:
				break;
			}

			return month + "-" + day + "-" + yyyy;

		}
		return "";

	}
	
	private long getCurrentDateInMilis(String time) {

		System.out.println("time to convetrt ======>" + time);
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			String stDate[] = time.split("-");
			cal.set(Integer.parseInt(stDate[0]),
					Integer.parseInt(stDate[1]),
					Integer.parseInt(stDate[2]));

			return cal.getTimeInMillis();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	
	private int monthInt(String month)
	{
		
		
		if (month.equalsIgnoreCase("Jan")) {
			//mth=0;
			return 0;
		}
		if (month.equalsIgnoreCase("Feb")) {
			//mth=1;
			return 1;
		}
		if (month.equalsIgnoreCase("Mar")) {
			//mth=2;
			return 2;
		}
		if (month.equalsIgnoreCase("Apr")) {
			//mth=3;
			return 3;
		}
		if (month.equalsIgnoreCase("May")) {
			//mth=4;
			return 4;
		}
		if (month.equalsIgnoreCase("Jun")) {
			//mth=5;
			return 5;
		}
		if (month.equalsIgnoreCase("Jul")) {
			//mth=6;
			return 6;
		}
		if (month.equalsIgnoreCase("Aug")) {
			//mth=7;
			return 7;
		}
		if (month.equalsIgnoreCase("Sep")) {
			//mth=8;
			return 8;
		}
		if (month.equalsIgnoreCase("Oct")) {
			//mth=9;
			return 9;
		}
		if (month.equalsIgnoreCase("Nov")) {
			//mth=10;
			return 10;
		}
		if (month.equalsIgnoreCase("Dec")) {
		//	//mth=11;
			return 11;
		}
		return 0;
	}
	

}
