package com.aqsamasjid;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

@SuppressLint("NewApi")
public class NewDonationActivity extends BaseActivity {

	private NetworkConnection networkConnection;
	private ImageView ivHome;
	private Button btnPayNow;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private EditText etDonationAmount;
	private EditText etEmail;
	private TextView tvAppLabel;
	private TextView tvEventText;
	private LinearLayout llEmail;
	private LinearLayout llAmount;
	//private String deviceId;
	private EditText etName;
	private EditText etPhone;
	private CheckedTextView tvAmountTwenty;
	private CheckedTextView tvAmountHundred;
	private CheckedTextView tvAmountTwoHundred;
	private CheckedTextView tvAmountFiveHundred;
	private String reason_id="";




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_donation);
		initComponents();
		initListiners();
		initSideBar();
		/*TelephonyManager manager = (TelephonyManager) getApplicationContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceId = manager.getDeviceId();*/
	}

	
	

	private void initComponents() {
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
		btnPayNow = (Button) findViewById(R.id.btn_pay_now);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		etDonationAmount = (EditText) findViewById(R.id.et_user_amount);
		etEmail = (EditText) findViewById(R.id.et_user_email);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvEventText= (TextView) findViewById(R.id.tv_event_text);
		llEmail = (LinearLayout) findViewById(R.id.ll_email);
		llAmount = (LinearLayout) findViewById(R.id.ll_amount);
		
		etName= (EditText) findViewById(R.id.et_user_name);
		etPhone = (EditText) findViewById(R.id.et_user_phone);
		tvAmountTwenty  = (CheckedTextView) findViewById(R.id.tv_amount_twenty);
		tvAmountHundred = (CheckedTextView) findViewById(R.id.tv_amount_hundred);
		tvAmountTwoHundred  = (CheckedTextView) findViewById(R.id.tv_amount_two_hundred);
		tvAmountFiveHundred  = (CheckedTextView) findViewById(R.id.tv_amount_five_hundred);
	
		tvAppLabel.setText(SPMasjid.getValue(NewDonationActivity.this, SPMasjid.Masjid_Name));
		reason_id=getIntent().getStringExtra("reason_id");
		etDonationAmount.setText("20");

		
		if (SPMasjid.getValue(NewDonationActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(NewDonationActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvEventText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(NewDonationActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(NewDonationActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvEventText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(NewDonationActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(NewDonationActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvEventText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		
		
		if (SPMasjid.getValue(NewDonationActivity.this, SPMasjid.donate_btn_BackColor).length()!=0&&(!SPMasjid.getValue(NewDonationActivity.this, SPMasjid.donate_btn_BackColor).equalsIgnoreCase(null)))
		{
			btnPayNow.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.donate_btn_BackColor)));
		}
		if (SPMasjid.getValue(NewDonationActivity.this, SPMasjid.donate_btn_alpha).length()!=0&&(!SPMasjid.getValue(NewDonationActivity.this, SPMasjid.donate_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.donate_btn_alpha))>0.0f)
		{
			btnPayNow.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.donate_btn_alpha)));
		}
		if (SPMasjid.getValue(NewDonationActivity.this, SPMasjid.donate_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(NewDonationActivity.this, SPMasjid.donate_btn_ForeColor).equalsIgnoreCase(null)))
		{
			btnPayNow.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.donate_btn_ForeColor)));
		}
	}
	
	
	
	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NewDonationActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NewDonationActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});
		
		btnPayNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (etDonationAmount.getText().length() == 0) {
					showMsgDialog("Please enter amount.");
				}
				else if (etName.getText().toString().length()==0) {
					showMsgDialog("Please enter Name.");
				}
				else if (etEmail.getText().toString().length()==0) {
					showMsgDialog("Please enter E-mail.");
				}else if (!EmailValidator.isValid(etEmail.getText().toString())) {
					showMsgDialog("please enter valid E-mail.");
				}else if (etPhone.getText().length() == 0) {
					showMsgDialog("Please enter Phone Number.");
				}else {
					if (networkConnection.isOnline(getApplicationContext())) {
						
						Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(WebUrl.DONATION_PAYMENT+"reason_id="+reason_id+"&amount="+etDonationAmount.getText().toString()+"&email="+etEmail.getText().toString()+"&name="+etName.getText().toString()+"&phone="+etPhone.getText().toString()));
						startActivity(browserIntent);	
					}else {
						Toast.makeText(NewDonationActivity.this,"No Internet Connection", Toast.LENGTH_LONG).show();
					}
					
				}
			}
		});
		
		tvAmountTwenty.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 if(((CheckedTextView) v).isChecked()){
			            ((CheckedTextView) v).setChecked(false);
			       
			        }else{
			            etDonationAmount.setText("20");
			            ((CheckedTextView) v).setChecked(true);  
			            
			            tvAmountFiveHundred.setChecked(false);
			            tvAmountTwoHundred.setChecked(false);
			            tvAmountHundred.setChecked(false);
			        }
				
			}
		});
		
		tvAmountHundred.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						 if(((CheckedTextView) v).isChecked()){
					            ((CheckedTextView) v).setChecked(false);
					          
					        }else{
					        	  etDonationAmount.setText("100");
					            ((CheckedTextView) v).setChecked(true);  
					            
					            tvAmountFiveHundred.setChecked(false);
					            tvAmountTwoHundred.setChecked(false);
					            tvAmountTwenty.setChecked(false);
					        }
						
					}
				});
		tvAmountTwoHundred.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 if(((CheckedTextView) v).isChecked()){
			            ((CheckedTextView) v).setChecked(false);
			          
			        }else{
			        	  etDonationAmount.setText("200");
			            ((CheckedTextView) v).setChecked(true);  
			          
			            tvAmountFiveHundred.setChecked(false);
			            tvAmountHundred.setChecked(false);
			            tvAmountTwenty.setChecked(false);
			        }
				
			}
		});
		tvAmountFiveHundred.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 if(((CheckedTextView) v).isChecked()){
			            ((CheckedTextView) v).setChecked(false);
			           
			        }else{
			        	 etDonationAmount.setText("500");
			            ((CheckedTextView) v).setChecked(true);   
			            tvAmountTwoHundred.setChecked(false);
			            tvAmountHundred.setChecked(false);
			            tvAmountTwenty.setChecked(false);
			        }
				
			}
		});
	}
	
	public void showMsgDialog(String message) {
		new AlertDialog.Builder(NewDonationActivity.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				}).create().show();
	}

	
	
	
	
}


