package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.Webimageloader.ImageLoader;
import com.aqsamasjid.imageloader.MasjidImageLoader;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

@SuppressLint("NewApi") public class EventDetailActivity extends BaseActivity {
	
	private TextView tvTitle;
//	private TextView tvDetail;
	private TextView tvWebsite;
	private EditText etFirstname;
	private EditText etEmail;
	private EditText etMobile;
	private Button btnRegisterToday;
	private Dialog dialog;
	private NetworkConnection networkConnection;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private TextView tvDate;
	private TextView tvTime;
	private TextView tvHostName;
	private TextView tvDatetime;
	private TextView tvRSVPNow;
	private String eventId;
	private TextView tvTExtHeader;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private TextView tvToDate;
	private ImageView ivBanner;
	private MasjidImageLoader imageLoader;
	private TextView tvRSVP;
	private ImageLoader webImageLoader;
	private WebView webView;
	private TextView tvAppLabel;
	private TextView tvEventDetailText;
	private LinearLayout llBannerDetailContainer;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_detail);
		initComponents();
		initListiners();
		initSideBar();
		setdata();
/*		imageLoader = new MasjidImageLoader(EventDetailActivity.this);
		Intent intent = getIntent();
		eventId = intent.getStringExtra("EventId");
		tvTitle.setText(intent.getStringExtra("EventTitle"));
//		tvDetail.setText(intent.getStringExtra("EventDetail"));
		tvWebsite.setText(": "+intent.getStringExtra("WeekDay"));
		tvDate.setText(": "+intent.getStringExtra("EventFromDate"));
		tvToDate.setText(": "+intent.getStringExtra("EventToDate"));
		tvTime.setText(": "+intent.getStringExtra("EventStartTime")+" - "+intent.getStringExtra("EventEndTime"));
		tvHostName.setText(": "+intent.getStringExtra("EventHostName"));
		tvRSVP.setText(": "+intent.getStringExtra("RSVP"));
		
		webView.loadData(intent.getStringExtra("EventDetail"), "text/html", "UTF-8");
//		webView.loadDataWithBaseURL(null, intent.getStringExtra("EventDetail"), "text/html", "UTF-8", null);
		WebSettings webSettings = webView.getSettings();
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		
		Calendar calendar = Calendar.getInstance();
		String str[] = intent.getStringExtra("EventFromDate").split("-");
		calendar.set(Integer.parseInt(str[0]), Integer.parseInt(str[1])-1, Integer.parseInt(str[2]));
		SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		String weekDay = null;

		switch (day) {
		case Calendar.SUNDAY:
			weekDay = "Sun";
			break;
		case Calendar.MONDAY:
			weekDay = "Mon";
			break;
		case Calendar.TUESDAY:
			weekDay = "Tue";
			break;
		case Calendar.WEDNESDAY:
			weekDay = "Wed";
			break;
		case Calendar.THURSDAY:
			weekDay = "Thu";
			break;
		case Calendar.FRIDAY:
			weekDay = "Fri";
			break;
		case Calendar.SATURDAY:
			weekDay = "Sat";
			break;

		default:
			break;
		}
		String formattedDate = df.format(calendar.getTime());
		tvDatetime.setText(weekDay+", "+formattedDate+". "+intent.getStringExtra("EventStartTime")+" - "+intent.getStringExtra("EventEndTime"));
//		tvDatetime.setText(intent.getStringExtra("EventFromDate")+". "+intent.getStringExtra("EventStartTime")+" - "+intent.getStringExtra("EventEndTime"));
//		tvDetail.setMovementMethod(new ScrollingMovementMethod());
		
		webImageLoader = new ImageLoader(EventDetailActivity.this);
	
		try {
			String imageUrl = intent.getStringExtra("EventPosterImage").toString();
			webImageLoader.DisplayImage(imageUrl, ivBanner);
			} catch (Exception ex) {
			System.out.println("Error :  Image URL " + ex);
			ex.printStackTrace();
		}*/
	}

	private void setdata() {
		// TODO Auto-generated method stub
		imageLoader = new MasjidImageLoader(EventDetailActivity.this);
		Intent intent = getIntent();
		eventId = intent.getStringExtra("EventId");
		tvTitle.setText(intent.getStringExtra("EventTitle"));
//		tvDetail.setText(intent.getStringExtra("EventDetail"));
		tvWebsite.setText(": "+intent.getStringExtra("WeekDay"));
		tvDate.setText(": "+intent.getStringExtra("EventFromDate"));
		tvToDate.setText(": "+intent.getStringExtra("EventToDate"));
		tvTime.setText(": "+intent.getStringExtra("EventStartTime")+" - "+intent.getStringExtra("EventEndTime"));
		tvHostName.setText(": "+intent.getStringExtra("EventHostName"));
		tvRSVP.setText(": "+intent.getStringExtra("RSVP"));
		
		webView.loadData(intent.getStringExtra("EventDetail"), "text/html", "UTF-8");
//		webView.loadDataWithBaseURL(null, intent.getStringExtra("EventDetail"), "text/html", "UTF-8", null);
		WebSettings webSettings = webView.getSettings();
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultFontSize((int)20);
		
		Calendar calendar = Calendar.getInstance();
		String str[] = intent.getStringExtra("EventFromDate").split("-");
		calendar.set(Integer.parseInt(str[0]), Integer.parseInt(str[1])-1, Integer.parseInt(str[2]));
		SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		String weekDay = null;

		switch (day) {
		case Calendar.SUNDAY:
			weekDay = "Sun";
			break;
		case Calendar.MONDAY:
			weekDay = "Mon";
			break;
		case Calendar.TUESDAY:
			weekDay = "Tue";
			break;
		case Calendar.WEDNESDAY:
			weekDay = "Wed";
			break;
		case Calendar.THURSDAY:
			weekDay = "Thu";
			break;
		case Calendar.FRIDAY:
			weekDay = "Fri";
			break;
		case Calendar.SATURDAY:
			weekDay = "Sat";
			break;

		default:
			break;
		}
		String formattedDate = df.format(calendar.getTime());
		tvDatetime.setText(weekDay+", "+formattedDate+". "+intent.getStringExtra("EventStartTime")+" - "+intent.getStringExtra("EventEndTime"));
		
		webImageLoader = new ImageLoader(EventDetailActivity.this);
	
		try {
			String imageUrl = intent.getStringExtra("EventPosterImage").toString();
			webImageLoader.DisplayImage(imageUrl, ivBanner);
			
			} catch (Exception ex) {
			System.out.println("Error :  Image URL " + ex);
			ex.printStackTrace();
		}
	
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		tvTitle = (TextView) findViewById(R.id.tv_detail_title);
//		tvDetail = (TextView) findViewById(R.id.tv_detail_description);
		tvWebsite = (TextView) findViewById(R.id.tv_detail_website);
		btnRegisterToday = (Button) findViewById(R.id.btn_register_today);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		tvDate = (TextView) findViewById(R.id.tv_detail_from_date);
		tvTime = (TextView) findViewById(R.id.tv_detail_time);
		tvHostName = (TextView) findViewById(R.id.tv_detail_host_name);
		tvDatetime = (TextView) findViewById(R.id.tv_detail_date_time);
		tvRSVPNow = (TextView) findViewById(R.id.tv_rsvp_now);
		tvToDate = (TextView) findViewById(R.id.tv_detail_to_date);
		ivBanner = (ImageView) findViewById(R.id.iv_banner_detail);
		tvRSVP = (TextView) findViewById(R.id.tv_detail_rsvp_name);
//		tvDetail.setMovementMethod(new ScrollingMovementMethod());
		webView  = (WebView) findViewById(R.id.webview_event_detail);
		tvEventDetailText= (TextView) findViewById(R.id.tv_event_detail_text);
		llBannerDetailContainer= (LinearLayout) findViewById(R.id.ll_banner_detail_container);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(EventDetailActivity.this, SPMasjid.Masjid_Name));
	

		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvEventDetailText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvEventDetailText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvEventDetailText.setTextColor(Color.parseColor(SPMasjid.getValue(EventDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_BackColor).equalsIgnoreCase(null))) {
			llBannerDetailContainer.setBackgroundColor(Color.parseColor(SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_BackColor)));
		}
		
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha))>0.0f) {
			llBannerDetailContainer.setAlpha(Float.parseFloat(SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_BackAlpha)));

		}
		
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor).equalsIgnoreCase(null))) {
			tvTitle.setTextColor(Color.parseColor(SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor)));
			tvDatetime.setTextColor(Color.parseColor(SPMasjid.getValue(EventDetailActivity.this, SPMasjid.home_banner_navigation_ForeColor)));
			}
		
		
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.application_btn_BackColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.application_btn_BackColor).equalsIgnoreCase(null)))
		{
			btnRegisterToday.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_BackColor)));
		}
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.application_btn_alpha).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.application_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha))>0.0f)
		{
			btnRegisterToday.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha)));
		}
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.application_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.application_btn_ForeColor).equalsIgnoreCase(null)))
		{
			btnRegisterToday.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_ForeColor)));
		}
		
		

		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.rsvp_btn_BackColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.rsvp_btn_BackColor).equalsIgnoreCase(null)))
		{
			tvRSVPNow.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.rsvp_btn_BackColor)));
		}
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.rsvp_btn_alpha).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.rsvp_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.rsvp_btn_alpha))>0.0f)
		{
			tvRSVPNow.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.rsvp_btn_alpha)));
		}
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.rsvp_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.rsvp_btn_ForeColor).equalsIgnoreCase(null)))
		{
			tvRSVPNow.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.rsvp_btn_ForeColor)));
		}
	
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				/*Intent intent = new Intent(EventDetailActivity.this,HomeActivity.class);
				startActivity(intent);*/
				setResult(RESULT_OK,new Intent(EventDetailActivity.this, EventActivity.class));
				finish();
			}
		});

		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EventDetailActivity.this,EventActivity.class);
				startActivity(intent);
				finish();
			}
		});*/

		btnRegisterToday.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String msg = "Volunteer";
				dialogForRegistration(msg);
			}
		});

		tvRSVPNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String msg = "Rsvp";
				dialogForRegistration(msg);
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(EventDetailActivity.this,HomeActivity.class);
				startActivity(intent);*/
				setResult(RESULT_OK,new Intent(EventDetailActivity.this, EventActivity.class));
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EventDetailActivity.this, EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EventDetailActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		
		llDonate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(EventDetailActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(EventDetailActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(EventDetailActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(EventDetailActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	/*	Intent intent = new Intent(EventDetailActivity.this,EventActivity.class);
		startActivity(intent);*/
		finish();
	}

	protected void dialogForRegistration(final String message) {
		// TODO Auto-generated method stub
		
		dialog = new Dialog(EventDetailActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_registration);
		tvTExtHeader = (TextView) dialog.findViewById(R.id.tv_text_header);
		etFirstname = (EditText) dialog.findViewById(R.id.et_name);
		etEmail = (EditText) dialog.findViewById(R.id.et_email);
		etMobile = (EditText) dialog.findViewById(R.id.et_mobile);
		Button btnSubmit = (Button) dialog.findViewById(R.id.btn_submit);
		Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancal);
		
		if (message.equalsIgnoreCase("Volunteer")) {
			tvTExtHeader.setText("Become a volunteer");
		} else {
			tvTExtHeader.setText("Register for RSPV");
		}
		
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.header_BackColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.header_BackColor).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));
			btnSubmit.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));
			btnCancel.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));

		}
		
		
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.header_BackAlpha).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.header_BackAlpha).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
			btnSubmit.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
			btnCancel.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));

		}
		
		if (SPMasjid.getValue(EventDetailActivity.this, SPMasjid.header_ForeColor).length()!=0&&(!SPMasjid.getValue(EventDetailActivity.this, SPMasjid.header_ForeColor).equalsIgnoreCase(null)))
		{
			tvTExtHeader.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));
			btnSubmit.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));
			btnCancel.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));

		}
		
		
		dialog.show();
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (etFirstname.getText().toString().length()==0) {
					showMsgDialog("Please enter first name");
				}else if (etEmail.getText().toString().length()==0) {
					showMsgDialog("Please enter E-mail.");
				}else if (!EmailValidator.isValid(etEmail.getText().toString())) {
					showMsgDialog("please enter valid E-mail.");
				}else if (etMobile.getText().toString().length()==0) {
					showMsgDialog("Please enter phone number.");
				}else if(etMobile.getText().toString().trim().length() != 10) {
					showMsgDialog("Please enter valid phone number.");
				}
				else {
					if (networkConnection.isOnline(getApplicationContext())) {
						if (message.equalsIgnoreCase("Volunteer")) {
							new RegistrationAsyncTask().execute();
						} else {
							new RSPVAsyncTask().execute();
						}
						
					} else {
						Toast.makeText(getBaseContext(),
								"Please check your network connection.", Toast.LENGTH_SHORT).show();
					}
					
				}
			}
		});
	}
	
	public void showMsgDialog(String message) {
		new AlertDialog.Builder(EventDetailActivity.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

					}
				}).create().show();
	}
	
	class RegistrationAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
	
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("firstname",etFirstname.getText().toString()));
			urlParameter.add(new BasicNameValuePair("email",etEmail.getText().toString()));
			urlParameter.add(new BasicNameValuePair("mobile",etMobile.getText().toString()));
			urlParameter.add(new BasicNameValuePair("type","event"));
			urlParameter.add(new BasicNameValuePair("object_id",eventId));
			
			progressDialog = new ProgressDialog(EventDetailActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter Registration", urlParameter+"");
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.SignUpVoluentierUrl, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}else {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}
					
				} else {
					Toast.makeText(EventDetailActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	class RSPVAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("event_id",eventId));
			urlParameter.add(new BasicNameValuePair("firstname",etFirstname.getText().toString()));
			urlParameter.add(new BasicNameValuePair("email",etEmail.getText().toString()));
			urlParameter.add(new BasicNameValuePair("mobile",etMobile.getText().toString()));
			progressDialog = new ProgressDialog(EventDetailActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter RSPV", urlParameter+"");
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.RSPVNow, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}else {
						showMsgDialog(jsonObject.optString("message"));
						dialog.dismiss();
					}
					
				} else {
					Toast.makeText(EventDetailActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
