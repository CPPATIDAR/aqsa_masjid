package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;

@SuppressLint("NewApi")
public class QuestioAnswerDetailActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private NetworkConnection networkConnection;
	private WebView webView;
	private TextView tvAppLabel;
	private TextView tvTitle;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_question_answer);
		initComponents();
		initListiners();
		initSideBar();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		/*Intent intent = new Intent(AnnouncementDetailActivity.this,
				AnnouncementActivity.class);
		startActivity(intent);*/
		finish();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		webView = (WebView) findViewById(R.id.webview_ques_answ);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvTitle= (TextView) findViewById(R.id.tv_about_us_text);
		tvAppLabel.setText(SPMasjid.getValue(QuestioAnswerDetailActivity.this, SPMasjid.Masjid_Name));
		
		Intent intent = getIntent();
		
		String question = "<H4>"+intent.getStringExtra("Question")+"</H4><body>"+"<p>"+intent.getStringExtra("Detail")+"</p>";
		
		webView.loadData(question, "text/html", "UTF-8");
		WebSettings webSettings = webView.getSettings();
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setDefaultFontSize((int)20);
		
		if (SPMasjid.getValue(QuestioAnswerDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(QuestioAnswerDetailActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvTitle.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(QuestioAnswerDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(QuestioAnswerDetailActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvTitle.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(QuestioAnswerDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(QuestioAnswerDetailActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvTitle.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		 
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(QuestioAnswerDetailActivity.this, AskImamLatestQAActivity.class));
				finish();
			}
		});
		tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setResult(RESULT_OK,new Intent(QuestioAnswerDetailActivity.this, AnnouncementActivity.class));
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(QuestioAnswerDetailActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(QuestioAnswerDetailActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(QuestioAnswerDetailActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(QuestioAnswerDetailActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(QuestioAnswerDetailActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(QuestioAnswerDetailActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
			
		});
	}

	

	public void showMsgDialog(String message) {
		new AlertDialog.Builder(QuestioAnswerDetailActivity.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();

					}
				}).create().show();
	}
	
}
