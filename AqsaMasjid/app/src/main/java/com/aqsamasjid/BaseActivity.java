package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import java.util.Calendar;

@SuppressLint("NewApi") public class BaseActivity extends FragmentActivity {

	private DisplayMetrics metrics;
	private int panelWidth;
//	private RelativeLayout headerPanel;
//	private LayoutParams headerPanelParameters;
	private LinearLayout menuPanel;
	private LinearLayout.LayoutParams menuPanelParameters;
	private LinearLayout slidingPanel;
	private FrameLayout.LayoutParams slidingPanelParameters;
//	private LinearLayout scroll;
//	private android.widget.LinearLayout.LayoutParams listViewParameters;
//	private Button menuViewButton;

//	private RelativeLayout rlHead;
	private LinearLayout llHome;
	private LinearLayout llEvents;
	private LinearLayout llService;
	private LinearLayout llDonate;
	private LinearLayout llProgram;
	private LinearLayout llJoinNewsLetter;
	private LinearLayout llSettings;
	private LinearLayout llYoutubeChannel;
	private LinearLayout llFaceBook;
	private LinearLayout llRamdanSchedule;
	private LinearLayout llCopyRight;
	private LinearLayout llAskImam;
	private LinearLayout llAboutUs;
	private LinearLayout llContactUs;
	private boolean isExpanded;
	//protected ScrollView scroll_sidebar_parent;
	protected ImageView btn_menu;
	protected Activity home;
//	private NetworkConnection networkConnection = new NetworkConnection(); 
	private TextView tvCopyright;
protected RelativeLayout rlHeaderParent;
protected TextView tvHeaderLabel;
private LinearLayout llFooterDonate;
private LinearLayout llFooterServices;
private LinearLayout llFooterEvents;
private LinearLayout llFooterPrayer;
private TextView tvFooterDonate;
private TextView tvFooterEvents;
private TextView tvFooterServices;
private TextView tvFooterPrayer;
private RelativeLayout rlHeaderSidebar;
protected ImageView imYoutubeChannel;
protected ImageView imFaceBook;
protected ImageView imTwitter;
private LinearLayout llLocalBusiness;
private LinearLayout llDailyDua;
	private LinearLayout llTasbi;
protected TextView tvSidebarHeaderTitle;
protected ImageView imWhatsapp;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
	}

	protected void initSideBar() {
		btn_menu = (ImageView) findViewById(R.id.iv_sidebar_icon);//////click side bar image
		rlHeaderParent= (RelativeLayout) findViewById(R.id.rl_header_parent);
		tvHeaderLabel= (TextView) findViewById(R.id.tv_masjid_label);
		llHome = (LinearLayout) findViewById(R.id.ll_prayer_side);
		llEvents = (LinearLayout) findViewById(R.id.ll_events_side);
		llService = (LinearLayout) findViewById(R.id.ll_services_side);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate_side);
		llProgram = (LinearLayout) findViewById(R.id.ll_programs_and_announcement_side);
		llJoinNewsLetter = (LinearLayout) findViewById(R.id.ll_join_news_letter_side);
		llSettings = (LinearLayout) findViewById(R.id.ll_settings_side);
		llYoutubeChannel = (LinearLayout) findViewById(R.id.ll_youtube_channel_side);
		llFaceBook = (LinearLayout) findViewById(R.id.ll_facebook_side);
		llRamdanSchedule = (LinearLayout) findViewById(R.id.ll_ramdan_schedule_side);
		llCopyRight = (LinearLayout) findViewById(R.id.ll_copyright_side);
		llAskImam  = (LinearLayout) findViewById(R.id.ll_ask_imam_side);
		llAboutUs  = (LinearLayout) findViewById(R.id.ll_about_us_side);
		llContactUs = (LinearLayout) findViewById(R.id.ll_contact_us_side);
		llTasbi = (LinearLayout) findViewById(R.id.ll_tasbi);
		tvCopyright = (TextView) findViewById(R.id.tv_copy_right);
		rlHeaderSidebar= (RelativeLayout) findViewById(R.id.rl_head);
		imYoutubeChannel = (ImageView) findViewById(R.id.im_youtube);
		imFaceBook = (ImageView) findViewById(R.id.im_fb);
		imTwitter = (ImageView) findViewById(R.id.im_twitter);
		imWhatsapp = (ImageView) findViewById(R.id.im_whatsapp);
		llLocalBusiness = (LinearLayout) findViewById(R.id.ll_local_business);
		llDailyDua = (LinearLayout) findViewById(R.id.ll_daily_dua_side);
		llFooterDonate= (LinearLayout) findViewById(R.id.ll_donate);
		llFooterServices= (LinearLayout) findViewById(R.id.ll_services);
		llFooterEvents= (LinearLayout) findViewById(R.id.ll_events);
		llFooterPrayer= (LinearLayout) findViewById(R.id.ll_prayer);

		tvFooterDonate= (TextView) findViewById(R.id.tv_footer_donate);
		tvFooterServices= (TextView) findViewById(R.id.tv_footer_services);
		tvFooterEvents= (TextView) findViewById(R.id.tv_footer_events);
		tvFooterPrayer= (TextView) findViewById(R.id.tv_footer_prayer);

		tvSidebarHeaderTitle= (TextView) findViewById(R.id.tv_sidebar_header_title);
		tvSidebarHeaderTitle.setText(SPMasjid.getValue(getApplicationContext(), SPMasjid.NAME));
		rlHeaderSidebar.setBackgroundColor(Color.parseColor("#3CC6FC"));
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.header_BackColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.header_BackColor).equalsIgnoreCase(null)))
		{
			
			rlHeaderParent.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));
		}
		
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.header_BackAlpha).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.header_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha))>0.0f)
		{
			rlHeaderParent.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.header_ForeColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.header_ForeColor).equalsIgnoreCase(null)))
		{
			tvHeaderLabel.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));
		}
		
		
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.header_BackColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.header_BackColor).equalsIgnoreCase(null)))
		{
			rlHeaderSidebar.setBackgroundResource(R.drawable.header1);
			rlHeaderSidebar.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));
		}
		
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.header_BackAlpha).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.header_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha))>0.0f)
		{
			rlHeaderSidebar.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
		}
		
	
		

		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_donation_BackColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_donation_BackColor).equalsIgnoreCase(null)))
		{
			llFooterDonate.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_donation_BackColor)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_donation_BackAlpha).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_donation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_donation_BackAlpha))>0.0f)
		{
			llFooterDonate.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_donation_BackAlpha)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_donation_ForeColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_donation_ForeColor).equalsIgnoreCase(null)))
		{
			tvFooterDonate.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_donation_ForeColor)));
		}
		
		
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_services_BackColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_services_BackColor).equalsIgnoreCase(null)))
		{
			llFooterServices.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_services_BackColor)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_services_BackAlpha).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_services_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_services_BackAlpha))>0.0f)
		{
			llFooterServices.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_services_BackAlpha)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_services_ForeColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_services_ForeColor).equalsIgnoreCase(null)))
		{
			tvFooterServices.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_services_ForeColor)));
		}
		
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_event_BackColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_event_BackColor).equalsIgnoreCase(null)))
		{
			llFooterEvents.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_event_BackColor)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_event_BackAlpha).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_event_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_event_BackAlpha))>0.0f)
		{
			llFooterEvents.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_event_BackAlpha)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_event_ForeColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_event_ForeColor).equalsIgnoreCase(null)))
		{
			tvFooterEvents.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_event_ForeColor)));
		}
		
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_prayer_BackColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_prayer_BackColor).equalsIgnoreCase(null)))
		{
			llFooterPrayer.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_prayer_BackColor)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_prayer_BackAlpha).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_prayer_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_prayer_BackAlpha))>0.0f)
		{
			llFooterPrayer.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_prayer_BackAlpha)));
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_prayer_ForeColor).length()!=0&&(!SPMasjid.getValue(BaseActivity.this, SPMasjid.home_btn_prayer_ForeColor).equalsIgnoreCase(null)))
		{
			tvFooterPrayer.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_btn_prayer_ForeColor)));
		}
		
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.MOBLIE_NO).length()!=0) {
			imWhatsapp.setVisibility(View.VISIBLE);
		}else {
			imWhatsapp.setVisibility(View.GONE);
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.URL_Youtube_CHANNEL).length()!=0) {
			imYoutubeChannel.setVisibility(View.VISIBLE);
		}else {
			imYoutubeChannel.setVisibility(View.GONE);
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.URL_FACEBOOK).length()!=0) {
			imFaceBook.setVisibility(View.VISIBLE);
		}else {
			imFaceBook.setVisibility(View.GONE);
		}
		
		if (SPMasjid.getValue(BaseActivity.this, SPMasjid.URL_TWITTER).length()!=0) {
			imTwitter.setVisibility(View.VISIBLE);
		}else {
			imTwitter.setVisibility(View.GONE);
		}
		
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		panelWidth = (int) ((metrics.widthPixels)*-0.85);

		menuPanel = (LinearLayout) findViewById(R.id.menuPanel);
		menuPanelParameters = (LinearLayout.LayoutParams) menuPanel
				.getLayoutParams();
		menuPanelParameters.width = panelWidth;
		menuPanel.setLayoutParams(menuPanelParameters);
		menuPanel.setVisibility(View.GONE);
		slidingPanel = (LinearLayout) findViewById(R.id.slidingPanel);
		slidingPanelParameters = (FrameLayout.LayoutParams) slidingPanel
				.getLayoutParams();
		slidingPanelParameters.width = metrics.widthPixels;
		slidingPanel.setLayoutParams(slidingPanelParameters);

		btn_menu.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				if (!isExpanded) {
					isExpanded = true;
					if (menuPanel.getVisibility() == View.GONE) {
						menuPanel.setVisibility(View.VISIBLE);
						try {
							InputMethodManager mgr = (InputMethodManager) BaseActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
				            mgr.hideSoftInputFromWindow(BaseActivity.this.getCurrentFocus().getWindowToken(), 0);
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					// Expand
					Log.e("SlideMenu=","ExpandAnimation");
					new ExpandAnimation(slidingPanel, panelWidth,
							Animation.RELATIVE_TO_SELF, 0.0f,
							Animation.RELATIVE_TO_SELF, 0.85f, 0, 0.0f, 0, 0.0f);
				} else {
					isExpanded = false;
					// Collapse
					Log.e("SlideMenu=","CollapseAnimation");

					new CollapseAnimation(menuPanel, slidingPanel, panelWidth,
							TranslateAnimation.RELATIVE_TO_SELF, 0.85f,
							TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f,
							0, 0.0f);
				}
			}
		});
		
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		tvCopyright.setText(year + " " + getResources().getString(R.string.copy_right_madina_apps));

		llHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (home != null) {
					close();
				} else {
					startActivity(new Intent(BaseActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
					finish();

				}
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(BaseActivity.this, EventActivity.class));
//				close();
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});
		
		llService.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(BaseActivity.this, ServiceActivity.class));
//				close();
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});
		
		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(BaseActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(BaseActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(BaseActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(BaseActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
//				close();
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});
		
		llJoinNewsLetter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(BaseActivity.this, JoinNewsLetter.class));
//				close();
//				finish();
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});
		
		llRamdanSchedule.setOnClickListener(new OnClickListener() {

			@Override	
			public void onClick(View v) {
				startActivity(new Intent(BaseActivity.this, RamdanScheduleActivity.class));
//				close();
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});
		
		llDailyDua.setOnClickListener(new OnClickListener() {

			@Override	
			public void onClick(View v) {
				Intent intent = new Intent(BaseActivity.this,
						MasjidUpdateActivity.class);
				startActivity(intent);
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});

		llSettings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(BaseActivity.this, SettingActivity.class));
//				close();
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}				
			}
		});


		llLocalBusiness.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(BaseActivity.this, AdvertisementActivity.class));
//				close();
				if(home!=null)
				{
					close();
				}else{
					finish();
				}
			}
		});

		llFaceBook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(SPMasjid.getValue(BaseActivity.this, SPMasjid.URL_FACEBOOK)));
					startActivity(newIntent);
					close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});

		llYoutubeChannel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(SPMasjid.getValue(BaseActivity.this, SPMasjid.URL_Youtube_CHANNEL)));
					startActivity(newIntent);
					close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		
		
		imFaceBook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(SPMasjid.getValue(BaseActivity.this, SPMasjid.URL_FACEBOOK)));
					startActivity(newIntent);
					close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});

		imYoutubeChannel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(SPMasjid.getValue(BaseActivity.this, SPMasjid.URL_Youtube_CHANNEL)));
					startActivity(newIntent);
					close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		
		
		imTwitter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(SPMasjid.getValue(BaseActivity.this, SPMasjid.URL_TWITTER)));
					startActivity(newIntent);
					close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		
		imWhatsapp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					try {
						Uri uri = Uri.parse("smsto:" +  SPMasjid.getValue(BaseActivity.this, SPMasjid.MOBLIE_NO));
						Intent i = new Intent(Intent.ACTION_SENDTO, uri);
						i.putExtra("sms_body", "");  
						i.setPackage("com.whatsapp");  
						startActivity(i);
					} catch (Exception e) {
						e.printStackTrace();
						
						 Toast.makeText(BaseActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
			                .show();
					}
					close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		
		
		llCopyRight.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
//				if (networkConnection.isOnline(getApplicationContext())) {
					try {
						Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(WebUrl.COPYRIGHT_URL));
						startActivity(newIntent);
						close();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
//				} else {
//				Toast.makeText(getBaseContext(),
//						"Please check your network connection.", Toast.LENGTH_SHORT).show();
//				}
				
			}
		});

		llProgram.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(BaseActivity.this, AnnouncementActivity.class));
//				close();
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
//				finish();
			}
		});
		
		llAskImam.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(BaseActivity.this, AskImamLatestQAActivity.class));
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});
		
		llAboutUs.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(BaseActivity.this, AboutUsActivity.class));
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});
		

	llTasbi.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(BaseActivity.this, TasbiListActivity.class).setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT));
				if(home!=null)
				{
					close();
				}else{
					finish();
				}
			}
		});
		
		llContactUs.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(BaseActivity.this, ContactUsListActivity.class));
				if(home!=null)
				{
					close();
				}else{
					finish();	
				}
			}
		});
	}
	void close()
	{
		isExpanded = false;

		// Collapse
		new CollapseAnimation(menuPanel, slidingPanel, panelWidth,
				TranslateAnimation.RELATIVE_TO_SELF, -1.0f,
				TranslateAnimation.RELATIVE_TO_SELF, 0.0f, 0, 0.0f,
				0, 0.0f);
	}
}
