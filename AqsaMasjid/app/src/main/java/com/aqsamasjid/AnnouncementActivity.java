package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.adapter.AnnouncementAdapter;
import com.aqsamasjid.pojo.AnnouncementPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class AnnouncementActivity extends BaseActivity {

	private NetworkConnection networkConnection;
	private ArrayList<AnnouncementPojo> alAnnouncement;
	private AnnouncementAdapter adapterAnnouncement;
	private ListView listView;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private int pageCount = 0;
	private boolean canSend = false;
	private TextView tvAppLabel;
	private TextView tvProgramText;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_announcement);
		initComponents();
		initListiners();
		initSideBar();

		if (networkConnection.isOnline(getApplicationContext())) {
			new AnnouncementAsyncTask().execute();
		} else {
			Toast.makeText(getBaseContext(),
					"Please check your network connection.", Toast.LENGTH_SHORT)
					.show();
		}
	}
	


	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		listView = (ListView) findViewById(R.id.listView);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		tvProgramText= (TextView) findViewById(R.id.tv_program_text);
		tvAppLabel.setText(SPMasjid.getValue(AnnouncementActivity.this, SPMasjid.Masjid_Name));
		
		if (SPMasjid.getValue(AnnouncementActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(AnnouncementActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvProgramText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(AnnouncementActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(AnnouncementActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvProgramText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(AnnouncementActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(AnnouncementActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvProgramText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
	
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(AnnouncementActivity.this,
//						HomeActivity.class);
//				startActivity(intent);
				finish();
			}
		});
		
		tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});

		/*ivLogoWithText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});*/

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AnnouncementActivity.this,
						AnnouncementDetailActivity.class);
				intent.putExtra("Title", alAnnouncement.get(position)
						.getTitle());
				intent.putExtra("Detail", alAnnouncement.get(position)
						.getDetail());
				intent.putExtra("Banner_Image", alAnnouncement.get(position).getBannerImage());
				startActivityForResult(intent,0);
				//startActivity(intent);
				//finish();
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(AnnouncementActivity.this,HomeActivity.class);
//				startActivity(intent);
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AnnouncementActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AnnouncementActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getIntValue(AnnouncementActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(AnnouncementActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(AnnouncementActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(AnnouncementActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});

		listView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub

				if (firstVisibleItem > (totalItemCount - 8) && canSend
						&& totalItemCount > 10) {
					canSend = false;
					if (networkConnection.isOnline(getApplicationContext())) {
						new AnnouncementAsyncTask().execute();
					} else {
						Toast.makeText(getBaseContext(),
								"Please check your network connection.",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
	}

	class AnnouncementAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private AnnouncementPojo pojo;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("page_number", pageCount+ ""));
			Log.v("urlParameter", urlParameter + "");
			progressDialog = new ProgressDialog(AnnouncementActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.ANNOUNCEMENT,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (response != null) {
					Log.v("Response", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						alAnnouncement = new ArrayList<AnnouncementPojo>();

						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);

							pojo = new AnnouncementPojo();
							pojo.setId(jsObject.optInt("id"));
							pojo.setTitle(jsObject.optString("title"));
							pojo.setDetail(jsObject.optString("details"));
							pojo.setDetailNoTag(jsObject.optString("detailnotag"));
							pojo.setLogoImage(jsObject.optString("logo_image"));
							pojo.setBannerImage(jsObject.optString("poster_image"));

							alAnnouncement.add(pojo);
						}
						adapterAnnouncement = new AnnouncementAdapter(
								AnnouncementActivity.this, alAnnouncement);
						listView.setAdapter(adapterAnnouncement);
						pageCount++;
						canSend = true;
					}
				} else {
					Toast.makeText(AnnouncementActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if (arg1 == RESULT_OK && arg0 ==0){
			finish();
		}
		super.onActivityResult(arg0, arg1, arg2);
		
	}
}
