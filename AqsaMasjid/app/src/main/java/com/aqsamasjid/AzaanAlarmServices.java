package com.aqsamasjid;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.aqsamasjid.utill.NotificationUtil;

import java.util.Random;

import static com.aqsamasjid.AqsaMasjid.CHANNEL_ID;

public class AzaanAlarmServices extends Service {

	PendingIntent pendingAlarmIntent;

	@Override
	public void onCreate() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);

		try {
			if (intent != null) {

				Bundle extras = intent.getExtras();
				if (extras != null) {

					String data = extras.getString("Message");

					int icon = R.mipmap.ic_launcher;
					long when = System.currentTimeMillis();


					String title = getApplicationContext().getString(
							R.string.app_name);

				/*	NotificationManager notificationManager = (NotificationManager) getApplicationContext()
							.getSystemService(Context.NOTIFICATION_SERVICE);

					Intent notificationIntent = new Intent(
							getApplicationContext(), HomeActivity.class);
					// set intent so it does not start a new activity
					notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP);
					PendingIntent intent2 = PendingIntent.getActivity(
							getApplicationContext(), 0, notificationIntent, 0);
					Notification notification= NotificationUtil.createNotification(getApplicationContext(), intent2, getApplicationContext().getResources().getString(R.string.app_name), data, icon);
					notification.flags |= Notification.FLAG_AUTO_CANCEL;
					notification.defaults |= Notification.DEFAULT_VIBRATE;
					notificationManager.notify(new Random().nextInt(),
							notification);
*/
					Intent notificationIntent = new Intent(
							getApplicationContext(), HomeActivity.class);
					notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP);

					PendingIntent pendingIntent = PendingIntent.getActivity(
							getApplicationContext(), 0, notificationIntent, 0);

					NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
					builder.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle(title)
							.setContentText(data)
							.setPriority(NotificationCompat.PRIORITY_HIGH)
							.setContentIntent(pendingIntent)
							.setAutoCancel(true);

					NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
					notificationManager.notify(new Random().nextInt(), builder.build());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
