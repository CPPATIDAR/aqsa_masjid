package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.aqsamasjid.adapter.QuestionAnswerAdapter;
import com.aqsamasjid.pojo.QuesAnsPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.ShowDialog;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class LatestQAFragment extends Fragment {
	
	private ListView listView;
	private TextView tvAskQuestion;
	private EditText etSearch;
	private NetworkConnection networkConnection;
	private ShowDialog showMessage;
	private ArrayList<QuesAnsPojo> alQueAnsList;
	private QuestionAnswerAdapter adapter;
	private SearchTranslationFilter filter;
	
	@SuppressLint("NewApi") @Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_latest_qa, container,false);
		networkConnection = new NetworkConnection();
		showMessage = new ShowDialog();
		listView = (ListView) v.findViewById(R.id.listview_latest_qa);
		tvAskQuestion = (TextView) v.findViewById(R.id.tv_ask_question);
		etSearch = (EditText) v.findViewById(R.id.et_search);
		
		if (SPMasjid.getValue(getActivity(), SPMasjid.application_btn_BackColor).length()!=0&&(!SPMasjid.getValue(getActivity(), SPMasjid.application_btn_BackColor).equalsIgnoreCase(null)))
		{
			tvAskQuestion.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getActivity(), SPMasjid.application_btn_BackColor)));
		}
		if (SPMasjid.getValue(getActivity(), SPMasjid.application_btn_alpha).length()!=0&&(!SPMasjid.getValue(getActivity(), SPMasjid.application_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getActivity(), SPMasjid.application_btn_alpha))>0.0f)
		{
			tvAskQuestion.setAlpha(Float.parseFloat(SPMasjid.getValue(getActivity(), SPMasjid.application_btn_alpha)));
		}
		if (SPMasjid.getValue(getActivity(), SPMasjid.application_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(getActivity(), SPMasjid.application_btn_ForeColor).equalsIgnoreCase(null)))
		{
			tvAskQuestion.setTextColor(Color.parseColor(SPMasjid.getValue(getActivity(), SPMasjid.application_btn_ForeColor)));
		}
		
		tvAskQuestion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), AskQuestionActivity.class);
				startActivityForResult(intent,0);
				//startActivity(intent);
				//getActivity().finish();
				/*getActivity().setResult(getActivity().RESULT_OK,new Intent(getActivity(),AskQuestionActivity.class));*/
			}
		});
		
		etSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (s.equals("") || s.length()==0) {
					listView.setAdapter(new QuestionAnswerAdapter(getActivity(), alQueAnsList));
				} else {
					getFilter().filter(s.toString());
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}

		});
		
		if (networkConnection.isOnline(getActivity())) {
			new QuestionAnswerAsyncTask().execute();
		} else {
		Toast.makeText(getActivity(),
				"Please check your network connection.", Toast.LENGTH_SHORT).show();
		}
		
		return v;
	}
	
	public Filter getFilter() {
		if (filter == null) {
			filter = new SearchTranslationFilter();
		}
		return filter;
	}
	
	private class SearchTranslationFilter extends Filter {
		private ArrayList<QuesAnsPojo> arrayListTemp;
		private ArrayList<QuesAnsPojo> search;

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			constraint = constraint.toString().toLowerCase();
			FilterResults result = new FilterResults();
			if (constraint != null && constraint.toString().length() > 0&&(!constraint.equals(""))) {

				arrayListTemp = new ArrayList<QuesAnsPojo>();
				
				for (int i = 0, l = alQueAnsList.size(); i < l; i++) {
						if (alQueAnsList.get(i).getQuestion().toString().toLowerCase().contains(constraint))

							arrayListTemp.add(alQueAnsList.get(i));
					

				}

				result.count = arrayListTemp.size();
				result.values = arrayListTemp;

			} else {
				synchronized (this) {
					result.values = arrayListTemp;
					result.count = arrayListTemp.size();
				}
			}
			return result;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			try {
				search = (ArrayList<QuesAnsPojo>) results.values;
				Log.v("results.count", "=" + results.count);
				
				listView.setAdapter(new QuestionAnswerAdapter(getActivity(), search));
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
	
	
	
	
	class QuestionAnswerAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		private QuesAnsPojo pojo;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("category_id",""));
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID + ""));
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter Registration", urlParameter+"");
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.GetAllQuestionAnswer, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						alQueAnsList = new ArrayList<QuesAnsPojo>();
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);
							pojo = new QuesAnsPojo();
							pojo.setId(jsObject.optLong("id"));
							pojo.setQuestion(jsObject.optString("question"));
							pojo.setAnswer(jsObject.optString("answer"));
							pojo.setEmail(jsObject.optString("email"));
							pojo.setCategoryId(jsObject.optInt("category_id"));
							alQueAnsList.add(pojo);
						}
						
						adapter = new QuestionAnswerAdapter(getActivity(), alQueAnsList);
						listView.setAdapter(adapter);
					}else {
						showMessage.showMsg(getActivity(), jsonObject.optString("message"));
					}
					
				} else {
					Toast.makeText(getActivity(), "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == getActivity().RESULT_OK && requestCode ==0){
			getActivity().finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	
}
