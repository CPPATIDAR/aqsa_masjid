package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.aqsamasjid.adapter.CategoryAdapter;
import com.aqsamasjid.pojo.CategoryPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.ShowDialog;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoriesFragment extends Fragment {
	
	private ListView listView;
	private TextView tvAskQuestion;
	private NetworkConnection networkConnection;
	private ShowDialog showMessage;
	private ArrayList<CategoryPojo> alCategoryList;
	private CategoryAdapter adapter;
	private Fragment fragment;
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_categories_list, container,false);
		networkConnection = new NetworkConnection();
		showMessage = new ShowDialog();
		listView = (ListView) v.findViewById(R.id.listview_categories);
		tvAskQuestion = (TextView) v.findViewById(R.id.tv_ask_question_cat);
		
		if (SPMasjid.getValue(getActivity(), SPMasjid.application_btn_BackColor).length()!=0&&(!SPMasjid.getValue(getActivity(), SPMasjid.application_btn_BackColor).equalsIgnoreCase(null)))
		{
			tvAskQuestion.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getActivity(), SPMasjid.application_btn_BackColor)));
		}
		if (SPMasjid.getValue(getActivity(), SPMasjid.application_btn_alpha).length()!=0&&(!SPMasjid.getValue(getActivity(), SPMasjid.application_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getActivity(), SPMasjid.application_btn_alpha))>0.0f)
		{
			tvAskQuestion.setAlpha(Float.parseFloat(SPMasjid.getValue(getActivity(), SPMasjid.application_btn_alpha)));
		}
		if (SPMasjid.getValue(getActivity(), SPMasjid.application_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(getActivity(), SPMasjid.application_btn_ForeColor).equalsIgnoreCase(null)))
		{
			tvAskQuestion.setTextColor(Color.parseColor(SPMasjid.getValue(getActivity(), SPMasjid.application_btn_ForeColor)));
		}
		
		tvAskQuestion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), AskQuestionActivity.class);
				//startActivityForResult(intent,0);
				startActivity(intent);
			}
		});
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				fragment = new ParticularCategoryDetailFragment();
				Bundle bundle = new Bundle();
				bundle.putLong("CATEGORY_ID", alCategoryList.get(position).getCategiryId());
				fragment.setArguments(bundle);
				FragmentManager fm = getActivity().getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fm.beginTransaction();
				fragmentTransaction.replace(R.id.fragment_ask_imam, fragment);
				fragmentTransaction.commit();
			}
		});
		
		if (networkConnection.isOnline(getActivity())) {
			new CategoryAsyncTask().execute();
		} else {
		Toast.makeText(getActivity(),
				"Please check your network connection.", Toast.LENGTH_SHORT).show();
		}
		
		return v;
	}
	
	class CategoryAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;
		private JSONObject jsObject;
		private CategoryPojo pojo;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID + ""));
			response = CustomHttpClient.executeHttpPost(WebUrl.GetCategory,urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						alCategoryList = new ArrayList<CategoryPojo>();
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);
							pojo = new CategoryPojo();
							pojo.setCategiryId(jsObject.optLong("id"));
							pojo.setCategoryName(jsObject.optString("title"));
							pojo.setCategoryCount(jsObject.optInt("number_of_question"));
							
							alCategoryList.add(pojo);
						}
						adapter = new CategoryAdapter(getActivity(), alCategoryList);
						listView.setAdapter(adapter);
					}else {
						showMessage.showMsg(getActivity(), jsonObject.optString("message"));
					}
					
				} else {
					Toast.makeText(getActivity(), "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	/*@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == getActivity().RESULT_OK && requestCode ==0){
			getActivity().finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}*/

}
