package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.aqsamasjid.imageloader.MasjidImageLoader;
import com.aqsamasjid.pojo.AnnouncementPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.MySqlLiteHelper;
import com.aqsamasjid.utill.NamazReminder;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.SalatDataBase;
import com.aqsamasjid.utill.WebUrl;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static com.aqsamasjid.AqsaMasjid.CHANNEL_ID;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("SimpleDateFormat")
public class HomeActivity extends BaseActivity {

	private TextView tvJuma;
	private TextView tvJumaText;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivBanner;
	private SalatDataBase salatDataBase;
	private String currentDate;
	private NetworkConnection networkConnection;
	private TextView tvKhutbah;
	private TextView tvKhutbahText;
	private String fridayDate;
	private TextView tvJumaDate;
	private String friFullDate;
	private LinearLayout llJuma;
	private ImageView ivHome;
	private ViewPager viewPager;
	private MyPageAdapter pageAdapter;
	private RelativeLayout rlRamdanPanel;
	private TextView tvRamdantTimer;
	private long ramdanTime = 0;
	private String ramdanDate;
	private MasjidImageLoader imageLoader;
	private List<MyFragment> fragments;
	private int ramdanCountdownFlag;
	private TextView tvRamdanCountDownText;
	Handler customHandler = new Handler();
	private Runnable updateTimerThread;
	private boolean isRun = true;
	private View viewlayout;
	private String resHeight;
	private String resWidth;

	private ImageView ivCountHeaderText;
	//private TextView tvCountHeaderText;
	//private LinearLayout llramdanSchedule;
	//private LinearLayout llYoutubePanel;
//	private LinearLayout llMapLocation;
	private Typeface typeface_bold;
	private TextView tvJumaPlace;
	private TextView tvAppLabel;
	private int versionCode;
	private RelativeLayout rlBannerParent;
//	private TextView tvLocation;
//	private TextView tvYoutube;
	private LinearLayout rlDirectionSchedule;
	private TextView tvSalatText;
	public JSONArray masjidJsonArray;
	private ViewFlipper mViewFlipper;
	private ImageView imYoutube;
	private ImageView imMapLocation;
	private TextView tvMarquee;
    private int pageCount = 0;
	private ViewFlipper viewFlipper;
	private Animation.AnimationListener mAnimationListener;
    private ArrayList<AnnouncementPojo> alAnnouncement;
	private AnnouncementPojo pojo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tmp_main);
	
		home = HomeActivity.this;
		FragmentManager fm = getSupportFragmentManager();
		List<Fragment> a = fm.getFragments();
		if (a != null) {
			FragmentTransaction t = fm.beginTransaction();
			for (Fragment fragment : a) {
				t.remove(fragment);
			}
			t.commit();
		}
		initComponents();
		initListiners();
		initSideBar();
		setFonts();
		setSalatTime(Calendar.getInstance());
		setViewPager();
		setRamdanTimer();
		setBanerImage();
        new AnnouncementAsyncTask().execute();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		try {

			Bundle extras = intent.getExtras();
			if (extras!=null) {

				String data = extras.getString("Message");
				System.out.println("Data===="+data);

				String title = getApplicationContext().getString(R.string.app_name);
				Intent notificationIntent = new Intent(
						getApplicationContext(), SplashActivity.class);
				notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_SINGLE_TOP);

				PendingIntent pendingIntent = PendingIntent.getActivity(
						getApplicationContext(), 0, notificationIntent, 0);

				NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
				builder.setSmallIcon(R.drawable.ic_launcher)
						.setContentTitle(title)
						.setContentText(data)
						.setPriority(NotificationCompat.PRIORITY_HIGH)
						.setContentIntent(pendingIntent)
						.setAutoCancel(true);

				NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
				notificationManager.notify(new Random().nextInt(), builder.build());
				//ResetAlarms.reset(HomeActivity.this);

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	class AnnouncementAsyncTask extends AsyncTask<Void, Void, Void> {
	private String response;
	private ArrayList<NameValuePair> urlParameter;
	private JSONObject jsObject;
	private ProgressDialog progressDialog;

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		urlParameter = new ArrayList<NameValuePair>();
		urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
		urlParameter.add(new BasicNameValuePair("page_number", pageCount+ ""));
		Log.v("urlParameter", urlParameter + "");
		progressDialog = new ProgressDialog(HomeActivity.this);
		progressDialog.setMessage("Loading...");
		progressDialog.show();
	}

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		response = CustomHttpClient.executeHttpPost(WebUrl.ANNOUNCEMENT,
				urlParameter);
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {

		super.onPostExecute(result);
		progressDialog.dismiss();
		try {
			if (response != null) {
				Log.v("Response", response);
				JSONObject jsonObject = new JSONObject(response);
				LayoutInflater inflater = HomeActivity.this
						.getLayoutInflater();
				if (jsonObject.optBoolean("status")) {
					JSONArray jsonArray = jsonObject.optJSONArray("result");
                    alAnnouncement = new ArrayList<AnnouncementPojo>();
					for (int i = 0; i < jsonArray.length(); i++) {
						jsObject = jsonArray.optJSONObject(i);
						RelativeLayout rlTmp = (RelativeLayout) inflater
								.inflate(R.layout.flipper_change, null);
						final TextView tvMarquee = (TextView) rlTmp
								.findViewById(R.id.tv_marquee);
						tvMarquee.setText(jsObject.optString("title"));
						pojo = new AnnouncementPojo();
						pojo.setId(jsObject.optInt("id"));
						pojo.setTitle(jsObject.optString("title"));
						pojo.setDetail(jsObject.optString("details"));
						pojo.setDetailNoTag(jsObject.optString("detailnotag"));
						pojo.setLogoImage(jsObject.optString("logo_image"));
						pojo.setBannerImage(jsObject.optString("poster_image"));

						alAnnouncement.add(pojo);
						tvMarquee.setTag(pojo);
						tvMarquee.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								AnnouncementPojo announcementPojo = (AnnouncementPojo) v.getTag();
								Intent intent=new Intent(HomeActivity.this, AnnouncementDetailActivity.class);
								intent.putExtra("Title",announcementPojo.getTitle());
								intent.putExtra("Detail", announcementPojo.getDetail());
								intent.putExtra("Banner_Image", announcementPojo.getBannerImage());
								startActivity(intent);
								// Pass this newsId to next activity via intent putExtra.

							}
						});

						viewFlipper.addView(rlTmp);




					}
					viewFlipper.setInAnimation(AnimationUtils.loadAnimation(
						HomeActivity.this, R.anim.left_in));
				viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(
							HomeActivity.this, R.anim.left_out));
					// // controlling animation
					viewFlipper.getInAnimation().setAnimationListener(
							mAnimationListener);

					viewFlipper.setAutoStart(true);
					viewFlipper.setFlipInterval(10000);
					viewFlipper.startFlipping();
					pageCount++;
				}
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}
}

	private void setBanerImage() {

		try {
			// Add all the images to the ViewFlipper
			JSONArray jsonArray2=new JSONArray(SPMasjid.getValue(getApplicationContext(), SPMasjid.BANNER_IMAGE_ARRAYLIST));
	      	for (int i = 0; i < jsonArray2.length(); i++) {

				JSONObject jobject=jsonArray2.optJSONObject(i);
	            ImageView ivBanner = new ImageView(HomeActivity.this);
	            ivBanner.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	            ivBanner.setScaleType(ScaleType.FIT_XY);
	            String imageUrl = jobject.optString("banner_image");
	            ivBanner.setBackgroundColor(Color.parseColor("#ffffff"));
	            ivBanner.setTag(jobject);
	         /*   String[] strfile = imageUrl.split("[/]");
				String fName = strfile[strfile.length - 1];
					File image = new File(Environment.getExternalStorageDirectory()
						.getAbsolutePath() + "/Aqsa_Masjid/Image/" + fName);

				if (image.exists()) {
					BitmapFactory.Options o = new BitmapFactory.Options();
					o.inJustDecodeBounds = true;
					BitmapFactory.decodeStream(new FileInputStream(image), null, o);
					// decode with inSampleSize
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = 2;
					ivBanner.setImageBitmap(BitmapFactory.decodeStream(
							new FileInputStream(image), null, o2));
				} else {
					imageLoader.DisplayImage(imageUrl, ivBanner, fName,
							R.drawable.banner);
				}*/

				Glide.with(this)
						.load(imageUrl)
						.centerCrop()
						.placeholder(R.drawable.banner)
						.into(ivBanner);


				ivBanner.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						JSONObject tempObject=(JSONObject)mViewFlipper.getCurrentView().getTag();
						 int linkType = tempObject.optInt("link_type");
			              String link = tempObject.optString("link");

						//if link_type==1 means internal link tobe open
						if (linkType==1) {
							openinternalScreen(link);
						} else if (linkType==2) {
							try {
								Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
								startActivity(newIntent);

							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}

					}
				});

	            mViewFlipper.addView(ivBanner);
	        }

	        // Set in/out flipping animations
	        mViewFlipper.setInAnimation(HomeActivity.this, R.anim.slide_in_from_right);
	        mViewFlipper.setOutAnimation(HomeActivity.this, R.anim.slide_out_to_left);

	       if (jsonArray2.length()>1) {

	    	   mViewFlipper.setAutoStart(true);
		       mViewFlipper.setFlipInterval(10000);
		       mViewFlipper.startFlipping();


		}
	      	} catch (Exception ex) {
			System.out.println("Error :  Image URL " + ex);
			ex.printStackTrace();
		}
}
	protected void openinternalScreen(String link) {
		if (link.equalsIgnoreCase("events")) {
			Intent intent = new Intent(HomeActivity.this,
					EventActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("ramadan")) {
			Intent intent = new Intent(HomeActivity.this,
					RamdanScheduleActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("services")) {
			Intent intent = new Intent(HomeActivity.this,
					ServiceActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("about_us")) {
			Intent intent = new Intent(HomeActivity.this,
					AboutUsActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("contact_us")) {
			Intent intent = new Intent(HomeActivity.this,
					ContactUsListActivity.class);
			startActivity(intent);
		}


	}


	private void setRamdanTimer() {
		// TODO Auto-generated method stub
		ramdanDate = SPMasjid.getValue(HomeActivity.this, SPMasjid.RAMDAN_DATE_ORIGINAL);
		ramdanTime = getRamdanTimeInMilis(ramdanDate);

		ramdanCountdownFlag = SPMasjid.getIntValue(HomeActivity.this,
				SPMasjid.RAMDAN_COUNTDOWN_FLAG);

		if (ramdanCountdownFlag == 1) {
			rlRamdanPanel.setVisibility(View.VISIBLE);
			tvRamdanCountDownText.setText(SPMasjid.getValue(HomeActivity.this,
					SPMasjid.RAMDAN_LABEL));
			new RamdanAsyncTask().execute();
			// Log.v("yes true ramdanCountdownFlag",ramdanCountdownFlag + "");
		} else {
			rlRamdanPanel.setVisibility(View.GONE);
			// Log.v("false ramdanCountdownFlag",ramdanCountdownFlag + "");
		}

		/*if (SPMasjid.getValue(HomeActivity.this, SPMasjid.URL_Youtube_Stream)
				.equalsIgnoreCase("")
				|| SPMasjid.getValue(HomeActivity.this,
						SPMasjid.URL_Youtube_Stream).length() == 0) {
			llYoutubePanel.setVisibility(View.GONE);
		} else {
			llYoutubePanel.setVisibility(View.VISIBLE);
		}*/
	}

	public void setFirstIndexViewPager() {
		viewPager.setCurrentItem(0);
	}

	private void setViewPager() {
		// TODO Auto-generated method stub
		fragments = getFragments();
		pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
		viewPager.setAdapter(pageAdapter);
	}

	private void setSalatTime(Calendar c) {

		SimpleDateFormat dateformat;

		if (c.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
			dateformat = new SimpleDateFormat("yyyy-MM-dd");
			fridayDate = dateformat.format(c.getTime());
			dateformat = new SimpleDateFormat("dd-MMM-yyyy");
			friFullDate = dateformat.format(c.getTime());
			System.out.println("test f=" + fridayDate);
		} else {

			switch (c.get(Calendar.DAY_OF_WEEK)) {
			case 1:

				c.add(Calendar.DATE, 5);
				dateformat = new SimpleDateFormat("yyyy-MM-dd");
				fridayDate = dateformat.format(c.getTime());
				dateformat = new SimpleDateFormat("dd-MMM-yyyy");
				friFullDate = dateformat.format(c.getTime());
				break;
			case 2:

				c.add(Calendar.DATE, 4);
				dateformat = new SimpleDateFormat("yyyy-MM-dd");
				fridayDate = dateformat.format(c.getTime());
				dateformat = new SimpleDateFormat("dd-MMM-yyyy");
				friFullDate = dateformat.format(c.getTime());
				break;
			case 3:

				c.add(Calendar.DATE, 3);
				dateformat = new SimpleDateFormat("yyyy-MM-dd");
				fridayDate = dateformat.format(c.getTime());
				dateformat = new SimpleDateFormat("dd-MMM-yyyy");
				friFullDate = dateformat.format(c.getTime());
				break;
			case 4:

				c.add(Calendar.DATE, 2);
				dateformat = new SimpleDateFormat("yyyy-MM-dd");
				fridayDate = dateformat.format(c.getTime());
				dateformat = new SimpleDateFormat("dd-MMM-yyyy");
				friFullDate = dateformat.format(c.getTime());
				break;
			case 5:

				c.add(Calendar.DATE, 1);
				dateformat = new SimpleDateFormat("yyyy-MM-dd");
				fridayDate = dateformat.format(c.getTime());
				dateformat = new SimpleDateFormat("dd-MMM-yyyy");
				friFullDate = dateformat.format(c.getTime());
				break;
			case 7:
				c.add(Calendar.DATE, -1);
				dateformat = new SimpleDateFormat("yyyy-MM-dd");
				fridayDate = dateformat.format(c.getTime());
				dateformat = new SimpleDateFormat("dd-MMM-yyyy");
				friFullDate = dateformat.format(c.getTime());
				break;

			default:
				break;
			}
		}

		Cursor cur = salatDataBase.getFridayTime(fridayDate);
		// if (cur.moveToNext()) {
		if (cur.moveToNext()) {
			if (cur.getString(6).length() != 0) {
				tvJumaPlace.setVisibility(View.VISIBLE);
				tvJumaPlace.setText(cur.getString(6));
			}
			tvKhutbah.setText(cur.getString(2));
			tvJuma.setText(cur.getString(3));
		}
		cur.close();
		tvJumaDate.setText("Fri," + friFullDate);
	}

	private void setFonts() {
		// TODO Auto-generated method stub

		typeface_bold = Typeface.createFromAsset(HomeActivity.this.getAssets(),
				"arialbd.ttf");
		tvJuma.setTypeface(typeface_bold);
		tvJumaText.setTypeface(typeface_bold);
		tvKhutbah.setTypeface(typeface_bold);
		tvKhutbahText.setTypeface(typeface_bold);
	}

	private void initComponents() {
		// TODO Auto-generated method stub

		/*String refreshedToken = FirebaseInstanceId.getInstance().getToken();
		Log.d("FirebaseIDService", "Refreshed token: " + refreshedToken);
		if(((refreshedToken!=null)&&!refreshedToken.equalsIgnoreCase(""))){
			SPMasjid.setValue(HomeActivity.this, SPMasjid.FCM_ANDROID_TOKEN,refreshedToken+"");
		}
*/

		FirebaseInstanceId.getInstance().getInstanceId()
				.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
					@Override
					public void onComplete(@NonNull Task<InstanceIdResult> task) {
						if (!task.isSuccessful()) {
							Log.w("getInstanceId failed", task.getException());
							return;
						}

						// Get new Instance ID token
						String token = task.getResult().getToken();

						SPMasjid.setValue(HomeActivity.this, SPMasjid.FCM_ANDROID_TOKEN,token+"");
						Log.w("token=",token);
					}
				});

		networkConnection = new NetworkConnection();
		imageLoader = new MasjidImageLoader(HomeActivity.this);
		ivHome = (ImageView) findViewById(R.id.iv_home);
		tvJuma = (TextView) findViewById(R.id.tv_juma);
		tvJumaText = (TextView) findViewById(R.id.tv_juma_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		//ivBanner = (ImageView) findViewById(R.id.iv_banner);
		tvKhutbah = (TextView) findViewById(R.id.tv_khutbah);
		tvKhutbahText = (TextView) findViewById(R.id.tv_khutbah_text);
		tvJumaDate = (TextView) findViewById(R.id.tv_juma_date);
		llJuma = (LinearLayout) findViewById(R.id.ll_juma);
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		viewlayout = (View) findViewById(R.id.layout_view_pager);
		ivCountHeaderText = (ImageView) findViewById(R.id.iv_countdown_header_img);
		//tvCountHeaderText = (TextView) findViewById(R.id.tv_countdown_header_text);
		//llramdanSchedule = (LinearLayout) findViewById(R.id.ll_ramdan_schedule);
		//llYoutubePanel = (LinearLayout) findViewById(R.id.ll_youtube_panel);
	//	llMapLocation = (LinearLayout) findViewById(R.id.ll_map_location);
		tvJumaPlace = (TextView) findViewById(R.id.tv_juma_place);
		rlRamdanPanel = (RelativeLayout) findViewById(R.id.rl_ramdan_panel);
		tvRamdantTimer = (TextView) findViewById(R.id.tv_ramdan_countdown_timer);
		tvRamdanCountDownText = (TextView) findViewById(R.id.tv_ramdan_countdown_text);
		tvAppLabel = (TextView) findViewById(R.id.tv_masjid_label);
		imYoutube = (ImageView) findViewById(R.id.im_youtube_header);
		imMapLocation = (ImageView) findViewById(R.id.im_map_location);

		rlBannerParent = (RelativeLayout) findViewById(R.id.rl_banner_parent);
		//rlDirectionSchedule = (LinearLayout) findViewById(R.id.rl_directio_schedule);
		//tvLocation = (TextView) findViewById(R.id.tv_location);
		//tvYoutube = (TextView) findViewById(R.id.tv_youtube);
		rlBannerParent = (RelativeLayout) findViewById(R.id.rl_banner_parent);
		tvSalatText = (TextView) findViewById(R.id.tv_salat_text);
		mViewFlipper= (ViewFlipper) findViewById(R.id.vf_banner);
		tvJuma = (TextView) findViewById(R.id.tv_juma);
		viewFlipper = (ViewFlipper) findViewById(R.id.view_flipper);
		mAnimationListener = new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {

			}
		};
		tvKhutbahText.setText(SPMasjid.getValue(HomeActivity.this, SPMasjid.KHUTBA_LABEL));
			tvSalatText.setText(SPMasjid.getValue(HomeActivity.this, SPMasjid.SALAT_LABEL));
			if (SPMasjid.getValue(HomeActivity.this, SPMasjid.KHUTBA_LABEL).length()==0) {
				tvKhutbah.setText("");
			}
			if (SPMasjid.getValue(HomeActivity.this, SPMasjid.SALAT_LABEL).length()==0) {
				tvJuma.setText("");
			}
		try {
			versionCode = getPackageManager().getPackageInfo(getPackageName(),
					0).versionCode;
			// Log.v("version code", versionCode+"");
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// salatDataBase = new SalatDataBase(getApplicationContext());
		if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.DATABASE_VERSION) != 2) {
			MySqlLiteHelper helper = new MySqlLiteHelper(
					getApplicationContext());
			helper.deleteTable();
			salatDataBase = new SalatDataBase(getApplicationContext());
			helper.alterTable();
		} else {
			salatDataBase = new SalatDataBase(getApplicationContext());
		}


		DisplayMetrics displayMatrix = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMatrix);
		resHeight = displayMatrix.heightPixels + "";
		resWidth = displayMatrix.widthPixels + "";

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		currentDate = dateformat.format(cal.getTime());

		int cursorCount = salatDataBase.getSalatTimeSwap(currentDate)
				.getCount();

		if (cursorCount == 0) {

			viewlayout.setVisibility(View.VISIBLE);
			viewPager.setVisibility(View.GONE);

			if (networkConnection.isOnline(getApplicationContext())) {
				new GetMasjidAsyncTask().execute();
				new NamazTimeAsyncTask().execute();
				new RamdanScheduleAsynsTask().execute();

			} else {

				updateTimerThread = new Runnable() {

					public void run() {
						if (isRun) {
							customHandler.postDelayed(this,
									WebUrl.TimerTenMinute);
							try {
								ShowSetting();
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (networkConnection
									.isOnline(getApplicationContext())) {
								isRun = false;
							}
						}

					}
				};
				customHandler.postDelayed(updateTimerThread,
						WebUrl.TimerTenMinute);
			}

		} else {

			viewlayout.setVisibility(View.GONE);
			viewPager.setVisibility(View.VISIBLE);

		}

		if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.IS_RAMADAN) == 1) {
			ivCountHeaderText.setImageResource(R.drawable.ramdan);
		//	tvCountHeaderText.setText(R.string.ramdan_schedule);
		} else {
			ivCountHeaderText.setImageResource(R.drawable.update);
		//	tvCountHeaderText.setText(R.string.daily_dua);
		}
		tvAppLabel.setText(SPMasjid.getValue(HomeActivity.this,
				SPMasjid.Masjid_Name));


	/*	if (SPMasjid.getValue(HomeActivity.this, SPMasjid.home_header_navigation_BackColor).length() != 0&& (!SPMasjid.getValue(HomeActivity.this, SPMasjid.home_header_navigation_BackColor).equalsIgnoreCase(null))) {
			rlDirectionSchedule.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackColor)));
		}

		if (SPMasjid.getValue(HomeActivity.this, SPMasjid.home_header_navigation_BackAlpha).length() != 0&& (!SPMasjid.getValue(HomeActivity.this, SPMasjid.home_header_navigation_BackAlpha).equalsIgnoreCase(null))&& Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackAlpha)) > 0.0f) {
			rlDirectionSchedule.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_BackAlpha)));
		}*/

		if (SPMasjid.getValue(HomeActivity.this, SPMasjid.home_header_navigation_ForeColor).length() != 0&& (!SPMasjid.getValue(HomeActivity.this, SPMasjid.home_header_navigation_ForeColor).equalsIgnoreCase(null))) {
		//	tvLocation.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_ForeColor)));
		//	tvYoutube.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_ForeColor)));
		//	tvCountHeaderText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_header_navigation_ForeColor)));
		}

		if (SPMasjid.getValue(HomeActivity.this,
				SPMasjid.home_jumuah_prayer_BackColor)
				.length() != 0
				&& (!SPMasjid
						.getValue(
								HomeActivity.this,
								SPMasjid.home_jumuah_prayer_BackColor)
						.equalsIgnoreCase(null))) {

			llJuma.setBackgroundColor(Color.parseColor(SPMasjid
					.getValue(
							getApplicationContext(),
							SPMasjid.home_jumuah_prayer_BackColor)));

		}

		if (SPMasjid.getValue(HomeActivity.this,
				SPMasjid.home_jumuah_prayer_BackAlpha)
				.length() != 0
				&& (!SPMasjid
						.getValue(
								HomeActivity.this,
								SPMasjid.home_jumuah_prayer_BackAlpha)
						.equalsIgnoreCase(null))
				&& Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.home_jumuah_prayer_BackAlpha)) > 0.0f) {
			llJuma.setAlpha(Float.parseFloat(SPMasjid
					.getValue(
							getApplicationContext(),
							SPMasjid.home_jumuah_prayer_BackAlpha)));
		}

		if (SPMasjid.getValue(HomeActivity.this,
				SPMasjid.home_jumuah_prayer_ForeColor)
				.length() != 0
				&& (!SPMasjid
						.getValue(
								HomeActivity.this,
								SPMasjid.home_jumuah_prayer_ForeColor)
						.equalsIgnoreCase(null))) {
			tvJumaText
					.setTextColor(Color.parseColor(SPMasjid
							.getValue(
									getApplicationContext(),
									SPMasjid.home_jumuah_prayer_ForeColor)));
			tvJumaPlace
					.setTextColor(Color.parseColor(SPMasjid
							.getValue(
									getApplicationContext(),
									SPMasjid.home_jumuah_prayer_ForeColor)));
			tvJumaDate
					.setTextColor(Color.parseColor(SPMasjid
							.getValue(
									getApplicationContext(),
									SPMasjid.home_jumuah_prayer_ForeColor)));

			tvKhutbahText
					.setTextColor(Color.parseColor(SPMasjid
							.getValue(
									getApplicationContext(),
									SPMasjid.home_jumuah_prayer_ForeColor)));
			tvKhutbah
					.setTextColor(Color.parseColor(SPMasjid
							.getValue(
									getApplicationContext(),
									SPMasjid.home_jumuah_prayer_ForeColor)));
			tvSalatText
					.setTextColor(Color.parseColor(SPMasjid
							.getValue(
									getApplicationContext(),
									SPMasjid.home_jumuah_prayer_ForeColor)));
			tvJuma.setTextColor(Color.parseColor(SPMasjid
					.getValue(
							getApplicationContext(),
							SPMasjid.home_jumuah_prayer_ForeColor)));

		}

	}

	@Override
	protected void onResume() {
		if (networkConnection.isOnline(getApplicationContext())) {
			new GetHeaderInfoAsyncTask().execute();
			new CheckStatusAsyncTask().execute();

			if (!SPMasjid.getBooleanValue(HomeActivity.this, SPMasjid.CallUserSetting)&&(SPMasjid.getValue(HomeActivity.this, SPMasjid.FCM_ANDROID_TOKEN).length()>0)&&((SPMasjid.getValue(HomeActivity.this, SPMasjid.TOKEN_CHECK).equals("")))){
			new NotificationAsyncTask().execute();
		}
		}
		super.onResume();
	}

	class NotificationAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("device_id", SPMasjid.getValue(HomeActivity.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("client_type", "android"));
			urlParameter.add(new BasicNameValuePair("device_ios_notification",""));
			urlParameter.add(new BasicNameValuePair("android_gcm_token",
					SPMasjid.getValue(HomeActivity.this, SPMasjid.GCM_Installation_ID)));
			urlParameter.add(new BasicNameValuePair("android_fcm_token", SPMasjid.getValue(HomeActivity.this, SPMasjid.FCM_ANDROID_TOKEN)));
			urlParameter.add(new BasicNameValuePair(
					"device_android_notification", SPMasjid.getValue(HomeActivity.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("all_notification","1"));
			urlParameter.add(new BasicNameValuePair("prayer_notification","1"));
			urlParameter.add(new BasicNameValuePair("event_notification","1"));
			urlParameter.add(new BasicNameValuePair("broadcast_notification","1"));
			urlParameter.add(new BasicNameValuePair("annoucment_notification","1"));
			Log.v("urlParameter", urlParameter + "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.UserSetting,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				if (response != null) {
					Log.v("Response", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						if (SPMasjid.getValue(HomeActivity.this, SPMasjid.FCM_ANDROID_TOKEN).length()>0)
						{
							SPMasjid.setBooleanValue(HomeActivity.this, SPMasjid.CallUserSetting, true);
							SPMasjid.setValue(HomeActivity.this, SPMasjid.TOKEN_CHECK,"tokencheck");
						}
					}
				} else {
					Toast.makeText(HomeActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}


	private List<MyFragment> getFragments() {
		List<MyFragment> fList = new ArrayList<MyFragment>();
		Cursor cur = salatDataBase.getSalatTimeSwap(currentDate);

		boolean isToday = true;

		while (cur.moveToNext()) {
			fList.add(MyFragment.newInstance(cur.getString(4),
					cur.getString(5), cur.getString(6), cur.getString(7),
					cur.getString(8), cur.getString(9), cur.getString(10),
					isToday, cur.getString(11), cur.getString(12),
					cur.getString(13), cur.getString(14), cur.getString(15),
					cur.getString(16), cur.getString(17), cur.getString(18),
					cur.getString(19), cur.getString(20)));
			isToday = false;
		}
		cur.close();
		return fList;
	}

	private class MyPageAdapter extends FragmentPagerAdapter {
		private List<MyFragment> fragments;

		public MyPageAdapter(FragmentManager fm, List<MyFragment> fragments) {
			super(fm);
			this.fragments = fragments;
		}

		@Override
		public Fragment getItem(int position) {
			return this.fragments.get(position);
		}

		@Override
		public int getCount() {

			return this.fragments.size();
		}
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});

		rlRamdanPanel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this,
						RamdanScheduleActivity.class);
				startActivity(intent);
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this,
						EventActivity.class);
				startActivity(intent);
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this,
						ServiceActivity.class);
				startActivity(intent);
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(HomeActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(HomeActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
			}
		});

		llJuma.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this,
						FridayActivity.class);
				intent.putExtra("FRIDAY_DATE", fridayDate);
				intent.putExtra("FriConvertDate", friFullDate);
				startActivity(intent);
			}
		});

		imMapLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
		try {

		if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.MASJID_ARRAY_COUNT)>1) {
					Intent intent = new Intent(HomeActivity.this, MasjidList.class).putExtra("masjidJsonArray", SPMasjid.getValue(HomeActivity.this, SPMasjid.MASJID_ARRAY)+"");
					startActivity(intent);
				}else {
					Intent intent = new Intent(HomeActivity.this,
							ProfileAndMapActivity.class);
					startActivity(intent);
				}
				} catch (Exception e) { 
					// TODO: handle exception
				}
				
			}
		});

		ivCountHeaderText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (SPMasjid
						.getIntValue(HomeActivity.this, SPMasjid.IS_RAMADAN) == 1) {
					Intent intent = new Intent(HomeActivity.this,
							RamdanScheduleActivity.class);
					startActivity(intent);
				} else {
					Intent intent = new Intent(HomeActivity.this,
							MasjidUpdateActivity.class);
					startActivity(intent);
				}
			}
		});

		imYoutube.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse(SPMasjid.getValue(HomeActivity.this,
								SPMasjid.URL_Youtube_Stream)));
				startActivity(newIntent);
			}
		});
	}

	class NamazTimeAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("date", currentDate));
			progressDialog = new ProgressDialog(HomeActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.PrayerTimeUrl,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {

				if (response != null) {
					JSONObject jsonObject = new JSONObject(response);

					if (jsonObject.optBoolean("status") == true) {

						SPMasjid.setValue(HomeActivity.this,
								SPMasjid.PrayerSyncDate,
								jsonObject.optString("date"));

						JSONArray jsonArray = jsonObject
								.optJSONArray("prayers_time");
						salatDataBase.deleteAllRecords();
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);

							salatDataBase.insertToTable(
									jsObject.optString("tbl_masjid_id"),
									jsObject.optString("p_month"),
									jsObject.optString("p_year"),
									jsObject.optString("p_date"),
									jsObject.optString("fajr"),
									jsObject.optString("sunrise"),
									jsObject.optString("dhuhr"),
									jsObject.optString("asr"),
									jsObject.optString("maghrib"),
									jsObject.optString("isha"),
									jsObject.optString("fajr_adhan"),
									jsObject.optString("fajr_adhan_place"),
									jsObject.optString("dhuhr_adhan"),
									jsObject.optString("dhuhr_adhan_place"),
									jsObject.optString("asr_adhan"),
									jsObject.optString("asr_adhan_place"),
									jsObject.optString("maghrib_adhan"),
									jsObject.optString("maghrib_adhan_place"),
									jsObject.optString("isha_adhan"),
									jsObject.optString("isha_adhan_place"));
						}

						JSONArray jsnArray = jsonObject
								.optJSONArray("friday_prayer");
						for (int i = 0; i < jsnArray.length(); i++) {
							jsObject = jsnArray.optJSONObject(i);
							salatDataBase.insertToFridayTable(
									jsObject.optString("id"),
									jsObject.optString("p_date"),
									jsObject.optString("khutba"),
									jsObject.optString("salat_time"),
									jsObject.optString("remark"),
									jsObject.optString("tbl_masjid_id"),
									jsObject.optString("place"));
						}

						SPMasjid.setBooleanValue(HomeActivity.this,
								SPMasjid.STATUS, true);

						setSalatTime(Calendar.getInstance());
						setViewPager();

						int curCount = salatDataBase.getSalatTimeSwap(
								currentDate).getCount();
						if (curCount == 0) {
							viewlayout.setVisibility(View.VISIBLE);
							viewPager.setVisibility(View.GONE);
						} else {
							viewlayout.setVisibility(View.GONE);
							viewPager.setVisibility(View.VISIBLE);
						}

						if (!SPMasjid.getBooleanValue(HomeActivity.this,
								SPMasjid.PrayerNoti)) {
							setSalatReminderTiming();
						}

					}

				} else {
					Toast.makeText(HomeActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	private void setSalatReminderTiming() {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		String todayDate = dateformat.format(cal.getTime());

		Cursor cur = salatDataBase.getSalatTimeDay(todayDate);
		String fajrTime = null;
		String sunriseTime = null;
		String dhuhrTime = null;
		String asrTime = null;
		String maghribTime = null;
		String ishaTime = null;

		try {
			if (cur.moveToNext()) {

				// Set Notification from Adhan timing
		/*		fajrTime = cur.getString(11);
				sunriseTime = cur.getString(6);
				dhuhrTime = cur.getString(12);
				asrTime = cur.getString(13);
				maghribTime = cur.getString(14);
				ishaTime = cur.getString(15);*/

			// Set Notification from Iqamah timing
				fajrTime = cur.getString(5);
				sunriseTime = cur.getString(6);
				dhuhrTime = cur.getString(7);
				asrTime = cur.getString(8);
				maghribTime = cur.getString(9);
				ishaTime = cur.getString(10);
			}

			cur.close();

			NamazReminder.setAlarm(HomeActivity.this, "Fajr", fajrTime,
					WebUrl.FAJR);
			NamazReminder.setAlarm(HomeActivity.this, "Sunrise", sunriseTime,
					WebUrl.SUNRISE);
			NamazReminder.setAlarm(HomeActivity.this, "Dhuhr", dhuhrTime,
					WebUrl.DHUHR);
			NamazReminder.setAlarm(HomeActivity.this, "Asr", asrTime,
					WebUrl.ASR);
			NamazReminder.setAlarm(HomeActivity.this, "Maghrib", maghribTime,
					WebUrl.MAGHRIB);
			NamazReminder.setAlarm(HomeActivity.this, "Isha", ishaTime, WebUrl.ISHA);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	class RamdanAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(480);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				Date date = new Date(ramdanTime - System.currentTimeMillis());
				long diff = (ramdanTime - System.currentTimeMillis()) / 1000;
				long hr = diff / 3600;
				diff = diff - (hr * 3600);
				long min = diff / 60;
				long sec = diff % 60;
				long day = hr / 24;
				hr = hr % 24;
				// formattter
				SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
				// Pass date object
				String formatted = formatter.format(date);
				tvRamdantTimer.setText(formatted + "");

				if (diff < 0) {
					rlRamdanPanel.setVisibility(View.GONE);
				}

				tvRamdantTimer.setText(String.format(
						"%02d Days %02d:%02d:%02d", day, hr, min, sec));
				if (ramdanTime - System.currentTimeMillis() > 1000) {
					new RamdanAsyncTask().execute();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	private long getRamdanTimeInMilis(String time) {

		try {
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			String strRamdanDate[] = time.split(" ");
			String stDate[] = strRamdanDate[0].split("-");
			String stTime[] = strRamdanDate[1].split(":");
			cal.set(Integer.parseInt(stDate[0]),
					Integer.parseInt(stDate[1]) - 1,
					Integer.parseInt(stDate[2]), Integer.parseInt(stTime[0]),
					Integer.parseInt(stTime[1]), Integer.parseInt(stTime[2]));

			return cal.getTimeInMillis();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	void matchFridaytime(String day) {
		String str[] = day.split("-");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Integer.parseInt(str[0]), Integer.parseInt(str[1]) - 1,
				Integer.parseInt(str[2]));
		// Log.v("Friday test", day + "");

		setSalatTime(calendar);
	}

	class GetMasjidAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			try {
				
			
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("token", WebUrl.TOKEN_KEY));
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("device_type", "android"));
			urlParameter.add(new BasicNameValuePair("res_height", resHeight));
			urlParameter.add(new BasicNameValuePair("res_width", resWidth));
				urlParameter.add(new BasicNameValuePair("android_fcm_token", SPMasjid.getValue(HomeActivity.this, SPMasjid.FCM_ANDROID_TOKEN)));
			// Log.v("Urlparmeter in Home", urlParameter + "");
			progressDialog = new ProgressDialog(HomeActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.GetMasjidUrl,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		try {
			
		
			progressDialog.dismiss();
		} catch (Exception e) {
			// TODO: handle exception
		}
			try {

				if (response != null) {
					JSONObject jsonObject = new JSONObject(response);
					SPMasjid.setValue(HomeActivity.this,
							SPMasjid.MasjidSyncDate,
							jsonObject.optString("date"));

					if (jsonObject.optBoolean("status") == true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						masjidJsonArray=jsonArray;
					
						SPMasjid.setValue(HomeActivity.this, SPMasjid.MASJID_ARRAY,masjidJsonArray+"");
						SPMasjid.setIntValue(HomeActivity.this, SPMasjid.MASJID_ARRAY_COUNT,masjidJsonArray.length());
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);

							SPMasjid.setValue(HomeActivity.this, SPMasjid.NAME,
									jsObject.optString("name"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.STREET,
									jsObject.optString("street"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.CITY,
									jsObject.optString("city"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.STATE, jsObject.optString("state"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.ZIPCODE,
									jsObject.optString("zipcode"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.Masjid_Name,
									jsObject.optString("name"));

							SPMasjid.setIntValue(HomeActivity.this,
									SPMasjid.RAMDAN_COUNTDOWN_FLAG,
									jsObject.optInt("ramdan_countdown"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.RAMDAN_LABEL,
									jsObject.optString("countdown_label"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.RAMDAN_DATE,
									jsObject.optString("ramdanstart_date"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.RAMDAN_DATE_ORIGINAL,
									jsObject.optString("original_ramdanstart_date"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.LATITUDE,
									jsObject.optString("lattitude"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.LONGITUDE,
									jsObject.optString("longitude"));
							SPMasjid.setValue(
									HomeActivity.this,
									SPMasjid.URL_Youtube_Stream,
									jsObject.optString("youtube_live_stream_url"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.URL_Youtube_CHANNEL,
									jsObject.optString("youtube_url"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.URL_FACEBOOK,
									jsObject.optString("facebook_url"));
							
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.URL_TWITTER,
									jsObject.optString("twitter_url"));
							SPMasjid.setValue(HomeActivity.this,
									SPMasjid.BANNER_IMAGE,
									jsObject.optString("banner_image"));
							
							SPMasjid.setIntValue(HomeActivity.this,
									SPMasjid.IS_SHOW_ISLAMIC_DATE,
									jsObject.optInt("is_show_islamic_date"));
							SPMasjid.setIntValue(HomeActivity.this, SPMasjid.ISLAMIC_DATE_OFFSET,jsObject.optInt("islamic_date_offset"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.ADHAN_LABEL,jsObject.optString("adhan_label"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.IQAMAH_LABEL,jsObject.optString("iqamah_label"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.BANNER_IMAGE_ARRAYLIST,jsObject.optString("banner_images")+"");
							SPMasjid.setValue(HomeActivity.this, SPMasjid.KHUTBA_LABEL,jsObject.optString("khutba_label"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.SALAT_LABEL,jsObject.optString("salat_label"));
							tvKhutbahText.setText(jsObject.optString("khutba_label"));
							tvSalatText.setText(jsObject.optString("salat_label"));
							
							if (jsObject.optString("khutba_label").length()==0) {
								tvKhutbah.setText("");
							}
							if (jsObject.optString("salat_label").length()==0) {
								tvJuma.setText("");
							}

							if (jsObject.optString("mobile").length()!=0) {
								imWhatsapp.setVisibility(View.VISIBLE);
							}else {
								imWhatsapp.setVisibility(View.GONE);
							}
							
							if (jsObject.optString("youtube_url").length()!=0) {
								imYoutubeChannel.setVisibility(View.VISIBLE);
							}else {
								imYoutubeChannel.setVisibility(View.GONE);
							}
							
							if (jsObject.optString("facebook_url").length()!=0) {
								imFaceBook.setVisibility(View.VISIBLE);
							}else {
								imFaceBook.setVisibility(View.GONE);
							}
							
							if (jsObject.optString("twitter_url").length()!=0) {
								imTwitter.setVisibility(View.VISIBLE);
							}else {
								imTwitter.setVisibility(View.GONE);
							}
							SPMasjid.setIntValue(HomeActivity.this, SPMasjid.DONATION_TYPE, jsObject.optInt("donation_type"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.MOBLIE_NO,jsObject.optString("mobile"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.SEHR_TIME,jsObject.optString("suhoor_label"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.IFTAR_TIME,jsObject.optString("iftar_label"));
							SPMasjid.setValue(HomeActivity.this, SPMasjid.TARAVI_TIME,jsObject.optString("taraweeh_label"));
							//tvSidebarHeaderTitle.setText(jsObject.optString("name"));
							tvAppLabel.setText(jsObject.optString("name"));
						}
						setRamdanTimer();
						setBanerImage();

						if (jsObject.optInt("is_ramadan") == 1) {
							SPMasjid.setIntValue(HomeActivity.this,
									SPMasjid.IS_RAMADAN,
									jsObject.optInt("is_ramadan"));
							ivCountHeaderText
									.setImageResource(R.drawable.ramdan);
						//	tvCountHeaderText.setText(R.string.ramdan_schedule);
						} else {
							SPMasjid.setIntValue(HomeActivity.this,
									SPMasjid.IS_RAMADAN,
									jsObject.optInt("is_ramadan"));
							ivCountHeaderText
									.setImageResource(R.drawable.update);
					//		tvCountHeaderText.setText(R.string.daily_dua);
						}
					}

					imYoutube.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri
									.parse(SPMasjid.getValue(HomeActivity.this,
											SPMasjid.URL_Youtube_Stream)));
							startActivity(newIntent);
						}
					});

					/*imMapLocation.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							try {

								if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.MASJID_ARRAY_COUNT)>1) {
									Intent intent = new Intent(HomeActivity.this,MasjidList.class).putExtra("masjidJsonArray", SPMasjid.getValue(HomeActivity.this, SPMasjid.MASJID_ARRAY)+"");
									startActivity(intent);
								}else {
									Intent intent = new Intent(HomeActivity.this,
											ProfileAndMapActivity.class);
									startActivity(intent);
								}
							} catch (Exception e) {
								// TODO: handle exception
							}

						}
					});

					ivCountHeaderText.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							if (SPMasjid
									.getIntValue(HomeActivity.this, SPMasjid.IS_RAMADAN) == 1) {
								Intent intent = new Intent(HomeActivity.this,
										RamdanScheduleActivity.class);
								startActivity(intent);
							} else {
								Intent intent = new Intent(HomeActivity.this,
										MasjidUpdateActivity.class);
								startActivity(intent);
							}
						}
					});
*/

				} else {
					Toast.makeText(HomeActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	class CheckStatusAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			Calendar c = Calendar.getInstance();
			System.out.println("Current time => " + c.getTime());
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String formattedDate = df.format(c.getTime());
			System.out.println("cHECK STATUUS" + formattedDate);
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("token", WebUrl.TOKEN_KEY));
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("current_date_time",
					formattedDate));
			urlParameter.add(new BasicNameValuePair("masjidSyncDate", SPMasjid
					.getValue(HomeActivity.this, SPMasjid.MasjidSyncDate)));
			urlParameter.add(new BasicNameValuePair("prayerSyncDate", SPMasjid
					.getValue(HomeActivity.this, SPMasjid.PrayerSyncDate)));
			urlParameter.add(new BasicNameValuePair("ramadanSyncDate", SPMasjid
					.getValue(HomeActivity.this, SPMasjid.RamdanSyncDate)));
			urlParameter.add(new BasicNameValuePair("serviceSyncDate", SPMasjid
					.getValue(HomeActivity.this, SPMasjid.ServiceSyncDate)));
			urlParameter.add(new BasicNameValuePair("themeSyncDate", SPMasjid
					.getValue(HomeActivity.this, SPMasjid.ThemeSyncDate)));
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.checkSyncStatus,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			System.out.println("Response" + response);

			try {
				if (response != null) {

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						if (jsonObject.optBoolean("masjid_sync")) {
							new GetMasjidAsyncTask().execute();
						}

						if (jsonObject.optBoolean("prayer_sync")) {
							new NamazTimeWithSyncAsyncTask().execute();
						}

						if (jsonObject.optBoolean("ramdan_sync")) {
							new RamdanScheduleWithSyncAsynsTask().execute();
						}
						if (jsonObject.optInt("android_app_version") > versionCode) {
							showUpdateDialog();
						}

					}
				} else {
					Toast.makeText(HomeActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	class RamdanScheduleAsynsTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			// Log.v("urlParameter ramdan", urlParameter + "");
			progressDialog = new ProgressDialog(HomeActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					WebUrl.RamdanScheduleTiming, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						SPMasjid.setValue(HomeActivity.this,
								SPMasjid.RamdanSyncDate,
								jsonObject.optString("date"));

						JSONArray jsonArray = jsonObject
								.optJSONArray("ramdanTimeing");
						salatDataBase.deleteRamdanRecords();
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);

							salatDataBase.insertToRamdanTable(
									jsObject.optString("ramadan_id"),
									jsObject.optString("ramdan_date"),
									jsObject.optString("ramdan_day"),
									jsObject.optString("sehr_time"),
									jsObject.optString("eftar_time"),
									jsObject.optString("taravi_time"));
						}

						Cursor cursor = salatDataBase.GetRamdanAllRecord();
						while (cursor.moveToNext()) {
							// Log.v("DATE",cursor.getString(1));
						}
					}
				} else {
					Toast.makeText(HomeActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}
	}

	void ShowSetting() {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				HomeActivity.this);

		// Setting Dialog Title
		alertDialog.setTitle("Internet is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("Internet is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(intent);

					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();

	}

	class NamazTimeWithSyncAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("date", currentDate));
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.PrayerTimeUrl,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {

				if (response != null) {
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						SPMasjid.setValue(HomeActivity.this,
								SPMasjid.PrayerSyncDate,
								jsonObject.optString("date"));

						JSONArray jsonArray = jsonObject
								.optJSONArray("prayers_time");
						salatDataBase.deleteAllRecords();
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);

							salatDataBase.insertToTable(
									jsObject.optString("tbl_masjid_id"),
									jsObject.optString("p_month"),
									jsObject.optString("p_year"),
									jsObject.optString("p_date"),
									jsObject.optString("fajr"),
									jsObject.optString("sunrise"),
									jsObject.optString("dhuhr"),
									jsObject.optString("asr"),
									jsObject.optString("maghrib"),
									jsObject.optString("isha"),
									jsObject.optString("fajr_adhan"),
									jsObject.optString("fajr_adhan_place"),
									jsObject.optString("dhuhr_adhan"),
									jsObject.optString("dhuhr_adhan_place"),
									jsObject.optString("asr_adhan"),
									jsObject.optString("asr_adhan_place"),
									jsObject.optString("maghrib_adhan"),
									jsObject.optString("maghrib_adhan_place"),
									jsObject.optString("isha_adhan"),
									jsObject.optString("isha_adhan_place"));
						}

						JSONArray jsnArray = jsonObject
								.optJSONArray("friday_prayer");
						for (int i = 0; i < jsnArray.length(); i++) {
							jsObject = jsnArray.optJSONObject(i);
							salatDataBase.insertToFridayTable(
									jsObject.optString("id"),
									jsObject.optString("p_date"),
									jsObject.optString("khutba"),
									jsObject.optString("salat_time"),
									jsObject.optString("remark"),
									jsObject.optString("tbl_masjid_id"),
									jsObject.optString("place"));
						}
						SPMasjid.setBooleanValue(HomeActivity.this,
								SPMasjid.STATUS, true);
						setViewPager();
						setSalatTime(Calendar.getInstance());
					}

				} else {
					Toast.makeText(HomeActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	class RamdanScheduleWithSyncAsynsTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			// Log.v("urlParameter ramdan", urlParameter + "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					WebUrl.RamdanScheduleTiming, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try {
				if (response != null) {

					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						SPMasjid.setValue(HomeActivity.this,
								SPMasjid.RamdanSyncDate,
								jsonObject.optString("date"));

						JSONArray jsonArray = jsonObject
								.optJSONArray("ramdanTimeing");
						salatDataBase.deleteRamdanRecords();
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);

							salatDataBase.insertToRamdanTable(
									jsObject.optString("ramadan_id"),
									jsObject.optString("ramdan_date"),
									jsObject.optString("ramdan_day"),
									jsObject.optString("sehr_time"),
									jsObject.optString("eftar_time"),
									jsObject.optString("taravi_time"));
						}
					}
				} else {
					Toast.makeText(HomeActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}
	}

	private void showUpdateDialog() {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder
				.setMessage("Are you sure,Do you wanted to update App.");

		alertDialogBuilder.setPositiveButton("yes",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						final String appPackageName = getPackageName();
						try {
							startActivity(new Intent(
									Intent.ACTION_VIEW,
									Uri.parse("http://play.google.com/store/apps/details?id="
											+ appPackageName)));
						} catch (android.content.ActivityNotFoundException anfe) {
							anfe.printStackTrace();
						}
					}
				});

		alertDialogBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	
	

	class GetHeaderInfoAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			try {
				
			
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID + ""));
			
			// Log.v("Urlparmeter in Home", urlParameter + "");
			progressDialog = new ProgressDialog(HomeActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.GET_HEADER_INFO,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			try {
				progressDialog.dismiss();

				if (response != null) {
					JSONObject jsonObject = new JSONObject(response);
			

					if (jsonObject.optBoolean("status") == true) {
						JSONArray jsonArray = jsonObject.optJSONArray("data");
					//	{"title":"","screen_name":"Home","icon":"","is_active":"0"}
						
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);

						
							if (i == 0) {
							if (jsObject.optInt("is_active")==1) {
								imMapLocation.setVisibility(View.VISIBLE);
								if (!jsObject.optString("title").equalsIgnoreCase("")) {
						//			tvLocation.setText(jsObject.optString("title"));
								}
							
							
								if (!jsObject.optString("icon").equalsIgnoreCase("")) {
									Log.e("Filename===",  getFileName(jsObject.optString("icon"))+"");
									imageLoader.DisplayImage(jsObject.optString("icon"), imMapLocation,  getFileName(jsObject.optString("icon")),
											R.drawable.blank);
								}
								if (!jsObject.optString("screen_name").equalsIgnoreCase("")) {

									imMapLocation.setTag((JSONObject)jsObject);
									imMapLocation.setOnClickListener(new OnClickListener() {
										
										@Override
										public void onClick(View v) {
											
											JSONObject tempJsonObject=(JSONObject) v.getTag();
											
											if (tempJsonObject.optInt("link_type")==0) {
												openHeaderScreen(tempJsonObject.optString("screen_name"));
											} else if (tempJsonObject.optInt("link_type")==1) {
												try {
													Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tempJsonObject.optString("screen_name")));
													startActivity(newIntent);
													
												} catch (Exception e) {
													// TODO: handle exception
													e.printStackTrace();
												}
											}
											
										}
									});
									
								}	
								
							}else {
								imMapLocation.setVisibility(View.GONE);
							}
								
							}
							else if (i == 1) {

								if (jsObject.optInt("is_active")==1) {
									ivCountHeaderText.setVisibility(View.VISIBLE);
									if (!jsObject.optString("title").equalsIgnoreCase("")) {
							//			tvCountHeaderText.setText(jsObject.optString("title"));
									}
								
								
									if (!jsObject.optString("icon").equalsIgnoreCase("")) {
										Log.e("Filename===",  getFileName(jsObject.optString("icon"))+"");
										imageLoader.DisplayImage(jsObject.optString("icon"), ivCountHeaderText,  getFileName(jsObject.optString("icon")),
												R.drawable.blank);
									}
									if (!jsObject.optString("screen_name").equalsIgnoreCase("")) {

										ivCountHeaderText.setTag((JSONObject)jsObject);
										ivCountHeaderText.setOnClickListener(new OnClickListener() {
											
											@Override
											public void onClick(View v) {
												
												JSONObject tempJsonObject=(JSONObject) v.getTag();
												
												if (tempJsonObject.optInt("link_type")==0) {
													openHeaderScreen(tempJsonObject.optString("screen_name"));
												} else if (tempJsonObject.optInt("link_type")==1) {
													try {
														Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tempJsonObject.optString("screen_name")));
														startActivity(newIntent);
														
													} catch (Exception e) {
														// TODO: handle exception
														e.printStackTrace();
													}
												}
												
											}
										});
										
									}
									
								}	else {
									ivCountHeaderText.setVisibility(View.GONE);
								}
									
								

							}
							else if (i == 2) {


								if (jsObject.optInt("is_active")==1) {
									imYoutube.setVisibility(View.VISIBLE);
									if (!jsObject.optString("title").equalsIgnoreCase("")) {
									//	tvYoutube.setText(jsObject.optString("title"));
									}
								
								
									if (!jsObject.optString("icon").equalsIgnoreCase("")) {
										Log.e("Filename===",  getFileName(jsObject.optString("icon"))+"");
										imageLoader.DisplayImage(jsObject.optString("icon"), imYoutube,  getFileName(jsObject.optString("icon")),
												R.drawable.blank);
									}
									if (!jsObject.optString("screen_name").equalsIgnoreCase("")) {

										imYoutube.setTag((JSONObject)jsObject);
										imYoutube.setOnClickListener(new OnClickListener() {
											
											@Override
											public void onClick(View v) {
												
												JSONObject tempJsonObject=(JSONObject) v.getTag();
												
												if (tempJsonObject.optInt("link_type")==0) {
													openHeaderScreen(tempJsonObject.optString("screen_name"));
												} else if (tempJsonObject.optInt("link_type")==1) {
													try {
														Intent newIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tempJsonObject.optString("screen_name")));
														startActivity(newIntent);
														
													} catch (Exception e) {
														// TODO: handle exception
														e.printStackTrace();
													}
												}
												
											}
										});
										
									}
									
								}
								else {
									imYoutube.setVisibility(View.GONE);
								}
								
							}
						}


				} else {
					Toast.makeText(HomeActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	private String getFileName(String url){
		
		String[] arrUrl=url.trim().split("/");
		
		return arrUrl[arrUrl.length-1];
		
	}


protected void openHeaderScreen(String link){
		
		try {

		
		if (link.equalsIgnoreCase("Location")) {

			try {
				
			if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.MASJID_ARRAY_COUNT)>1) {
						Intent intent = new Intent(HomeActivity.this, MasjidList.class).putExtra("masjidJsonArray", SPMasjid.getValue(HomeActivity.this, SPMasjid.MASJID_ARRAY)+"");
						startActivity(intent);
					}else {
						Intent intent = new Intent(HomeActivity.this,
								ProfileAndMapActivity.class);
						startActivity(intent);
					}
					} catch (Exception e) { 
						// TODO: handle exception
					}
					
				
		}
		else if (link.equalsIgnoreCase("Event")) {
			Intent intent = new Intent(HomeActivity.this,
					EventActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("Service")) {
			Intent intent = new Intent(HomeActivity.this,
					ServiceActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("Donate")) {
			if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.DONATION_TYPE)==0) {
				Intent intent = new Intent(HomeActivity.this,
						DonateActivity.class);
				startActivity(intent);
			} else if (SPMasjid.getIntValue(HomeActivity.this, SPMasjid.DONATION_TYPE)==1) {
				Intent intent = new Intent(HomeActivity.this,
						DonateReasonGrid.class);
				startActivity(intent);
			}
		}
		else if (link.equalsIgnoreCase("ContactUs")) {
			Intent intent = new Intent(HomeActivity.this,
					ContactUsListActivity.class);
			startActivity(intent);
		}
		
		else if (link.equalsIgnoreCase("Question")) {
			Intent intent = new Intent(HomeActivity.this,
					AskImamLatestQAActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("AskImam")) {
			Intent intent = new Intent(HomeActivity.this,
					AskQuestionActivity.class);
			startActivity(intent);
		}
	
		else if (link.equalsIgnoreCase("Ramdan")) {
			Intent intent = new Intent(HomeActivity.this,
					RamdanScheduleActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("Program")) {
			Intent intent = new Intent(HomeActivity.this,
					AnnouncementActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("AboutPage")) {
			Intent intent = new Intent(HomeActivity.this,
					AboutUsActivity.class);
			startActivity(intent);
		}
		else if (link.equalsIgnoreCase("Daily Dua")) {
			if (SPMasjid
					.getIntValue(HomeActivity.this, SPMasjid.IS_RAMADAN) == 1) {
				Intent intent = new Intent(HomeActivity.this,
						RamdanScheduleActivity.class);
				startActivity(intent);
			} else {
				Intent intent = new Intent(HomeActivity.this,
						MasjidUpdateActivity.class);
				startActivity(intent);
			}
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}
	
}
