package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aqsamasjid.ProfileAndMapActivity;
import com.aqsamasjid.pojo.MasjidPojo;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.R;

import java.util.ArrayList;

public class MasjidAddressAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<MasjidPojo> alMasjidAddress;
	
	public MasjidAddressAdapter(Activity activity, ArrayList<MasjidPojo> alMasjidAddress) {
		super();
		this.activity = activity;
		this.alMasjidAddress = alMasjidAddress;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alMasjidAddress.size();
	}

	@Override
	public MasjidPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alMasjidAddress.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
	
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.masjid_address_item, null);
			holder = new Holder();
			holder.tvMasjidAddress = (TextView) convertView.findViewById(R.id.tv_masjid_address);

		} else {
			holder = (Holder) convertView.getTag();
		}
	
		holder.tvMasjidAddress.setText(alMasjidAddress.get(position).getStreet()+" , "+alMasjidAddress.get(position).getCity()+" , "+alMasjidAddress.get(position).getState()+" , "+alMasjidAddress.get(position).getZipcode()+"");
	
		holder.tvMasjidAddress.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			//	SPMasjid.setValue(activity, SPMasjid.MASJID_ID, jsObject.optString("id"));
				SPMasjid.setValue(activity, SPMasjid.STREET, alMasjidAddress.get(position).getStreet());
				SPMasjid.setValue(activity, SPMasjid.CITY, alMasjidAddress.get(position).getCity());
				SPMasjid.setValue(activity, SPMasjid.STATE, alMasjidAddress.get(position).getState());
				SPMasjid.setValue(activity, SPMasjid.ZIPCODE,alMasjidAddress.get(position).getZipcode());
				SPMasjid.setValue(activity, SPMasjid.NAME,alMasjidAddress.get(position).getName());
				SPMasjid.setValue(activity, SPMasjid.Masjid_Name, alMasjidAddress.get(position).getMasjid_name());
				
				SPMasjid.setIntValue(activity, SPMasjid.RAMDAN_COUNTDOWN_FLAG, alMasjidAddress.get(position).getRamdanCountdown());
				SPMasjid.setValue(activity, SPMasjid.RAMDAN_LABEL, alMasjidAddress.get(position).getCountdownLabel());
				SPMasjid.setValue(activity, SPMasjid.RAMDAN_DATE, alMasjidAddress.get(position).getRamdanstartDate());
				SPMasjid.setValue(activity, SPMasjid.LATITUDE, alMasjidAddress.get(position).getLattitude());
				SPMasjid.setValue(activity, SPMasjid.LONGITUDE, alMasjidAddress.get(position).getLongitude());
				SPMasjid.setValue(activity, SPMasjid.URL_Youtube_Stream, alMasjidAddress.get(position).getYoutubeLiveStreamUrl());
				SPMasjid.setValue(activity, SPMasjid.URL_Youtube_CHANNEL, alMasjidAddress.get(position).getYoutubeUrl());
				SPMasjid.setValue(activity, SPMasjid.URL_FACEBOOK, alMasjidAddress.get(position).getFacebookUrl());
				SPMasjid.setValue(activity, SPMasjid.BANNER_IMAGE, alMasjidAddress.get(position).getBannerImage());
				Intent intent = new Intent(activity, ProfileAndMapActivity.class);
				activity.startActivity(intent);
			}
		});
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		private TextView tvMasjidAddress;
		
	}
}

