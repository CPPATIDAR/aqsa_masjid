package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aqsamasjid.pojo.TasbiPojo;
import com.aqsamasjid.utill.SalatDataBase;
import com.aqsamasjid.R;

import java.util.ArrayList;

public class TasbiAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<TasbiPojo> alTasbi;
	private SalatDataBase salatDataBase;
	
	
	public TasbiAdapter(Activity activity, ArrayList<TasbiPojo> alTasbi) {
		super();
		this.activity = activity;
		this.alTasbi = alTasbi;
		 salatDataBase=new SalatDataBase(activity);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alTasbi.size();
	}

	@Override
	public TasbiPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alTasbi.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.tasbi_item, null);
			holder = new Holder();
			holder.tvTitleEng = (TextView) convertView.findViewById(R.id.tv_title_eng);
			holder.tvTitleAr= (TextView) convertView.findViewById(R.id.tv_title_ar);
			holder.tvCount= (TextView) convertView.findViewById(R.id.tv_tasbi_count);
			
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvTitleEng.setText(alTasbi.get(position).getEnglishName().toString());
		//holder.tvTitleAr.setText(alTasbi.get(position).getArabicName().toString());
		
		if (salatDataBase.CheckIsTasbiAlreadyInDBorNot(alTasbi.get(position).getId()+"")) {
			holder.tvCount.setText("Total : "+salatDataBase.getTasbihat(alTasbi.get(position).getId()+"")+"");

		//	salatDataBase.getTasbihat(alTasbi.get(position).getId()+"");
		} else {
			holder.tvCount.setText("Total : "+"0");
}
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		
		TextView tvTitleEng;
		TextView tvTitleAr;
		TextView tvCount;
	}
}
