package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aqsamasjid.imageloader.MasjidImageLoader;
import com.aqsamasjid.pojo.AnnouncementPojo;
import com.aqsamasjid.R;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

public class AnnouncementAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<AnnouncementPojo> alAnnouncement;
	private MasjidImageLoader imageLoader;
	
	public AnnouncementAdapter(Activity activity, ArrayList<AnnouncementPojo> alAnnouncement) {
		super();
		this.activity = activity;
		this.alAnnouncement = alAnnouncement;
		imageLoader = new MasjidImageLoader(activity.getApplicationContext());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alAnnouncement.size();
	}

	@Override
	public AnnouncementPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alAnnouncement.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.event_item_announcement, null);
			holder = new Holder();
			holder.tvItemName = (TextView) convertView.findViewById(R.id.tv_title);
			holder.tvDetail = (TextView) convertView.findViewById(R.id.tv_details_service_item);
			holder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_icon);
			
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvItemName.setText(alAnnouncement.get(position).getTitle().toString());
		holder.tvDetail.setText(alAnnouncement.get(position).getDetailNoTag().toString());
		holder.ivIcon.setImageResource(R.drawable.banner_icon);
		try {
			String imageUrl = alAnnouncement.get(position).getLogoImage().toString();

			String[] strfile = imageUrl.split("[/]");
			String fName = strfile[strfile.length - 1];
			File image = new File(Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/Aqsa_Masjid/Image/ " + fName);
			if (image.exists()) {
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(image),
						null, o);
				// decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = 8;
				holder.ivIcon.setImageBitmap(BitmapFactory.decodeStream(
						new FileInputStream(image), null, o2));
			} else {
				imageLoader.DisplayImage(imageUrl, holder.ivIcon, fName,
						R.drawable.banner_icon);
			}

			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		
		TextView tvItemName;
		TextView tvDetail;
		ImageView ivIcon;
	}
}
