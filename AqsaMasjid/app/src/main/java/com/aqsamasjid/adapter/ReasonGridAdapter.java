package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aqsamasjid.NewDonationActivity;
import com.aqsamasjid.pojo.ReasonPojo;
import com.aqsamasjid.utill.SquareImageView;
import com.aqsamasjid.R;

import java.util.ArrayList;


public class ReasonGridAdapter extends BaseAdapter {

	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<ReasonPojo> alReason;
	private GridView gridReason;

	public ReasonGridAdapter(Activity activity, ArrayList<ReasonPojo> alReason) {
		super();
		this.activity = activity;
		this.alReason = alReason;
		gridReason = (GridView) activity.findViewById(R.id.gridview_reason);
	}

	@Override
	public int getCount() {
		return alReason.size();
	}

	@Override
	public ReasonPojo getItem(int position) {
		return alReason.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder;

		if (convertView == null) {

			layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.grid_item_reason,
					null);
			holder = new Holder();
			holder.tvReason = (TextView) convertView.findViewById(R.id.tv_reason);
			holder.imReason = (SquareImageView) convertView.findViewById(R.id.im_reason);
			holder.rlParent = (RelativeLayout) convertView.findViewById(R.id.rl_parent);
		} else {
			holder = (Holder) convertView.getTag();
		}
		convertView.setTag(holder);
		holder.tvReason.setTag(alReason.get(position).getId() + "");
		holder.tvReason.setText(alReason.get(position).getTitle().toString());
		holder.rlParent.setTag(alReason.get(position).getId() + "");
		holder.rlParent.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			activity.startActivity(new Intent(activity, NewDonationActivity.class).putExtra("reason_id", v.getTag()+""));
			activity.finish();
				
			}
		});
		

		return convertView;
	}

	public static class Holder {

		RelativeLayout rlParent;
		SquareImageView imReason;
		TextView tvReason;
	}

}
