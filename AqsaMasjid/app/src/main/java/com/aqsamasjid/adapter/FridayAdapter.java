package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aqsamasjid.pojo.NamazTimePojo;
import com.aqsamasjid.R;

import java.util.ArrayList;

public class FridayAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<NamazTimePojo> alJuma;
	
	public FridayAdapter(Activity activity, ArrayList<NamazTimePojo> alJuma) {
		super();
		this.activity = activity;
		this.alJuma = alJuma;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alJuma.size();
	}

	@Override
	public NamazTimePojo getItem(int position) {
		// TODO Auto-generated method stub
		return alJuma.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.friday_item, null);
			holder = new Holder();
			holder.tvKhutbahTime = (TextView) convertView.findViewById(R.id.tv_khubah_friday);
			holder.tvSalatTime = (TextView) convertView.findViewById(R.id.tv_salat_friday);
			holder.tvJumaNumber = (TextView) convertView.findViewById(R.id.jumah_numaber);
			
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvKhutbahTime.setText(alJuma.get(position).getKhutbahTime().toString());
		holder.tvSalatTime.setText(alJuma.get(position).getSalatTime().toString());
		holder.tvJumaNumber.setText(alJuma.get(position).getNumber().toString());
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		
		TextView tvKhutbahTime;
		TextView tvSalatTime;
		TextView tvJumaNumber;
	}
}
