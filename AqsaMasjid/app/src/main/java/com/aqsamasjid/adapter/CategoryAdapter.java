package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aqsamasjid.pojo.CategoryPojo;
import com.aqsamasjid.R;

import java.util.ArrayList;

public class CategoryAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<CategoryPojo> alCategory;
	
	public CategoryAdapter(Activity activity, ArrayList<CategoryPojo> alCategory) {
		super();
		this.activity = activity;
		this.alCategory = alCategory;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alCategory.size();
	}

	@Override
	public CategoryPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alCategory.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.category_item, null);
			holder = new Holder();
			holder.tvCategoryName = (TextView) convertView.findViewById(R.id.tv_category_name);
			holder.tvCategoryCount = (TextView) convertView.findViewById(R.id.tv_category_total);
			
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvCategoryName.setText(alCategory.get(position).getCategoryName().toString());
		holder.tvCategoryCount.setText(alCategory.get(position).getCategoryCount()+"");
		
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		
		TextView tvCategoryName;
		TextView tvCategoryCount;
		
	}
}
