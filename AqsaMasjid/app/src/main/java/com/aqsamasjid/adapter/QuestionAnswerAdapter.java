package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aqsamasjid.QuestioAnswerDetailActivity;
import com.aqsamasjid.pojo.QuesAnsPojo;
import com.aqsamasjid.R;

import java.util.ArrayList;

public class QuestionAnswerAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<QuesAnsPojo> alList;
	
	public QuestionAnswerAdapter(Activity activity, ArrayList<QuesAnsPojo> alList) {
		super();
		this.activity = activity;
		this.alList = alList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alList.size();
	}

	@Override
	public QuesAnsPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.question_answer_item, null);
			holder = new Holder();
			holder.tvQuestion = (TextView) convertView.findViewById(R.id.tv_question_item);
			holder.tvAnswer = (TextView) convertView.findViewById(R.id.tv_answer_item);
			holder.llQAns = (LinearLayout) convertView.findViewById(R.id.ll_que_ans);
			
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvQuestion.setText(alList.get(position).getQuestion().toString());
		holder.tvAnswer.setText(alList.get(position).getAnswer().toString());
		
		holder.llQAns.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(activity, QuestioAnswerDetailActivity.class);
				intent.putExtra("Question", alList.get(position).getQuestion());
				intent.putExtra("Detail", alList.get(position).getAnswer());
				//activity.startActivity(intent);
				//activity.finish();
				activity.startActivityForResult(intent,0);
			}
		});
		
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		
		TextView tvQuestion;
		TextView tvAnswer;
		LinearLayout llQAns;
	}
}
