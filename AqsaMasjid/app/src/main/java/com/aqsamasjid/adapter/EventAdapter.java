package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aqsamasjid.R;
import com.aqsamasjid.pojo.EventPojo;

import java.util.ArrayList;

public class EventAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<EventPojo> alEvent;
	
	public EventAdapter(Activity activity, ArrayList<EventPojo> alEvent) {
		super();
		this.activity = activity;
		this.alEvent = alEvent;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alEvent.size();
	}

	@Override
	public EventPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alEvent.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.event_item, null);
			holder = new Holder();
			holder.tvItemName = (TextView) convertView.findViewById(R.id.tv_title);
//			holder.tvFromDate = (TextView) convertView.findViewById(R.id.tv_from_date);
//			holder.tvToDate = (TextView) convertView.findViewById(R.id.tv_to_date);
			
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvItemName.setText(alEvent.get(position).getEventName().toString());
		holder.tvFromDate.setText("From "+alEvent.get(position).getFromDate().toString());
		holder.tvToDate.setText(" To "+alEvent.get(position).getToDate().toString());
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		
		TextView tvItemName;
		TextView tvFromDate;
		TextView tvToDate;
	}
}
