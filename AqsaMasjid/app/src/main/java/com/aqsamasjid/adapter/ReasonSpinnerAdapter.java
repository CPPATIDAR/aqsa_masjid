package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aqsamasjid.pojo.ReasonPojo;
import com.aqsamasjid.R;

import java.util.ArrayList;

public class ReasonSpinnerAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<ReasonPojo> alReason;
	
	public ReasonSpinnerAdapter(Activity activity, ArrayList<ReasonPojo> alReason) {
		super();
		this.activity = activity;
		this.alReason = alReason;
	}

	@Override
	public int getCount() {
		return alReason.size();
	}

	@Override
	public ReasonPojo getItem(int position) {
		return alReason.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.reason_item, null);
			holder = new Holder();
			holder.tvReason = (TextView) convertView.findViewById(R.id.tv_reason_name);

			
		} else {
			holder = (Holder) convertView.getTag();
		}
		  holder.tvReason.setTag(alReason.get(position).getId()+"");
		holder.tvReason.setText(alReason.get(position).getTitle().toString());
	
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		
		
		TextView tvReason;
	}
}
