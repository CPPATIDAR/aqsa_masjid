package com.aqsamasjid.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aqsamasjid.pojo.SubjectPojo;
import com.aqsamasjid.R;

import java.util.ArrayList;

public class GetSubjectAdapter extends BaseAdapter{
	
	private LayoutInflater layoutInflater;
	private Activity activity;
	private ArrayList<SubjectPojo> alSubject;
	
	public GetSubjectAdapter(Activity activity, ArrayList<SubjectPojo> alSubject) {
		super();
		this.activity = activity;
		this.alSubject = alSubject;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alSubject.size();
	}

	@Override
	public SubjectPojo getItem(int position) {
		// TODO Auto-generated method stub
		return alSubject.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		
		if (convertView==null) {
			
			layoutInflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.subject_item, null);
			holder = new Holder();
			holder.tvSubjectName = (TextView) convertView.findViewById(R.id.tv_subject_name);
			
		} else {
			holder = (Holder) convertView.getTag();
		}
		
		holder.tvSubjectName.setText(alSubject.get(position).getSubjectName().toString());
		
		convertView.setTag(holder);
		
		return convertView;
	}

	public static class Holder{
		
		TextView tvSubjectName;
	}
}
