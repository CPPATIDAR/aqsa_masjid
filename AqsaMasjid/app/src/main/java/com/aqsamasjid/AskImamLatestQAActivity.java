package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.ShowDialog;

@SuppressLint("NewApi") public class AskImamLatestQAActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private NetworkConnection networkConnection;
	private ShowDialog showMessage;
	private Fragment fragment;
//	private TextView tvLatestQA;
//	private TextView tvCategories;
	private RadioGroup radioGroup;
	private RadioButton rbCategory;
	private RadioButton rbLatestQA;
	private TextView tvAppLabel;
	private TextView tvServiceText;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ask_imam_latest_qa);
		initComponents();
		initListiners();
		initSideBar();
		
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		showMessage = new ShowDialog();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvAppLabel.setText(SPMasjid.getValue(AskImamLatestQAActivity.this, SPMasjid.Masjid_Name));
		tvServiceText= (TextView) findViewById(R.id.tv_serviice_text);
//		tvLatestQA =(TextView) findViewById(R.id.tv_latest_qa);
//		tvCategories =(TextView) findViewById(R.id.tv_categories);
		radioGroup = (RadioGroup) findViewById(R.id.radiogroup);
		rbCategory = (RadioButton) findViewById(R.id.rb_categories);
		rbLatestQA = (RadioButton) findViewById(R.id.rb_latest_qa);
		rbLatestQA.setBackgroundResource(R.drawable.left_green);
		rbCategory.setBackgroundResource(R.drawable.right_white);
		
		
		if (SPMasjid.getValue(AskImamLatestQAActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(AskImamLatestQAActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(AskImamLatestQAActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(AskImamLatestQAActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvServiceText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(AskImamLatestQAActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(AskImamLatestQAActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		fragment = new LatestQAFragment();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
		fragmentTransaction.replace(R.id.fragment_ask_imam, fragment);
		fragmentTransaction.commit();
	}

	private void initListiners() {
		// TODO Auto-generated method stub

		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//setResult(RESULT_OK,new Intent(AskImamLatestQAActivity.this,AskQuestionActivity.class));
				finish();
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AskImamLatestQAActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AskImamLatestQAActivity.this, ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SPMasjid.getIntValue(AskImamLatestQAActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(AskImamLatestQAActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(AskImamLatestQAActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(AskImamLatestQAActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
		
		/*tvCategories.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tvCategories.setBackgroundColor(getResources().getColor(R.color.green));
				tvCategories.setTextColor(getResources().getColor(R.color.white));
				tvLatestQA.setBackgroundColor(getResources().getColor(R.color.white));
				tvLatestQA.setTextColor(getResources().getColor(R.color.green));
				fragment = new CategoriesFragment();
				FragmentManager fm = getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fm.beginTransaction();
				fragmentTransaction.replace(R.id.fragment_ask_imam, fragment);
				fragmentTransaction.commit();
				
			}
		});*/
		
		rbCategory.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					/*rbCategory.setBackgroundColor(R.drawable.right_green);
					rbLatestQA.setBackgroundColor(R.drawable.left_white);*/
					rbCategory.setBackgroundResource(R.drawable.right_green);
					//rbCategory.setBackgroundResource(R.drawable.right_bottom_green);
					rbLatestQA.setBackgroundResource(R.drawable.left_white);
					//rbLatestQA.setBackgroundResource(R.drawable.left_bottom_white);
					fragment = new CategoriesFragment();
					FragmentManager fm = getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fm.beginTransaction();
					fragmentTransaction.replace(R.id.fragment_ask_imam, fragment);
					fragmentTransaction.commit();
				}
			}
		});
		
		rbLatestQA.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
//					rbLatestQA.setBackgroundColor(getResources().getColor(R.color.green));
//					rbCategory.setBackgroundColor(getResources().getColor(R.color.white));
					rbLatestQA.setBackgroundResource(R.drawable.left_green);
					rbCategory.setBackgroundResource(R.drawable.right_white);
					fragment = new LatestQAFragment();
					
					FragmentManager fm = getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fm.beginTransaction();
					fragmentTransaction.replace(R.id.fragment_ask_imam, fragment);
					fragmentTransaction.commit();
				}
			}
		});
		
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if (arg1 == RESULT_OK && arg0 ==0){
			finish();
		}
		super.onActivityResult(arg0, arg1, arg2);
		
	}
}
