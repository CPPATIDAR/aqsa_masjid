package com.aqsamasjid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


public class SplashActivity extends Activity {

	private NetworkConnection networkConnection;
	private String deviceId;
	private String regId;
	public static SplashActivity splashActivity;
	private int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE=10;
	private Random randm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		networkConnection = new NetworkConnection();
		splashActivity=this;
		randm = new Random();
		deviceId = String.valueOf((long)(randm.nextDouble()*10000000000000000L));
		SPMasjid.setValue(SplashActivity.this, SPMasjid.DEVICE_ID, deviceId);

		getPermissions();

	}

	private void getPermissions() {
		// Here, thisActivity is the current activity
		if (networkConnection.isOnline(getApplicationContext())) {
			try {
				new GetThemeSettingAsyncTask().execute();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		} else {
			Toast.makeText(getBaseContext(),
					"Please check your network connection.", Toast.LENGTH_SHORT)
					.show();
		}
		if ((!SPMasjid.getBooleanValue(SplashActivity.this, SPMasjid.CallUserSetting))&&((SPMasjid.getValue(SplashActivity.this, SPMasjid.TOKEN_CHECK).equals(""))&&((SPMasjid.getValue(SplashActivity.this, SPMasjid.FCM_ANDROID_TOKEN).equals(""))))) {

			if (networkConnection.isOnline(getApplicationContext())) {
				try {
					new NotificationAsyncTask().execute();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				Toast.makeText(getBaseContext(),
						"Please check your network connection.", Toast.LENGTH_SHORT)
						.show();
			}
		}
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent intent = new Intent(SplashActivity.this,
						HomeActivity.class);
				startActivity(intent);
				SplashActivity.this.finish();
			}
		}, 4000);

	}




	class NotificationAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("device_id", SPMasjid.getValue(SplashActivity.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("client_type", "android"));
			urlParameter.add(new BasicNameValuePair("device_ios_notification",""));
			urlParameter.add(new BasicNameValuePair("android_gcm_token",
					SPMasjid.getValue(SplashActivity.this, SPMasjid.GCM_Installation_ID)));
			urlParameter.add(new BasicNameValuePair("android_fcm_token", SPMasjid.getValue(SplashActivity.this, SPMasjid.FCM_ANDROID_TOKEN)));
			urlParameter.add(new BasicNameValuePair(
					"device_android_notification", SPMasjid.getValue(SplashActivity.this, SPMasjid.DEVICE_ID)));
			urlParameter.add(new BasicNameValuePair("all_notification","1"));
			urlParameter.add(new BasicNameValuePair("prayer_notification","1"));
			urlParameter.add(new BasicNameValuePair("event_notification","1"));
			urlParameter.add(new BasicNameValuePair("broadcast_notification","1"));
			urlParameter.add(new BasicNameValuePair("annoucment_notification","1"));
			Log.v("urlParameter", urlParameter + "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.UserSetting,
					urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				if (response != null) {
					Log.v("Response", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {
						if (SPMasjid.getValue(SplashActivity.this, SPMasjid.FCM_ANDROID_TOKEN).length()>0) {
							SPMasjid.setBooleanValue(SplashActivity.this, SPMasjid.CallUserSetting, true);
							SPMasjid.setValue(SplashActivity.this, SPMasjid.TOKEN_CHECK,"tokencheck");
						}

					}
				} else {
					Toast.makeText(SplashActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}


	class GetThemeSettingAsyncTask extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();

			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));

			/*progressDialog = new ProgressDialog(SplashActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();*/
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					WebUrl.GET_THEME_SETTING, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//	progressDialog.dismiss();
			try {

				if (response != null) {
					JSONObject jsonObject = new JSONObject(response);
					SPMasjid.setValue(SplashActivity.this, SPMasjid.ThemeSyncDate,jsonObject.optString("date"));
					if (jsonObject.optBoolean("status") == true) {
						JSONArray jsonArray = jsonObject.optJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);
							if (jsObject.optInt("is_set_default_theme")==0) {
								Iterator iterator = jsObject.keys();
								while (iterator.hasNext()) {
									String key = (String) iterator.next();
									SPMasjid.setValue(getApplicationContext(), key,
											jsObject.optString(key));
								}
							}else {
								Iterator iterator = jsObject.keys();
								while (iterator.hasNext()) {
									String key = (String) iterator.next();
									SPMasjid.setValue(getApplicationContext(), key,"");
								}
							}

						}

					}

				} else {
					Toast.makeText(SplashActivity.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}