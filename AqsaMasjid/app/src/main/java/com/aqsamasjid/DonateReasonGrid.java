package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.adapter.ReasonGridAdapter;
import com.aqsamasjid.pojo.ReasonPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class DonateReasonGrid extends BaseActivity {

	// private Button btnDonateWithPaypal;
	private Button btnPayNow;
	private NetworkConnection networkConnection;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
	// private ImageView ivLogoWithText;
	private EditText etDonationAmount;
	private EditText etEmail;
	private TextView tvAppLabel;

	//private String deviceId;
	private String id;
	private String createTime;
	private String platform;
	private String shortDescription;
	private String ammount;
	private String currencyCode;

	private String reasonValue;

	ArrayList<ReasonPojo> listdata;

	private TextView tvEventText;

	private TextView tvDontionText;
	public String donationPageText;
	private GridView gridReason;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_donate_reason_grid);
		initComponents();
		initListiners();
		initSideBar();
		/*TelephonyManager manager = (TelephonyManager) getApplicationContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		deviceId = manager.getDeviceId();*/
		if (networkConnection.isOnline(getApplicationContext())) {
			new GetPaymentDetail().execute();
		} else {
			Toast.makeText(getBaseContext(),
					"Please check your network connection.", Toast.LENGTH_SHORT)
					.show();
		}
	}
	
	

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);

		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		gridReason = (GridView) findViewById(R.id.gridview_reason);
		tvAppLabel = (TextView) findViewById(R.id.tv_masjid_label);
		tvEventText = (TextView) findViewById(R.id.tv_event_text);
		tvAppLabel.setText(SPMasjid.getValue(DonateReasonGrid.this,
				SPMasjid.Masjid_Name));

		if (SPMasjid.getValue(DonateReasonGrid.this,
				SPMasjid.inner_header_navigation_BackColor).length() != 0
				&& (!SPMasjid.getValue(DonateReasonGrid.this,
						SPMasjid.inner_header_navigation_BackColor)
						.equalsIgnoreCase(null))) {
			tvEventText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(
					getApplicationContext(),
					SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(DonateReasonGrid.this,
				SPMasjid.inner_header_navigation_BackAlpha).length() != 0
				&& (!SPMasjid.getValue(DonateReasonGrid.this,
						SPMasjid.inner_header_navigation_BackAlpha)
						.equalsIgnoreCase(null))
				&& Float.parseFloat(SPMasjid.getValue(getApplicationContext(),
						SPMasjid.inner_header_navigation_BackAlpha)) > 0.0f) {
			tvEventText.setAlpha(Float.parseFloat(SPMasjid.getValue(
					getApplicationContext(),
					SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(DonateReasonGrid.this,
				SPMasjid.inner_header_navigation_ForeColor).length() != 0
				&& (!SPMasjid.getValue(DonateReasonGrid.this,
						SPMasjid.inner_header_navigation_ForeColor)
						.equalsIgnoreCase(null))) {
			tvEventText.setTextColor(Color.parseColor(SPMasjid.getValue(
					getApplicationContext(),
					SPMasjid.inner_header_navigation_ForeColor)));
		}

		if (SPMasjid.getValue(DonateReasonGrid.this,
				SPMasjid.donate_btn_BackColor).length() != 0
				&& (!SPMasjid.getValue(DonateReasonGrid.this,
						SPMasjid.donate_btn_BackColor).equalsIgnoreCase(null))) {
			btnPayNow.setBackgroundColor(Color.parseColor(SPMasjid.getValue(
					getApplicationContext(), SPMasjid.donate_btn_BackColor)));
		}
		if (SPMasjid.getValue(DonateReasonGrid.this, SPMasjid.donate_btn_alpha)
				.length() != 0
				&& (!SPMasjid.getValue(DonateReasonGrid.this,
						SPMasjid.donate_btn_alpha).equalsIgnoreCase(null))
				&& Float.parseFloat(SPMasjid.getValue(getApplicationContext(),
						SPMasjid.donate_btn_alpha)) > 0.0f) {
			btnPayNow.setAlpha(Float.parseFloat(SPMasjid.getValue(
					getApplicationContext(), SPMasjid.donate_btn_alpha)));
		}
		if (SPMasjid.getValue(DonateReasonGrid.this,
				SPMasjid.donate_btn_ForeColor).length() != 0
				&& (!SPMasjid.getValue(DonateReasonGrid.this,
						SPMasjid.donate_btn_ForeColor).equalsIgnoreCase(null))) {
			btnPayNow.setTextColor(Color.parseColor(SPMasjid.getValue(
					getApplicationContext(), SPMasjid.donate_btn_ForeColor)));
		}
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DonateReasonGrid.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DonateReasonGrid.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});

		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});



		
	}



	

	public void showMsgDialog(String message) {
		new AlertDialog.Builder(DonateReasonGrid.this)

		.setMessage(message)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				}).create().show();
	}

	class GetPaymentDetail extends AsyncTask<Void, Void, Void> {
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter = new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id",
					WebUrl.MASJID_ID + ""));
			urlParameter.add(new BasicNameValuePair("token", WebUrl.TOKEN_KEY));
			progressDialog = new ProgressDialog(DonateReasonGrid.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
			Log.v("URL PArameter payment", urlParameter + "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(
					WebUrl.GetPaymentDetail, urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();

			try {
				if (response != null) {
					Log.v("Response=", response);
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status") == true) {

						JSONObject jsObject = jsonObject.optJSONObject("data");
						
						donationPageText = jsObject
								.optString("donation_page_text");
						
						JSONArray jsonArray = jsObject.optJSONArray("reasons");
						System.out.println(jsObject.optJSONArray("reasons"));

						listdata = new ArrayList<ReasonPojo>();
						JSONArray jArray = (JSONArray) jsonArray;
						if (jArray != null) {
							for (int i = 0; i < jArray.length(); i++) {
								JSONObject jobject = jArray.optJSONObject(i);
								ReasonPojo pojo = new ReasonPojo();
								pojo.setId(jobject.optLong("id"));
								pojo.setTitle(jobject.optString("title"));
								listdata.add(pojo);
							}
						}
					
						gridReason.setAdapter(new ReasonGridAdapter(
								DonateReasonGrid.this, listdata));
					}

				} else {
					Toast.makeText(DonateReasonGrid.this, "Network error",
							Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
