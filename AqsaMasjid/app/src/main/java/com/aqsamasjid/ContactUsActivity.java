package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.adapter.GetSubjectAdapter;
import com.aqsamasjid.pojo.SubjectPojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.EmailValidator;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.ShowDialog;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi") public class ContactUsActivity extends BaseActivity {

	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private EditText etEmail;
	private EditText etName;
	private EditText etContact;
	private EditText etComment;
	private Spinner spnSubject;
	private Button btnSubmit;
	private NetworkConnection networkConnection;
	private ShowDialog showMessage;
	private ArrayList<SubjectPojo> alSubject;
	private GetSubjectAdapter adapter;
	private SubjectPojo pojo;
	private long subjectId;
	private TextView tvAppLabel;
	private TextView tvServiceText;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_us);
		initComponents();
		initListiners();
		initSideBar();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		showMessage = new ShowDialog();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		etName = (EditText) findViewById(R.id.et_contact_name);
		etEmail = (EditText) findViewById(R.id.et_contact_email);
		etComment = (EditText) findViewById(R.id.et_contact_comment);
		etContact = (EditText) findViewById(R.id.et_contact);
		spnSubject = (Spinner) findViewById(R.id.spn_subject);
		btnSubmit = (Button) findViewById(R.id.btn_contact_submit);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvServiceText= (TextView) findViewById(R.id.tv_serviice_text);
		tvAppLabel.setText(SPMasjid.getValue(ContactUsActivity.this, SPMasjid.Masjid_Name));
		spnSubject.setVisibility(View.GONE);

		try {
			subjectId=getIntent().getExtras().getLong("subject_id", 0);
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (SPMasjid.getValue(ContactUsActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(ContactUsActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(ContactUsActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(ContactUsActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvServiceText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(ContactUsActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(ContactUsActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}

		if (SPMasjid.getValue(ContactUsActivity.this, SPMasjid.application_btn_BackColor).length()!=0&&(!SPMasjid.getValue(ContactUsActivity.this, SPMasjid.application_btn_BackColor).equalsIgnoreCase(null)))
		{
			btnSubmit.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_BackColor)));
		}
		if (SPMasjid.getValue(ContactUsActivity.this, SPMasjid.application_btn_alpha).length()!=0&&(!SPMasjid.getValue(ContactUsActivity.this, SPMasjid.application_btn_alpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha))>0.0f)
		{
			btnSubmit.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_alpha)));
		}
		if (SPMasjid.getValue(ContactUsActivity.this, SPMasjid.application_btn_ForeColor).length()!=0&&(!SPMasjid.getValue(ContactUsActivity.this, SPMasjid.application_btn_ForeColor).equalsIgnoreCase(null)))
		{
			btnSubmit.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.application_btn_ForeColor)));
		}




	}

	private void initListiners() {
		// TODO Auto-generated method stub

		ivHome.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(ContactUsActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

				//setResult(RESULT_OK,new Intent(ContactUsActivity.this,ContactUsActivity.class));
				finish();
			}
		});
		tvAppLabel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(ContactUsActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				//setResult(RESULT_OK,new Intent(ContactUsActivity.this,ContactUsActivity.class));
				finish();
			}
		});
		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ContactUsActivity.this,
						EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llServices.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ContactUsActivity.this,
						ServiceActivity.class);
				startActivity(intent);
				finish();
			}
		});
		llDonate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if (SPMasjid.getIntValue(ContactUsActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(ContactUsActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(ContactUsActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(ContactUsActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});
		
		spnSubject.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				subjectId = alSubject.get(position).getSubjectId();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				/*if (subjectId == 0) {
					showMessage.showMsg(ContactUsActivity.this, "Please Choose Subject.");
				} else */
				if (etEmail.getText().toString().length() == 0) {
					showMessage.showMsg(ContactUsActivity.this, "Please Enter E-mail Id.");
				}else if (!EmailValidator.isValid(etEmail.getText().toString())) {
					showMessage.showMsg(ContactUsActivity.this, "Please Enter Valid E-mail.");
				}else if (etName.getText().toString().length() == 0) {
					showMessage.showMsg(ContactUsActivity.this, "Please Enter Name.");
				}else if (etContact.getText().toString().length() == 0) {
					showMessage.showMsg(ContactUsActivity.this, "Please Enter Contact Number.");
				}else if (etComment.getText().toString().length() == 0) {
					showMessage.showMsg(ContactUsActivity.this, "Please Enter Comment.");
				}else {
				if (networkConnection.isOnline(getApplicationContext())) {
					new ContactUsAsyncTask().execute();
				} else {
					Toast.makeText(getBaseContext(),
							"Please check your network connection.", Toast.LENGTH_SHORT)
							.show();
				}
			  } 	
			}
		});
	}
	
	
	class ContactUsAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("subject_id",subjectId+""));
			urlParameter.add(new BasicNameValuePair("email",etEmail.getText().toString()));
			urlParameter.add(new BasicNameValuePair("message",etComment.getText().toString()));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(ContactUsActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.Save_ContactAS, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			
			try {
				if (response!=null) {
					JSONObject jsonObject = new JSONObject(response);
					if (jsonObject.optBoolean("status")==true) {
						
							new AlertDialog.Builder(ContactUsActivity.this)

							.setMessage(jsonObject.optString("message"))
									.setPositiveButton("OK", new DialogInterface.OnClickListener() {

										public void onClick(DialogInterface dialog, int id) {
//											dialog.cancel();
											finish();
										}
									}).create().show();
						
					}else {
						showMessage.showMsg(ContactUsActivity.this, jsonObject.optString("message"));
					}
					
				} else {
					Toast.makeText(ContactUsActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
