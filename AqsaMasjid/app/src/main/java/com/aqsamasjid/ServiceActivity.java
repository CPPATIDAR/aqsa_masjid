package com.aqsamasjid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aqsamasjid.adapter.ServiceAdapter;
import com.aqsamasjid.pojo.ServicePojo;
import com.aqsamasjid.utill.CustomHttpClient;
import com.aqsamasjid.utill.NetworkConnection;
import com.aqsamasjid.utill.SPMasjid;
import com.aqsamasjid.utill.WebUrl;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

@SuppressLint("NewApi") public class ServiceActivity extends BaseActivity {
	
	private NetworkConnection networkConnection;
	private ArrayList<ServicePojo> alService;
	private ServiceAdapter adapterService;
	private ListView listView;
	private LinearLayout llPrayer;
	private LinearLayout llEvents;
	private LinearLayout llServices;
	private LinearLayout llDonate;
	private ImageView ivHome;
//	private ImageView ivLogoWithText;
	private int pageCount = 0;
	private boolean canSend = false;
	private TextView tvAppLabel;
	private TextView tvServiceText;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service);
		initComponents();
		initListiners();
		initSideBar();
		
		if (networkConnection.isOnline(getApplicationContext())) {
			new ServiceAsyncTask().execute();
			} else {
				Toast.makeText(getBaseContext(),
						"Please check your network connection.", Toast.LENGTH_SHORT).show();
		}
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		networkConnection = new NetworkConnection();
		ivHome = (ImageView) findViewById(R.id.iv_home);
//		ivLogoWithText = (ImageView) findViewById(R.id.iv_logo_text);
		listView = (ListView) findViewById(R.id.listView);
		llPrayer = (LinearLayout) findViewById(R.id.ll_prayer);
		llEvents = (LinearLayout) findViewById(R.id.ll_events);
		llServices = (LinearLayout) findViewById(R.id.ll_services);
		llDonate = (LinearLayout) findViewById(R.id.ll_donate);
		//rlHeaderParent= (RelativeLayout) findViewById(R.id.rl_header_parent);
		tvAppLabel  = (TextView) findViewById(R.id.tv_masjid_label);
		tvServiceText= (TextView) findViewById(R.id.tv_serviice_text);
		tvAppLabel.setText(SPMasjid.getValue(ServiceActivity.this, SPMasjid.Masjid_Name));
		
		if (SPMasjid.getValue(ServiceActivity.this, SPMasjid.inner_header_navigation_BackColor).length()!=0&&(!SPMasjid.getValue(ServiceActivity.this, SPMasjid.inner_header_navigation_BackColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackColor)));
		}
		if (SPMasjid.getValue(ServiceActivity.this, SPMasjid.inner_header_navigation_BackAlpha).length()!=0&&(!SPMasjid.getValue(ServiceActivity.this, SPMasjid.inner_header_navigation_BackAlpha).equalsIgnoreCase(null))&&Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha))>0.0f)
		{
			tvServiceText.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_BackAlpha)));
		}
		if (SPMasjid.getValue(ServiceActivity.this, SPMasjid.inner_header_navigation_ForeColor).length()!=0&&(!SPMasjid.getValue(ServiceActivity.this, SPMasjid.inner_header_navigation_ForeColor).equalsIgnoreCase(null)))
		{
			tvServiceText.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.inner_header_navigation_ForeColor)));
		}
		
		
		/*if (SPMasjid.getValue(ServiceActivity.this, SPMasjid.header_BackColor).length()!=0&&(!SPMasjid.getValue(ServiceActivity.this, SPMasjid.header_BackColor).equalsIgnoreCase(null)))
		{
			rlHeaderParent.setBackgroundColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackColor)));								
		}
		
		
		if (SPMasjid.getValue(ServiceActivity.this, SPMasjid.header_BackAlpha).length()!=0&&(!SPMasjid.getValue(ServiceActivity.this, SPMasjid.header_BackAlpha).equalsIgnoreCase(null)))
		{
			rlHeaderParent.setAlpha(Float.parseFloat(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_BackAlpha)));
		}
		
		if (SPMasjid.getValue(ServiceActivity.this, SPMasjid.header_ForeColor).length()!=0&&(!SPMasjid.getValue(ServiceActivity.this, SPMasjid.header_ForeColor).equalsIgnoreCase(null)))
		{
			tvAppLabel.setTextColor(Color.parseColor(SPMasjid.getValue(getApplicationContext(), SPMasjid.header_ForeColor)));
		}*/
		
	}

	private void initListiners() {
		// TODO Auto-generated method stub
		ivHome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(ServiceActivity.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});
		
		/*ivLogoWithText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish();
			}
		});*/
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ServiceActivity.this, ServiceDetailActivity.class);
				intent.putExtra("ServiceTitle", alService.get(position).getTitle());
				intent.putExtra("ServiceDetail", alService.get(position).getDetail());
				intent.putExtra("ServiceBannerImage", alService.get(position).getBannerImage());
				startActivityForResult(intent, 0);
				//startActivity(intent);
				//finish();
			}
		});

		listView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub

				if (firstVisibleItem > (totalItemCount - 8) && canSend && totalItemCount > 10) {
					canSend = false;
					if (networkConnection.isOnline(getApplicationContext())) {
						new ServiceAsyncTask().execute();
					} else {
						Toast.makeText(getBaseContext(),"Please check your network connection.",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});

		llPrayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(ServiceActivity.this,HomeActivity.class);
				startActivity(intent);*/
				finish();
			}
		});

		llEvents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ServiceActivity.this, EventActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
		llServices.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});

		
		llDonate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (SPMasjid.getIntValue(ServiceActivity.this, SPMasjid.DONATION_TYPE)==0) {
					Intent intent = new Intent(ServiceActivity.this,
							DonateActivity.class);
					startActivity(intent);
				} else if (SPMasjid.getIntValue(ServiceActivity.this, SPMasjid.DONATION_TYPE)==1) {
					Intent intent = new Intent(ServiceActivity.this,
							DonateReasonGrid.class);
					startActivity(intent);
				}
				finish();
			}
		});

	}
	
	class ServiceAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String response;
		private ArrayList<NameValuePair> urlParameter;
		private JSONObject jsObject;
		private ServicePojo pojo;
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			urlParameter=new ArrayList<NameValuePair>();
			urlParameter.add(new BasicNameValuePair("masjid_id", WebUrl.MASJID_ID+""));
			urlParameter.add(new BasicNameValuePair("page_number",pageCount+""));
			Log.v("urlParameter", urlParameter+"");
			progressDialog = new ProgressDialog(ServiceActivity.this);
			progressDialog.setMessage("Loading...");
			progressDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			response = CustomHttpClient.executeHttpPost(WebUrl.ServiceUrl, urlParameter);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {
				if (response!=null) {
					Log.v("Response", response);
					JSONObject jsonObject = new JSONObject(response);
					SPMasjid.setValue(ServiceActivity.this, SPMasjid.ServiceSyncDate,jsonObject.optString("date"));
					if (jsonObject.optBoolean("status")==true) {
						JSONArray jsonArray = jsonObject.optJSONArray("result");
						alService = new ArrayList<ServicePojo>();
						
						for (int i = 0; i < jsonArray.length(); i++) {
							jsObject = jsonArray.optJSONObject(i);
							
							pojo = new ServicePojo();
							pojo.setId(jsObject.optInt("id"));
							pojo.setTitle(jsObject.optString("title"));
							pojo.setDetail(jsObject.optString("details"));
							pojo.setDetailNoTag(jsObject.optString("detailnotag"));
							pojo.setLogoImage(jsObject.optString("logo_image"));
							pojo.setBannerImage(jsObject.optString("poster_image"));
							alService.add(pojo);
						}
						adapterService = new ServiceAdapter(ServiceActivity.this, alService);
						listView.setAdapter(adapterService);
						pageCount++;
						canSend = true;
					}	
				} else {
					Toast.makeText(ServiceActivity.this, "Network error", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if (arg1 == RESULT_OK && arg0 ==0){
			finish();
		}
		super.onActivityResult(arg0, arg1, arg2);
		
	}
	
}
